#!/usr/bin/env bash
# -*- coding: utf-8 -*-
# bash

# This script was used to create this repo

if [ -z "4" ] || [ -z "4" ] || [ 4 -lt 4 ]; then
   colecho -e1 "This script requires Bash version >= 4"
   exit 1
fi

cd "$(dirname "$0")"
set -o nounset
set -o errexit
# Prevent overwriting of files by redirection (may be overridden by >|)
set -o noclobber
# Enable exteneded glob https://mywiki.wooledge.org/glob#extglob
shopt -s extglob
# globstar recursively repeats a pattern containing '**'.
shopt -s globstar

mkdir -p "/tmp/merger.d/{filter,cache,tmp}"

msg="colecho -v"

insert_submodule () {
    $msg -i2 "Inserting submodule"
    path="$2"
    url="$1"

    $msg "Path : $path"
    $msg "Url  : $url"

    cache="/tmp/merger.d/cache/$path"

    if [ -d "$cache" ]; then
        $msg -i1 "Directory is cached"
        url="$cache"
    else
        $msg -i1 "Directory is not cached. Cloning to cache"
        git clone $url "$cache"
        url="$cache"
    fi

    $msg -i1 "Current directory" $(pwd)

    start=$(pwd)

    rm -rf "$path"
    git clone "$url" "$path"
    cd "$path"
    git remote rm origin



    $msg -i1 "Cloned"
    $msg -w1 "Changing commit paths"
    $msg "Current working directory: $(pwd)"
    git filter-branch \
        --setup 'echo $(pwd) ; mkdir -p ../tmp'  \
        --tree-filter \
        "mv * ../tmp; mkdir -p $path; \
            ( mv ../tmp/* $path/ || exit 0; )" \
        --tag-name-filter cat --prune-empty -- --all

    colecho -vi3 "Filter-branch"
    cd "$start"
    $msg -i1 "Moving files"
    mv $path/$path/* $path
    rm -r $path/$path

    echo $(pwd)

    # git remote add "$path" "$(pwd)/$path"
    # git remote -v
    # colecho -vi1 "Added remote"

    $msg "Current working directory: $(pwd)"
    $msg -i1 "Running merge"
    git pull "$path" master \
        --allow-unrelated-histories \
        --no-edit \
        --strategy=ours

    rm -rf $path/.git
    $msg -i1 "Removed git folder from submodule"
}




start_dir="hmerge_tmp"
res_dir="hmerge"
top_dir=$(pwd)

rm -rf $start_dir
mkdir -p $start_dir
cd $start_dir

$msg "Current working directory: $(pwd)"

git init
touch none
git add none
git commit -m "[REPO] Initial commit"


insert_submodule \
    https://gitlab.com/haxscamper-misc/cpp/Algorithm.git cpp/halgorithm

insert_submodule \
    https://gitlab.com/haxscamper-misc/cpp/Support.git cpp/hsupport

insert_submodule \
    https://gitlab.com/haxscamper-misc/cpp/DebugMacro.git cpp/hdebugmacro

insert_submodule \
    https://gitlab.com/haxscamper-misc/cpp/cppcmutils.git utils/cppcmutils

insert_submodule \
    https://gitlab.com/haxscamper-misc/support-misc.git support_misc


colecho -vw2 "Removing unwanted files"

git filter-branch \
    --index-filter \
    'git rm --quiet --cached --ignore-unmatch \
    **/main *.html **/CMakeFiles/* **/CMakeLists.txt.user \
    **/Doxyfile **/*.pro.user **/*.png **/strace_out **/gsl/*' HEAD

git for-each-ref --format="%(refname)" refs/original/ | xargs -n 1 git update-ref -d
git reflog expire --expire-unreachable=all --all
git gc --aggressive --prune=now

$msg -i2 "Done merging repos"
$msg "Total commits added" $(git rev-list --all --count)

cat << EOF >> .gitignore
**/main
**/*.html
**/CMakeFiles/*
**/CmakeLists.txt.user
**/Doxyfile
**/*.pro.user
**/*.png
**/strace_out
**/gsl/*
EOF


git add .
git commit --quiet -m "[REPO]"


cd $top_dir
$msg "Current working directory: $(pwd)"
rm -rf $res_dir
git clone $start_dir $res_dir
rm -rf $start_dir
cd $res_dir
$msg "Current working directory: $(pwd)"


colecho -vi1 "Top-10 biggest blobs"
list-biggest-git-blobs.sh | tail | tac | nl
colecho -vi1 "Total repository size:" $(list-biggest-git-blobs.sh \
                                            | awk '{ print $2; }' \
                                            | numfmt --from=iec \
                                            | paste -sd+ \
                                            | bc | numfmt --to=iec)
