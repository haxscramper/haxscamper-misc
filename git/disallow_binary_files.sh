#!/usr/bin/env bash
cecho -i3 -p "Disallow binary files"

while read file
do
    type=$(file -b --mime-type $file)
    if [[ $type =~ ^text ]]
    then
        cecho -l -p $file

    elif [[ $type =~ ^application ]]
    then
        cecho -E -p "Binary file. Aborting commit"
        cecho -l -p $file " " $type
        exit 1

    else
        cecho -e -p "Not a text file"
        cecho -l -p $file " " $(file -b --mime-type $file)
        exit 1
    fi
done < <(git diff --cached --name-only --diff-filter=ACM)

cecho -i -p "Binary file check passed"
