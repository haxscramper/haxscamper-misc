#!/usr/bin/env bash
# -*- coding: utf-8 -*-
# bash

rm -rf bin
mkdir -p bin

for file in $(find . -type f | grep "git-"); do
    ln -s ../$file bin/${file%.*}
done

exa -T ../git/*
