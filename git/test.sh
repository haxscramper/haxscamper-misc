#!/usr/bin/env bash
# -*- coding: utf-8 -*-
# bash
cd "$(dirname "$0")"
set -o nounset

cd /tmp/
rm -rf git-testing
mkdir -p git-testing
cd git-testing

rm -rf hack
git-clone-cached https://gitlab.com/haxscramper/hack.git
cd hack

git-insert-repo-as-dir \
    /mnt/workspace/git-sandbox/rpi-keyboard \
    projects/rpi-keyboard

cd projects/rpi-keyboard
git log --oneline --decorate --graph -n10
