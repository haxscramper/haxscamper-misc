#!/usr/bin/env bash
# -*- coding: utf-8 -*-
# bash

# TODO Add support for --quiet option
# TODO Add support for passthrough option list (eveything
# after -- will be passed directly to both git-clone)
# TODO Add support for --force option to make new clone even
# if already cached

{
    if [[ "$1" = "-h" ]]; then
        cat << EOF | groff -man -Tascii
.TH git-clone-cached.sh 1 '2019-07-08' '0.0.1'
.SH NAME
git-clone-cached.sh URL [PATH]
.SH SYNOPSIS
git-clone-cached.sh
.SH DESCRIPTION
Clone repository to the /tmp/git-clone-cached.d/ and then
clone to target. On subsequent runs this will cause script
to immediatelly use repo from /tmp instead of downloading
everything over and over again
.SS Options
.TP
URL
url of the remote to clone from
.TP
[PATH]
.B
Relative
path to the directory to clone to. Optional.
Defaults to $(basename URL | sed 's/.git//')
EOF

        exit 1
    fi

}



if [[ $# -lt 1 ]]; then
    colecho -e1 "Missing repo url"
    exit 1
fi

tmp_dir="/tmp/git-clone-cached.d"
url=$1

repo_name=$(basename $url | sed 's/.git//')
dir=${2:-$repo_name}


mkdir -p $tmp_dir

{
    git clone $1 $tmp_dir/$repo_name
    git clone $tmp_dir/$repo_name $(pwd)/$dir
} 2>&1 | xargs -i colecho -v '{}'
