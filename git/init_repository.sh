#!/usr/bin/env bash
git init
cd .git/hooks/
rm *.sample

script_soource_dir=~/.config/personal/scripts/git

ln -s $script_source_dir/disallow_binary_files.sh .
cp $script_source_dir/pre-commit .

cd ../..

cecho -i -p "Updated hooks"

read -p "Enter comma-separated languages " ignoring
cecho -i -p $ignoring

curl -sL https://www.gitignore.io/api/$ignoring |
    tee .gitignore |
    grep ERROR
