#!/usr/bin/env bash
git remote set-url origin \
    $(git remote -v \
    | head -n1 \
    | awk '{print $2}' \
    | sed 's!https://gitlab.com/!git@gitlab.com:!')
