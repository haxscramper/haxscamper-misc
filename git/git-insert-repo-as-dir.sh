#!/usr/bin/env bash
# -*- coding: utf-8 -*-
# bash
set -o nounset
set -o errexit

# TODO Add support for after-clone actions (usefule for checking out
# required branch)

{
    if [[ "$1" = "-h" ]]; then
        cat << EOF | groff -man -Tascii
.TH git-insert-repo-as-dir.sh 1 '2019-07-08' '0.0.1'
.SH NAME
git-insert-repo-as-dir.sh URL PATH
.SH SYNOPSIS
git-insert-repo-as-dir.sh
.SH DESCRIPTION
Clone repo from URL, put it into the PATH and integrate
it's history to the history of the parent repository.
.SS Options
.TP
URL
Url of the remote repository to clone from
.TP
PATH
.B
Relative
Path to the newly inserted module
EOF
exit 1
fi
}


msg="colecho -v"

$msg -i2 "Inserting submodule"
path="$2"
url="$1"

$msg "Path : $path"
$msg "Url  : $url"

$msg -i1 "Current directory" $(pwd)

start=$(pwd)

rm -rf "$path"
git-clone-cached $url $path
cd $path
git remote rm origin


tmp_d="/tmp/tmp.d"
mkdir -p $tmp_d

$msg -i1 "Cloned"
$msg -w1 "Changing commit paths"
$msg "Current working directory: $(pwd)"

git filter-branch --tree-filter \
    "mkdir -p ../tmp ; mv * ../tmp ; mkdir -p $path ; mv ../tmp/* $path "


$msg -i3 "Filter-branch"

cd $start
mv $(pwd)/$path/$path/* $path
rm -r $path/$path

$msg "Current working directory: $(pwd)"
$msg -i1 "Running merge"
git pull "$path" master \
    --allow-unrelated-histories \
    --no-edit \
    --strategy=ours

rm -rf $path/.git
$msg -i1 "Removed git folder from submodule"
