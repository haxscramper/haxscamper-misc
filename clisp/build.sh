#!/usr/bin/env bash
cd "$(dirname "$0")"
set -o nounset
set -o errexit

cecho -i1 "Running build.sh"\n
sbcl --load
