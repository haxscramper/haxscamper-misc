#!/usr/bin/env bash
cd "$(dirname "$0")"
set -o nounset
set -o errexit

cecho -i1 "Running test.sh"
##== Only edit lines after this comment

# Run target
sbcl --script colecho.lisp
