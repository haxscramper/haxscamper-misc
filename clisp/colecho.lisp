(load "~/.sbclrc")
;; TODO reformat using format (for verbose mode)

(defun colecho-get-prefix (style type level)
  (cond
    ((eq style :default)
     (cond
       ((eq type :log)
        (cond
          ((eq level :0) "  >")
          ((eq level :1) "---")
          ((eq level :2) "***")
          ((eq level :3) ">>>")))
       ((eq type :info)
        (cond
          ((eq level :0) "  >")
          ((eq level :1) "  >")
          ((eq level :2) "-->")
          ((eq level :3) "-->")))
       ((eq type :warn)
        (cond
          ((eq level :0) "==>")
          ((eq level :1) "==>")
          ((eq level :2) "=>>")
          ((eq level :3) "=>>")))
       ((eq type :error)
        (cond
          ((eq level :0) "###")
          ((eq level :1) "!!!")
          ((eq level :2) "!!!")
          ((eq level :3) "!!!")))))
    ((eq style :verbose)
     (cond
       ((eq type :log)
        (format nil "[ LOG ~A   ]:" level))
       ((eq type :info)
        (format nil "[ INFO ~A  ]:" level))
       ((eq type :warn)
        (format nil "[ WARN ~A  ]:" level))
       ((eq type :error)
        (format nil "[ ERROR ~A ]:" level))))
    ((eq style :gtest)
     (cond
       ((eq type :log)
        (cond
          ((eq level :0) "[          ]")
          ((eq level :1) "[----------]")
          ((eq level :2) "[**********]")
          ((eq level :3) "[>>>>>>>>>>]")))
       ((eq type :info)
        (cond
          ((eq level :0) "[->->->->->]")
          ((eq level :1) "[->->->->->]")
          ((eq level :2) "[->>->>->>-]")
          ((eq level :3) "[->>->>->>-]")))
       ((eq type :warn)
        (cond
          ((eq level :0) "[=>>=>>=>>=]")
          ((eq level :1) "[=>>=>>=>>=]")
          ((eq level :2) "[=>>=>>=>>=]")
          ((eq level :3) "[=>>=>>=>>=]")
          ))
       ((eq type :error)
        (cond
          ((eq level :0) "[##########]")
          ((eq level :1) "[!!!!!!!!!!]")
          ((eq level :2) "[!!!!!!!!!!]")
          ((eq level :3) "[!!!!!!!!!!]")
          ))))
    ((eq style :parser)
     (format nil "[colecho:~A:~A]" type level))))


(defun colecho-get-color-modifiers (type level)
  (cond
    ((eq type :log)
     (cond
       ((eq level :0) "[0m")
       ((eq level :1) "[0m")
       ((eq level :2) "[0m")
       ((eq level :3) "[0m")))
    ((eq type :info)
     (cond
       ((eq level :0) "[32m")
       ((eq level :1) "[31m")
       ((eq level :2) "[34m")
       ((eq level :3) "[33m")))
    ((eq type :warn)
     (cond
       ((eq level :0) "[1;32m")
       ((eq level :1) "[1;33m")
       ((eq level :2) "[1;93m")
       ((eq level :3) "[1;91m")))
    ((eq type :error)
     (cond
       ((eq level :0) "[1;92m")
       ((eq level :1) "[1;97;41m")
       ((eq level :2) "[1;96;41m")
       ((eq level :3) "[1;92;41m")))))


(defun colecho (input-string
                &key
                  (style :default)
                  (type :log)
                  (level :0))
  (let
      ((color-modifiers
         (if (eq style :parser)
             "[0m"
             (colecho-get-color-modifiers type level)))
       (prefix (colecho-get-prefix style type level)))
    (format
     t "~C~A~A~C[0m ~A"
     #\Esc color-modifiers prefix #\Esc
     input-string)))

(defun main()
  (let ((styles '(:default :verbose :gtest))
        (types '(:log :info :warn :error))
        (levels '(:0 :1 :2 :3)))
    (loop
      for style in styles
      do (loop
           for type in types
           do (loop
                for level in levels
                do (colecho
                    (format nil "~A ~A ~A~%" type level style)
                    :type type
                    :style style
                    :level level))))))


(main)
