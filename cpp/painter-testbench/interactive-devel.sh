#!/usr/bin/env bash

export DISPLAY=:0
Xephyr -resizeable -sw-cursor -retro -ac :1 &
export DISPLAY=:1

pid=0

function run() {
    echo "======================="
    echo "======================="
    echo "======================="
    mkdir -p build
    cd build
    qmake ..
    make --quiet -j12
    ./painter-testbench &
    pid=$!
    cd ..

    echo "======================="
    echo "======================="
    echo "======================="
}

run


echo "monitoring $PWD ..."

inotifywait -m $PWD -e close_write |
    while read -r directory events filename; do
        echo "$events $filename"
        kill $pid
        run
    done

