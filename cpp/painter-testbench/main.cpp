#include <QApplication>
#include <QDebug>
#include <QMainWindow>

#include <QBrush>

#include "listview.hpp"
#include <QRegularExpression>
#include <vector>

QFont        monoFont   = QFont("JetBrains Mono");
QFontMetrics monoMetric = QFontMetrics(monoFont);

QRegularExpression mkRe(QString pattern) {
    return QRegularExpression(pattern);
}

std::vector<std::pair<QRegularExpression, QColor>> colorize = {
    {mkRe("::"), QColor("blue")},
    {mkRe("->"), QColor("orange")}};


void typedDraw(TYPED_DELEGATE_CALLBACK_ARG_SIGNATURE) {
    painter->setFont(monoFont);
    // painter->drawText(option.rect, Qt::AlignVCenter, data.str);
    if (option.state & QStyle::State_MouseOver) {
        painter->fillRect(option.rect, QBrush("black"));
        painter->fillRect(
            option.rect.marginsRemoved(QMargins(1, 1, 1, 1)),
            QBrush("green"));
    } else {
        painter->fillRect(option.rect, QBrush("green"));
    }

    QPointF lastStop = option.rect.topLeft();

    QStringList split = data.str.split(" ");
    for (auto& item : split) {
        bool anyMatch = false;
        for (auto [reg, color] : colorize) {
            auto match = reg.match(item);
            if (match.hasMatch()) {
                qDebug() << "Drawing" << item << "in color" << color;
                anyMatch = true;
                painter->setPen(QPen(color));
                painter->drawText(
                    QRectF(lastStop, option.rect.bottomRight()),
                    Qt::AlignVCenter,
                    item);
            }
        }

        if (!anyMatch) {
            painter->setPen(QPen("black"));
            painter->drawText(
                QRectF(lastStop, option.rect.bottomRight()),
                Qt::AlignVCenter,
                item);
        }

        lastStop = QPointF(
            lastStop.x() + monoMetric.horizontalAdvance(item + " "),
            lastStop.y());

        qDebug() << "Drew" << item << "at" << lastStop;
    }
}

int main(int argc, char* argv[]) {
    QApplication a(argc, argv);
    QMainWindow  w;

    auto list = new ListView();

    list->setItems({makeListData(12, ":: int -> ::"),
                    makeListData(123, "hello :: ->"),
                    makeListData(122, "nice day")});


    //    list->setTypedDelegateCallbac(&typedDraw);
    QBrush color("blue");
    list->setTypedDelegateCallback(
        [color](TYPED_DELEGATE_CALLBACK_ARG_SIGNATURE) {
            painter->fillRect(option.rect, color);
        });

    w.setMinimumSize(400, 600);

    w.setCentralWidget(list);

    w.show();
    return a.exec();
}
