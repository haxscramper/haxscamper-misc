QT       += core gui widgets
CONFIG += c++17
TARGET = painter-testbench

DEFINES += QT_DEPRECATED_WARNINGS
SOURCES += \
    main.cpp

HEADERS += \
    listview.hpp

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

DISTFILES += \
    interactive-devel.sh \
    readme.md
