#ifndef LISTVIEW_DELEGATE_HPP
#define LISTVIEW_DELEGATE_HPP

#include <QAbstractListModel>
#include <QListView>
#include <QModelIndex>
#include <QPainter>
#include <QStyleOptionViewItem>
#include <QStyledItemDelegate>
#include <QVariant>

#include <functional>
#include <vector>


struct ListModelData {
    int     score;
    QString str;
};

inline ListModelData makeListData(int score, QString str) {
    ListModelData res;
    res.score = score;
    res.str   = str;
    return res;
}

Q_DECLARE_METATYPE(ListModelData);

class ListModel : public QAbstractListModel
{
    std::vector<ListModelData> items;

    // QAbstractItemModel interface
  public:
    int rowCount(const QModelIndex& parent
                 [[maybe_unused]]) const override {
        return items.size();
    }
    QVariant data(const QModelIndex& index, int role) const override {
        if (role == Qt::DisplayRole) {
            QVariant result;
            result.setValue(items.at(index.row()));
            return result;
        } else {
            return QVariant();
        }
    }
    void setItems(const std::vector<ListModelData>& value) {
        beginResetModel();
        items = value;
        endResetModel();
    }
};

#define DELEGATE_CALLBACK_ARG_SIGNATURE                                   \
    QPainter *painter, const QStyleOptionViewItem &option,                \
        const QModelIndex &index

#define TYPED_DELEGATE_CALLBACK_ARG_SIGNATURE                             \
    DELEGATE_CALLBACK_ARG_SIGNATURE, ListModelData data

using DelegateCallbackPtr = std::function<void(
    DELEGATE_CALLBACK_ARG_SIGNATURE)>;

using TypedDelegateCallback = std::function<void(
    TYPED_DELEGATE_CALLBACK_ARG_SIGNATURE)>;

class ListItemDelegate : public QStyledItemDelegate
{

    // QAbstractItemDelegate interface
    DelegateCallbackPtr   callback      = nullptr;
    TypedDelegateCallback typedCallback = nullptr;
    QSize                 preferredSize = QSize(120, 40);

  public:
    void setDrawCallback(DelegateCallbackPtr _callback) {
        callback = _callback;
    }

    void setTypedDelegateCallback(TypedDelegateCallback _typedCallback) {
        typedCallback = _typedCallback;
    }

    // QAbstractItemDelegate interface
    void paint(
        QPainter*                   painter,
        const QStyleOptionViewItem& option,
        const QModelIndex&          index) const override {
        if (callback != nullptr) {
            callback(painter, option, index);
        } else if (typedCallback != nullptr) {
            if (index.data().canConvert<ListModelData>()) {
                auto data = qvariant_cast<ListModelData>(index.data());
                typedCallback(painter, option, index, data);
            } else {
                // REVIEW throw error or just write warning to log?
            }
        } else {
            throw std::logic_error(
                "Attempt to draw delegate without setting callback");
        }
    }

    QSize sizeHint(
        const QStyleOptionViewItem& option,
        const QModelIndex&          index) const override {
        return preferredSize;
    }

    void setPreferredItemSize(QSize _size) {
        preferredSize = _size;
    }
};

class ListView : public QListView
{
    Q_OBJECT

    ListModel*        model;
    ListItemDelegate* delegate;

  public:
    ListView() {
        model    = new ListModel();
        delegate = new ListItemDelegate();
        this->setItemDelegate(delegate);
        this->setModel(model);
    }


    void setPreferredItemSize(QSize size) {
        delegate->setPreferredItemSize(size);
    }

    void setDelegateCallback(DelegateCallbackPtr callback) {
        delegate->setDrawCallback(callback);
    }

    void setTypedDelegateCallback(TypedDelegateCallback callback) {
        delegate->setTypedDelegateCallback(callback);
    }

    void setItems(const std::vector<ListModelData>& value) {
        model->setItems(value);
    }
};

#endif // LISTVIEW_DELEGATE_HPP
