
#include "cppcmutils_support.hpp"

std::string dbg_get_std_string(CXString clang_string) {
    return clang_getCString(clang_string);
}

std::string dbg_get_std_string(CXSourceRange range) {
    return "[" + std::to_string(range.begin_int_data) + ","
           + std::to_string(range.end_int_data) + "]";
}

std::string dbg_get_std_string(CXTokenKind tokenKind) {
    switch (tokenKind) {
        case CXToken_Comment: return "CXToken_Comment";
        case CXToken_Identifier: return "CXToken_Identifier";
        case CXToken_Keyword: return "CXToken_Keyword";
        case CXToken_Punctuation: return "CXToken_Punctuation";
        case CXToken_Literal: return "CXToken_Literal";
    }
}
