#include "codenode.hpp"
#include "debug.hpp"
#include <algorithm>
#include <debug_support.hpp>


//#  /////////////////////////////////////////////////////////////////////
//#  CodeNodeList
//#  /////////////////////////////////////////////////////////////////////

NodeIter CodeNodeList::findNodeContainsRange(CXSourceRange range) {
    return std::find_if(nodes.begin(), nodes.end(), [&](auto& node) {
        return node->containsRange(range);
    });
}

uint CodeNodeList::getSourceRangeStart() const {
    if (nodes.size() == 0) {
        return 0;
    } else {
        auto iter = std::max_element(
            nodes.begin(),
            nodes.end(),
            [](const CodeNodeUPtr& lhs, const CodeNodeUPtr& rhs) -> bool {
                return lhs->getSourceRangeStart()
                       > rhs->getSourceRangeStart();
            });
        return (*iter)->getSourceRangeStart();
    }
}

uint CodeNodeList::getSourceRangeEnd() const {
    if (nodes.size() == 0) {
        return 0;
    } else {
        auto iter = std::max_element(
            nodes.begin(),
            nodes.end(),
            [](const CodeNodeUPtr& next, const CodeNodeUPtr& max) -> bool {
                return max->getSourceRangeEnd()
                       > next->getSourceRangeEnd();
            });
        return (*iter)->getSourceRangeEnd();
    }
}

bool CodeNodeList::anyNodeContainsRange(CXSourceRange range) const {
    return std::any_of(nodes.begin(), nodes.end(), [&](auto& node) {
        return node->containsRange(range);
    });
}

//#  /////////////////////////////////////////////////////////////////////
//#  CodeNode
//#  /////////////////////////////////////////////////////////////////////


bool CodeNode::containsRange(CXSourceRange range) const {
    //    DEBUG_INDENT
    //    LOG << "Node contains range?";
    auto [start, stop] = getSourceRange();
    //    LOG_BOOL(
    //        "Node range " << dbg::str(getCXSourceRange()) << "test range"
    //                      << dbg::str(range),
    //        start < range.begin_int_data && range.end_int_data < stop)
    //    DEBUG_DEINDENT
    return start < range.begin_int_data && range.end_int_data < stop;
}

std::pair<uint, uint> CodeNode::getSourceRange() const {
    return sourceRange;
}

CXSourceRange CodeNode::getCXSourceRange() const {
    CXSourceRange range;

    range.begin_int_data = sourceRange.first;
    range.end_int_data   = sourceRange.second;

    return range;
}

uint CodeNode::getSourceRangeStart() const {
    return sourceRange.first;
}

uint CodeNode::getSourceRangeEnd() const {
    return sourceRange.second;
}
