#include <clang-c/Index.h>
#include <debug_support.hpp>
#include <fstream>
#include <string>
#include <tuple>
#include <unordered_map>
#include <variant>

#include "codenode.hpp"
#include "debug.hpp"
#include "tree_builder.hpp"
#include <debuglogger/support.hpp>


int main() {

    CXIndex     index = clang_createIndex(0, 0);
    const char* source_file
        = "/mnt/workspace/Test/qiucus-development/cppcmutils/test.cpp";

    const char* args[] = {
        "-c",
        "-x",
        "c++",
        "-fsyntax-only",
        "-std=c+17",
        "-include=/mnt/workspace/Code/qiucus_development/cppcmutils/"
        "test.cpp",
        //
    };

    CXTranslationUnit unit = clang_parseTranslationUnit(
        index,                          // CIdx
        source_file,                    // source_filename
        args,                           // command_line_args
        sizeof(args) / sizeof(args[0]), // num_command_line_args
        nullptr,                        // unsave_files
        0,                              // num_unsaved_files

        CXTranslationUnit_SingleFileParse | //
            CXTranslationUnit_Incomplete);

    if (unit == nullptr) {
        LOG << "Fuck you";
        exit(-1);
    }

    auto nodes = build_source_tree(unit, source_file);

    CodeNodeList topNodes;

    LOG << "Created " << nodes.size() << "code nodes";

    for (auto& node : nodes) {
        topNodes.insert(std::move(node));
    }

    topNodes.sortRecursive();

    for (auto& node : topNodes) {
        node->printRecursiveSmall();
    }

    clang_disposeTranslationUnit(unit);
    clang_disposeIndex(index);

    LOG << FOREG_RED("ALL DONE");

    return 0;
}
