#include "tree_builder.hpp"
#include "debug.hpp"
#include <algorithm.hpp>
#include <algorithm>
#include <cstring>
#include <memory>

#include "cppcmutils_support.hpp"

using TokenVec  = std::vector<CXToken>;
using TokenIter = std::vector<CXToken>::iterator;

#include <flip_debug_state.hpp>

std::pair<TokenIter, TokenIter> get_include_token_range(
    CXTranslationUnit& unit,
    TokenIter          start) {


    auto include_end = start;

    bool path_start_found = false;
    bool path_end_found   = false;

    while (!(path_start_found && path_end_found)) {
        LOG << FOREG_CYAN("---")
            << clang_getTokenSpelling(unit, *include_end);
        switch (clang_getTokenKind(*include_end)) {
            case CXToken_Punctuation: {
                if (!path_start_found) {
                    if (strcmp(
                            clang_getCString(clang_getTokenSpelling(
                                unit, *include_end)),
                            "<")
                        == 0) {
                        path_start_found = true;
                    }
                    ++include_end;
                } else if (path_start_found && !path_end_found) {
                    if (strcmp(
                            clang_getCString(clang_getTokenSpelling(
                                unit, *include_end)),
                            ">")
                        == 0) {
                        path_end_found = true;
                    } else {
                        ++include_end;
                    }
                } else {
                    ++include_end;
                }
            } break;
            case CXToken_Literal: {
                path_start_found = true;
                path_end_found   = true;
            } break;
            default: { ++include_end; }
        }
    }


    return {start, include_end};
}

/// Extract include starting at first into CodeNode. Advance first to point
/// to the end of the include range
CodeNodeUPtr extract_include(
    CXTranslationUnit& unit,
    TokenIter&         first,
    std::string&       file) {
    LOG << FOREG_RED("-----");
    DEBUG_INDENT

    CodeNodeUPtr res                  = std::make_unique<CodeNode>(unit);
    auto [include_start, include_end] = get_include_token_range(
        unit, first);

    res->setNodeSource(include_start, include_end, file);

    LOG << "Found include directive at range" //
        << res->getSourceRangeStart()         //
        << res->getSourceRangeEnd();
    LOG << res->getNodeSource();

    first = include_end;

    DEBUG_DEINDENT
    LOG << FOREG_RED("-----");

    return res;
}


bool is_preprocessor_start(CXTranslationUnit& unit, TokenIter token) {
    return clang_getTokenKind(*token) == CXToken_Punctuation
           && strcmp(
                  clang_getCString(clang_getTokenSpelling(unit, *token)),
                  "#")
                  == 0;
}

bool is_include_directive(CXTranslationUnit& unit, TokenIter token) {
    ++token;
    return clang_getTokenKind(*token) == CXToken_Identifier
           && strcmp(
                  clang_getCString(clang_getTokenSpelling(unit, *token)),
                  "include")
                  == 0;
}


bool is_comment(const TokenIter& token) {
    return clang_getTokenKind(*token) == CXToken_Comment;
}

bool is_ignored_comment(
    [[maybe_unused]] CXTranslationUnit& unit,
    [[maybe_unused]] const TokenIter&   token,
    [[maybe_unused]] std::string&       file) {
    return false;
}


CodeNodeUPtr extract_comment(
    CXTranslationUnit& unit,
    const TokenIter&   token,
    std::string&       file) {
    CodeNodeUPtr res = std::make_unique<CodeNode>(unit);
    res->setNodeSource(*token, file);

    LOG << FOREG_YELLOW("-----");
    DEBUG_INDENT
    LOG << FOREG_CYAN("---") << res->getNodeSource();
    DEBUG_DEINDENT
    LOG << FOREG_YELLOW("-----");

    return res;
}

#include <flip_debug_state.hpp>

std::vector<CodeNodeUPtr> extract_tokens(
    CXTranslationUnit&    unit,
    std::vector<CXToken>& tokens,
    std::string&          file) {
    std::vector<CodeNodeUPtr> res;

    auto first = tokens.begin();
    auto last  = tokens.end();


    while (first != last) {
        if (is_preprocessor_start(unit, first)) {
            if (is_include_directive(unit, first)) {
                res.push_back(extract_include(unit, first, file));
            }
            ++first;
        } else if (
            is_comment(first) && !is_ignored_comment(unit, first, file)) {
            res.push_back(extract_comment(unit, first, file));
            ++first;
        } else {
            ++first;
        }
    }

    return res;
}


std::vector<CodeNodeUPtr> build_source_tree(
    CXTranslationUnit& unit,
    const char*        source_file) {


    struct Source {
        Source(CXTranslationUnit& _unit) : unit(_unit) {
        }
        std::string                             str;
        std::unordered_map<CXCursor, CodeNode*> map;
        std::vector<CXCursorKind>               acceptedCursors;
        std::vector<CodeNodeUPtr>               nodes;
        CXTranslationUnit&                      unit;
    } source(unit);


    std::ifstream ifs(source_file);
    source.str.assign(
        (std::istreambuf_iterator<char>(ifs)),
        (std::istreambuf_iterator<char>()));

    source.acceptedCursors = {
        CXCursor_CXXMethod,
        CXCursor_Namespace,
        CXCursor_ClassDecl,
        CXCursor_EnumDecl,
        CXCursor_FieldDecl,
        CXCursor_Destructor,
        CXCursor_Constructor,
        CXCursor_CXXAccessSpecifier,
        CXCursor_FunctionDecl,
        CXCursor_PreprocessingDirective,
        CXCursor_MacroDefinition,
        CXCursor_InclusionDirective
        //
    };

    CXCursor cursor = clang_getTranslationUnitCursor(unit);

    clang_visitChildren(
        cursor,
        [](CXCursor                  cursor,
           [[maybe_unused]] CXCursor parent,
           CXClientData              client_data) {
            Source* file = static_cast<Source*>(client_data);
            LOG << "---" << clang_getCursorSpelling(cursor);
            if (spt::contains(
                    file->acceptedCursors, clang_getCursorKind(cursor))) {
                auto node = std::make_unique<CodeNode>(file->unit);
                node->setNodeSource(cursor, file->str);
                file->map[cursor] = node.get();
                file->nodes.push_back(std::move(node));
            }
            return CXChildVisit_Recurse;
        },
        &source);

    CXSourceRange unitRange = clang_getCursorExtent(
        clang_getTranslationUnitCursor(unit));
    CXToken* tokens_arr;
    uint     numTokens;

    clang_tokenize(unit, unitRange, &tokens_arr, &numTokens);

    std::vector<CXToken> tokens_vect = std::vector<CXToken>(
        tokens_arr, tokens_arr + numTokens);

    spt::concatenate_move(
        source.nodes, extract_tokens(unit, tokens_vect, source.str));

    return std::move(source.nodes);
}

bool operator==(const CXCursor& lhs, const CXCursor& rhs) {
    return clang_equalCursors(lhs, rhs);
}
