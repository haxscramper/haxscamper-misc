#ifndef DEBUG_CPPCMUTILS_HPP
#define DEBUG_CPPCMUTILS_HPP

#include <clang-c/Index.h>
#include <string>
#include <debug_support.hpp>
#include <fstream>
#include <functional>
#include <list>
#include <memory>

class CodeNode;
class CodeNodeList;

namespace dbg {
void print_token_cursor(
    CXToken            token,
    CXCursor           cursor,
    std::string&       file_content,
    CXTranslationUnit& unit);

void print_all_tokens_cursors(
    CXTranslationUnit& unit,
    const char*        source_file);

void print_cursor_to_table(
    int          table_index,
    CXCursor&    cursor,
    std::string& file);

void print_node_to_table(int table_index, CodeNode* node);

void print_all_cursors(CXTranslationUnit& unit, const char* source_file);
void print_node_ranges(std::list<std::unique_ptr<CodeNode>>& list);
void print_node_ranges(const CodeNodeList& list);

std::string str(CXSourceRange range);


/*! \todo Make into working function
template <class Container>
std::string str(
    const Container& container,
    typename std::function<std::string(typename Container::value_type)>
         converter,
    bool multiline = true) {
    std::string                  result = multiline ? "[ \n" : "[ ";
    typename Container::iterator begin  = container.begin();
    typename Container::iterator end    = container.end();

    while (begin != end) {
        result += multiline ? "  " : "" + converter(*begin);
        begin++;
    }


    result.erase(result.size() - 2, 2);
    result += multiline ? " ]" : " ]";
    return result;
}
*/


} // namespace dbg


#endif // DEBUG_CPPCMUTILS_HPP
