set(CMAKE_C_COMPILER /usr/bin/clang)
set(CMAKE_CXX_COMPILER /usr/bin/clang++)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++14")
set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -Wall")

set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS OFF)

file(GLOB sources
    "main.cpp"
    "../DebugMacro/debug_support.cpp"
    "../DebugMacro/debuglogger/*.cpp"
)

include_directories("../Algorithm")
include_directories("../DebugMacro")
include_directories("/usr/include/clang-c")
add_executable(main ${sources})

link_directories(/usr/lib)
target_link_libraries(main clang)

