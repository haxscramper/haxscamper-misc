//===    STL   ===//
#include <map>
#include <unordered_set>
#include <vector>

//=== Sibling  ===//
#include "../ldap_features.h"
#include "../security/_pam_macros.h"

//===   Boost  ===//
#include <boost/utility/detail/minstd_rand.hpp>


class TestClass
{
  public:
    TestClass() {
    }
    virtual ~TestClass() {
    }
};
