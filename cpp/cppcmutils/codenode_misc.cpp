#include <codenode.hpp>

//#  /////////////////////////////////////////////////////////////////////
//#  CodeNodeList
//#  /////////////////////////////////////////////////////////////////////

NodeList::const_iterator CodeNodeList::begin() const {
    return nodes.begin();
}

NodeList::const_iterator CodeNodeList::end() const {
    return nodes.end();
}

NodeIter CodeNodeList::begin() {
    return nodes.begin();
}

NodeIter CodeNodeList::end() {
    return nodes.end();
}

CodeNode* CodeNodeList::front() const {
    return nodes.size() == 0 ? nullptr : (*nodes.begin()).get();
}

CodeNode* CodeNodeList::back() const {
    return nodes.size() == 0 ? nullptr : (*(--nodes.end())).get();
}

bool CodeNodeList::isEmpty() const {
    return nodes.size() == 0;
}

NodeIter CodeNodeList::erase(NodeIter first, NodeIter last) {
    return nodes.erase(first, last);
}

CodeNodeList::CodeNodeList(CodeNodeList&& other) {
    nodes = std::move(other.nodes);
}

CodeNodeList& CodeNodeList::operator=(CodeNodeList&& other) {
    nodes = std::move(other.nodes);
    return *this;
}


size_t CodeNodeList::size() const {
    return nodes.size();
}

//#  /////////////////////////////////////////////////////////////////////
//#  CodeNode
//#  /////////////////////////////////////////////////////////////////////

std::string CodeNode::getNodeSource() const {
    return nodeSource;
}

std::string CodeNode::getNodeKindSpelling() const {
    return "None";
}

void CodeNode::setNodeSource(const std::string& source) {
    nodeSource = source;
}
