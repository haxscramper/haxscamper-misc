#ifndef TREE_BUILDER_HPP
#define TREE_BUILDER_HPP

#include <clang-c/Index.h>
#include <debug_support.hpp>
#include <unordered_map>
#include <fstream>
#include "codenode.hpp"


namespace std {
template <>
struct hash<CXCursor> {
    std::size_t operator()(const CXCursor& item) const {
        return clang_hashCursor(item);
    }
};
} // namespace std

bool operator==(const CXCursor& lhs, const CXCursor& rhs);

std::vector<CodeNodeUPtr> build_source_tree(
    CXTranslationUnit& unit,
    const char*        source_file);

#endif // TREE_BUILDER_HPP
