/*!
 * \file codenode.hpp
 * \brief
 */

#ifndef CODENODE_HPP
#define CODENODE_HPP

//===    STL   ===//
#include <list>
#include <memory>
#include <optional>
#include <set>
#include <string>
#include <variant>
#include <vector>

//===   Other  ===//
#include <clang-c/Index.h>


//  ////////////////////////////////////

class CodeNode;

using CodeNodeUPtr = std::unique_ptr<CodeNode>;
using NodeList     = std::list<CodeNodeUPtr>;
using NodeIter     = NodeList::iterator;

/*!
 * \brief The CodeNodeList class represents ordered nested list of nodes
 * with non-overlapping ranges.
 */
class CodeNodeList
{
  public:
    CodeNodeList()                          = default;
    CodeNodeList(const CodeNodeList& other) = delete;
    CodeNodeList(CodeNodeList&& other);
    CodeNodeList& operator=(CodeNodeList&& other);
    bool          anyNodeContainsRange(CXSourceRange range) const;
    NodeIter      findNodeContainsRange(CXSourceRange range);
    uint          getSourceRangeStart() const;
    uint          getSourceRangeEnd() const;
    CXSourceRange getSourceRange() const;
    size_t        size() const;
    void          sortRecursive();

    void insert(CodeNodeUPtr node);

    NodeList::const_iterator begin() const;
    NodeList::const_iterator end() const;

    NodeIter  begin();
    NodeIter  end();
    CodeNode* front() const;
    CodeNode* back() const;
    bool      isEmpty() const;
    NodeIter  erase(NodeIter first, NodeIter last);


    std::tuple< //
        std::remove_const<NodeIter>::type,
        std::remove_const<NodeIter>::type,
        bool>
        findFirstLastInRange(CXSourceRange range);


  private:
    NodeList nodes;
};

/*!
 * \brief The CodeNode class represents range of source code and nested
 * source nodes.
 */
class CodeNode
{
  public:
    CodeNode(CXTranslationUnit& _unit);
    void                  setCursor(CXCursor cursor);
    void                  setToken(CXToken token);
    void                  setParentNode(CodeNode* _parent);
    std::pair<uint, uint> getSourceRange() const;
    CXSourceRange         getCXSourceRange() const;
    uint                  getSourceRangeStart() const;
    uint                  getSourceRangeEnd() const;
    void                  setNodeSource(const std::string& source);
    std::string           getNodeKindSpelling() const;
    std::string           getNodeSource() const;
    std::string           getSourceHead() const;
    std::string           getSourceTail() const;
    void                  sortRecursive();
    bool                  operator<(const CodeNode& other) const;
    void                  setNodeSource(
                         std::string::iterator sourceStart,
                         std::string::iterator sourceEnd);

    void setNodeSource(const CXCursor& cursor, std::string& file);

    void setNodeSource(const CXToken& token, std::string& file);

    void setNodeSource(
        const CXToken& start,
        const CXToken& end,
        std::string&   file);

    void setNodeSource(
        const std::vector<CXToken>::iterator& token,
        std::string&                          file);

    void setNodeSource(
        const std::vector<CXToken>::iterator& start,
        const std::vector<CXToken>::iterator& end,
        std::string&                          file);

    void insertChildNode(CodeNodeUPtr child);
    void printRecursive() const;
    void printRecursiveSmall() const;
    bool containsRange(CXSourceRange range) const;

  private:
    std::pair<size_t, size_t>   sourceRange;
    CXTranslationUnit&          unit;
    std::optional<CXCursorKind> cursorKind;
    CodeNodeList                childNodes;
    CodeNode*                   parent;
    std::string                 nodeSource;
};


#endif // CODENODE_HPP
