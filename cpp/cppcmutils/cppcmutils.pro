QT -= gui core

CONFIG   += depend_includepath
CONFIG   += object_parallel_to_source
DEFINES  += QT_DEPRECATED_WARNINGS
CONFIG   += console

QMAKE_CXXFLAGS += -std=c++17
SOURCES += \
    $$PWD/main.cpp \
    $$PWD/codenode.cpp \
    $$PWD/tree_builder.cpp \
    $$PWD/debug.cpp \
    $$PWD/codenode_rangeopers.cpp \
    $$PWD/codenode_misc.cpp \
    cppcmutils_support.cpp

include($$PWD/../DebugMacro/debug.pri)
include($$PWD/../Algorithm/algorithm.pri)


INCLUDEPATH *= /usr/include/clang-c
LIBS += -L/usr/lib -lclang


HEADERS += \
    codenode.hpp    \
    debug.hpp       \
    tree_builder.hpp \
    cppcmutils_support.hpp

DISTFILES += \
    notes.adoc \
    test.cpp

