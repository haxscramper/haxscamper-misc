#include "debug.hpp"
#include "codenode.hpp"


void dbg::print_all_tokens_cursors(
    CXTranslationUnit& unit,
    const char*        source_file) {
    DEBUG_TABLE(2).setHorizontalLines(DBGPartVisibility::Hide);
    DEBUG_TABLE(2).setVerticalLines(DBGPartVisibility::EraseEdges);

    CXToken*      tokens;
    unsigned int  num_tokens;
    CXSourceRange range = clang_getCursorExtent(
        clang_getTranslationUnitCursor(unit));
    clang_tokenize(unit, range, &tokens, &num_tokens);
    LOG << "Created " << num_tokens << "tokens";
    LOG << "From range " << range.begin_int_data << range.end_int_data;

    CXCursor cursors[num_tokens];
    clang_annotateTokens(unit, tokens, num_tokens, cursors);
    LOG << "Created cursors";

    DEBUG_TABLE(2).lastRow(0) << "Token type";
    DEBUG_TABLE(2).lastRow(1) << "Range";
    DEBUG_TABLE(2).lastRow(2) << "Token text";
    DEBUG_TABLE(2).lastRow(3) << "Cursor";
    DEBUG_TABLE(2).lastRow(4) << "Range";
    DEBUG_TABLE(2).lastRow(5) << "Cursor text";

    std::ifstream ifs(source_file);
    std::string   file_content(
        (std::istreambuf_iterator<char>(ifs)),
        (std::istreambuf_iterator<char>()));

    for (size_t i = 0; i < num_tokens; ++i) {
        dbg::print_token_cursor(tokens[i], cursors[i], file_content, unit);
    }

    DEBUG_PRINT_TABLE(2)
}

void dbg::print_cursor_to_table(
    int          table_index,
    CXCursor&    cursor,
    std::string& file) {
    DEBUG_TABLE(table_index).appendRow();
    DEBUG_TABLE(table_index).lastRow(0)
        << clang_getCString(clang_getCursorSpelling(cursor));

    DEBUG_TABLE(table_index).lastRow(1) << clang_getCString(
        clang_getCursorKindSpelling(clang_getCursorKind(cursor)));

    CXSourceRange range = clang_getCursorExtent(cursor);

    DEBUG_TABLE(table_index).lastRow(2) << "Starts at" <<= //
        range.begin_int_data;
    DEBUG_TABLE(table_index).lastRow(2) << "Ends at  " <<= //
        range.end_int_data;

    DEBUG_TABLE(table_index).lastRow(3) << std::string(
        file.begin() + range.begin_int_data - 2,
        file.begin() + range.end_int_data - 2);
}

void dbg::print_all_cursors(
    CXTranslationUnit& unit,
    const char*        source_file) {
    CXCursor cursor = clang_getTranslationUnitCursor(unit);

    std::ifstream ifs(source_file);
    std::string   file_content(
        (std::istreambuf_iterator<char>(ifs)),
        (std::istreambuf_iterator<char>()));

    struct File {
        std::string* str;
    } file;

    file.str = &file_content;

    clang_visitChildren(
        cursor,
        [](CXCursor                  c,
           [[maybe_unused]] CXCursor parent,
           CXClientData              client_data) {
            File* file = static_cast<File*>(client_data);
            DEBUG_TABLE(1).appendRow();
            DEBUG_TABLE(1).lastRow(0)
                << clang_getCString(clang_getCursorSpelling(c));

            DEBUG_TABLE(1).lastRow(1) << clang_getCString(
                clang_getCursorKindSpelling(clang_getCursorKind(c)));

            CXSourceRange range = clang_getCursorExtent(c);

            DEBUG_TABLE(1).lastRow(2) << "Starts at" <<= //
                range.begin_int_data;
            DEBUG_TABLE(1).lastRow(2) << "Ends at  " <<= //
                range.end_int_data;

            DEBUG_TABLE(1).lastRow(3) << std::string(
                file->str->begin() + range.begin_int_data - 2,
                file->str->begin() + range.end_int_data - 2);


            return CXChildVisit_Recurse;
        },
        &file);

    DEBUG_PRINT_TABLE(1)
}

void dbg::print_token_cursor(
    CXToken            token,
    CXCursor           cursor,
    std::string&       file_content,
    CXTranslationUnit& unit) {
    DEBUG_TABLE(2).appendRow();

    switch (clang_getTokenKind(token)) {
        case CXToken_Punctuation: {
            // continue;
            DEBUG_TABLE(2).lastRow(0) << "Punctuation";
        } break;
        case CXToken_Keyword: {
            DEBUG_TABLE(2).lastRow(0) << "Keyword";
        } break;
        case CXToken_Identifier: {
            DEBUG_TABLE(2).lastRow(0) << "Identifier";
        } break;
        case CXToken_Literal: {
            DEBUG_TABLE(2).lastRow(0) << "Literal";
        } break;
        case CXToken_Comment: {
            DEBUG_TABLE(2).lastRow(0) << "Comment";
        } break;
    }

    CXSourceRange token_range = clang_getTokenExtent(unit, token);
    DEBUG_TABLE(2).lastRow(1)
        << token_range.begin_int_data << token_range.end_int_data;
    DEBUG_TABLE(2).lastRow(2)
        << clang_getCString(clang_getTokenSpelling(unit, token));


    DEBUG_TABLE(2).lastRow(3)
        << clang_getCString(
               clang_getCursorKindSpelling(clang_getCursorKind(cursor)))
        << "Is declaration"
        <<= dbg::str(clang_isDeclaration(clang_getCursorKind(cursor)));

    CXSourceRange cursor_range = clang_getCursorExtent(cursor);

    DEBUG_TABLE(2).lastRow(4)
        << cursor_range.begin_int_data << cursor_range.end_int_data;

    std::string cursor_text;
    cursor_text.assign(
        file_content.begin() + (cursor_range.begin_int_data - 2),
        file_content.begin() + (cursor_range.end_int_data - 2));

    DEBUG_TABLE(2).lastRow(5) << cursor_text;
}

void dbg::print_node_to_table(int table_index, CodeNode* node) {
    DEBUG_TABLE(table_index).appendRow();
    DEBUG_TABLE(table_index).lastRow(0) << node->getNodeKindSpelling();

    CXSourceRange range = node->getCXSourceRange();

    DEBUG_TABLE(table_index).lastRow(1) << "Starts at" <<= //
        range.begin_int_data;
    DEBUG_TABLE(table_index).lastRow(2) << "Ends at  " <<= //
        range.end_int_data;

    DEBUG_TABLE(table_index).lastRow(3) << node->getNodeSource();
}

std::string dbg::str(CXSourceRange range) {
    return "[ " + std::to_string(range.begin_int_data) + " , "
           + std::to_string(range.end_int_data) + " ]";
}

void dbg::print_node_ranges(std::list<std::unique_ptr<CodeNode>>& list) {
    for (const std::unique_ptr<CodeNode>& node : list) {
        LOG << dbg::str(node->getCXSourceRange());
    }
}

void dbg::print_node_ranges(const CodeNodeList& list) {
    for (const std::unique_ptr<CodeNode>& node : list) {
        LOG << dbg::str(node->getCXSourceRange());
    }
}
