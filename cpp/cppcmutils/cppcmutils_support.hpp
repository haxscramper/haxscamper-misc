#ifndef CPPCMUTILS_SUPPORT_HPP
#define CPPCMUTILS_SUPPORT_HPP

#include <Index.h>
#include <string>

std::string dbg_get_std_string(CXString clang_string);

std::string dbg_get_std_string(CXSourceRange range);

std::string dbg_get_std_string(CXTokenKind tokenKind);

#endif // CPPCMUTILS_SUPPORT_HPP
