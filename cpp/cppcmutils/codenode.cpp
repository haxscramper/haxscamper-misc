#include "codenode.hpp"
#include "debug.hpp"
#include <algorithm.hpp>
#include <algorithm>
#include <debug_support.hpp>

//#  /////////////////////////////////////////////////////////////////////
//#  CodeNode
//#  /////////////////////////////////////////////////////////////////////

CodeNode::CodeNode(CXTranslationUnit& _unit) : unit(_unit) {
}

void CodeNode::setParentNode(CodeNode* _parent) {
    parent = _parent;
}


std::string CodeNode::getSourceHead() const {
    auto [start, end] = getSourceRange();
    if (childNodes.size() == 0) {
        return nodeSource;
    } else {
        int childStart = childNodes.getSourceRangeStart();
        return {nodeSource.begin(),
                nodeSource.begin() + childStart - start};
    }
}

std::string CodeNode::getSourceTail() const {
    auto [start, end] = getSourceRange();
    if (childNodes.size() == 0) {
        return "";
    } else {
        int childEnd = childNodes.getSourceRangeEnd();
        return {nodeSource.begin() + childEnd - start,
                nodeSource.begin() + end - start};
    }
}

void CodeNode::sortRecursive() {
    childNodes.sortRecursive();
}

bool CodeNode::operator<(const CodeNode& other) const {
    return getSourceRangeEnd() < other.getSourceRangeStart();
}

void CodeNode::setNodeSource(
    std::string::iterator sourceStart,
    std::string::iterator sourceEnd) {
    nodeSource.assign(sourceStart, sourceEnd);
}

void CodeNode::setNodeSource(const CXCursor& cursor, std::string& file) {
    LOG << "Settings node source";

    sourceRange.first  = clang_getCursorExtent(cursor).begin_int_data - 2;
    sourceRange.second = clang_getCursorExtent(cursor).end_int_data - 2;

    nodeSource.assign(
        file.begin() + sourceRange.first,
        file.begin() + sourceRange.second);

    LOG << "Node source";
    LOG << getNodeSource();
}

void CodeNode::setNodeSource(const CXToken& token, std::string& file) {
    sourceRange.first = clang_getTokenExtent(unit, token).begin_int_data
                        - 2;
    sourceRange.second = clang_getTokenExtent(unit, token).end_int_data
                         - 2;

    nodeSource.assign(
        file.begin() + sourceRange.first,
        file.begin() + sourceRange.second);
}


void CodeNode::setNodeSource(
    const CXToken& start,
    const CXToken& end,
    std::string&   file) {
    sourceRange.first = clang_getTokenExtent(unit, start).begin_int_data
                        - 2;
    sourceRange.second = clang_getTokenExtent(unit, end).end_int_data - 2;

    nodeSource.assign(
        file.begin() + sourceRange.first,
        file.begin() + sourceRange.second);
}


void CodeNode::setNodeSource(
    const std::vector<CXToken>::iterator& token,
    std::string&                          file) {
    sourceRange.first = clang_getTokenExtent(unit, *token).begin_int_data
                        - 2;
    sourceRange.second = clang_getTokenExtent(unit, *token).end_int_data
                         - 2;

    nodeSource.assign(
        file.begin() + sourceRange.first,
        file.begin() + sourceRange.second);
}


void CodeNode::setNodeSource(
    const std::vector<CXToken>::iterator& start,
    const std::vector<CXToken>::iterator& end,
    std::string&                          file) {
    sourceRange.first = clang_getTokenExtent(unit, *start).begin_int_data
                        - 2;
    sourceRange.second = clang_getTokenExtent(unit, *end).end_int_data - 2;

    nodeSource.assign(
        file.begin() + sourceRange.first,
        file.begin() + sourceRange.second);
}


void CodeNode::insertChildNode(CodeNodeUPtr child) {
    childNodes.insert(std::move(child));
}

void CodeNode::printRecursive() const {

    LOG << FOREG_RED("==") << getSourceHead();
    DEBUG_INDENT

    for (auto& node : childNodes) {
        node->printRecursive();
    }

    DEBUG_DEINDENT
    LOG << FOREG_GREEN("==") << getSourceTail();
}

void CodeNode::printRecursiveSmall() const {
    DEBUG_INDENT

    LOG << dbg::str(getCXSourceRange());
    LOG << "Head" << FOREG_CYAN("---");
    LOG << getSourceHead();

    for (auto& node : childNodes) {
        node->printRecursiveSmall();
    }

    LOG << "Tail" << FOREG_CYAN("---");
    LOG << getSourceTail();
    DEBUG_DEINDENT
}


//#  /////////////////////////////////////////////////////////////////////
//#  CodeNodeList
//#  /////////////////////////////////////////////////////////////////////


void CodeNodeList::sortRecursive() {
    if (nodes.size() != 0) {
        nodes.sort([](const CodeNodeUPtr& lhs, const CodeNodeUPtr& rhs) {
            return *lhs < *rhs;
        });

        for (auto& node : nodes) {
            node->sortRecursive();
        }
    }
}


/*!
 * \brief Insert node into the list. If node is child of any node in the
 * list it will be inserted as child node
 */
void CodeNodeList::insert(CodeNodeUPtr node) {
    auto bestFit = std::find_if(
        nodes.begin(), nodes.end(), [&](CodeNodeUPtr& test) -> bool {
            return test->containsRange(node->getCXSourceRange());
        });

    if (bestFit == nodes.end()) {
        auto iter = std::find_if(
            nodes.begin(), nodes.end(), [&](const CodeNodeUPtr& lhs) {
                return lhs->getSourceRange().second
                       < node->getSourceRange().first;
            });

        if (iter != nodes.end()) {
            node->getNodeSource();
        }

        nodes.insert(iter++, std::move(node));
    } else {
        (*bestFit)->insertChildNode(std::move(node));
    }
}

std::tuple< //
    std::remove_const<NodeIter>::type,
    std::remove_const<NodeIter>::type,
    bool>
    CodeNodeList::findFirstLastInRange(CXSourceRange range) {
    NodeIter start = nodes.begin();

    if (start == nodes.end()) {
        return {nodes.end(), nodes.end(), false};
    }

    NodeIter end = --nodes.end();

    while ((*start)->getSourceRangeStart() < range.begin_int_data) {
        ++start;
        if (start == nodes.end()) {
            return {nodes.end(), nodes.end(), false};
        }
    }

    while ((*end)->getSourceRangeEnd() > range.end_int_data) {
        --end;
    }

    return {start, end, true};
}
