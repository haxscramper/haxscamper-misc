#include <iostream>

#include <QAbstractListModel>
#include <QApplication>
#include <QDebug>
#include <QElapsedTimer>
#include <QKeyEvent>
#include <QLabel>
#include <QLineEdit>
#include <QListView>
#include <QMainWindow>
#include <QSortFilterProxyModel>
#include <QVBoxLayout>
#include <QVariant>
#include <QWidget>
#include <QWindow>

#include "debuginator_fuzzy.hpp"
#include "fuzzy_match.hpp"


#include <algorithm>
#include <fstream>
#include <iostream>
#include <unordered_map>
#include <vector>

#include "common.hpp"
#include "fuzzysearchwidget.hpp"
#include "textinputfield.hpp"


int gui_main() {
    int          argc = 0;
    char**       argv = {};
    QApplication app(argc, argv);
    QMainWindow  window;

    strvec dictionary;

    { // Load dataset from file
        std::ifstream infile("/tmp/thefile.txt");
        str           buf;
        while (std::getline(infile, buf)) {
            dictionary.push_back(buf);
        }
        qDebug() << "Filtering on " << dictionary.size() << " items\n";
    }

    let search = new FuzzySearchWidget();
    search->setDictionary(dictionary);
    search->setItemDelegate(new ListItemDelegate);

    window.setCentralWidget(search);

    window.show();
    return app.exec();
    return 0;
}

int main() {

    return gui_main();
    //    return cli_main();
}
