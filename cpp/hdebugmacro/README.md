DebugMacro

Collection of debugging macro and assertions. Supports universal
indentation, background and foreground text coloring.

![image](example.png)

### Usage example:

````
#include "debug_support.hpp"

void increaseValue(int& i) {
    DEBUG_FUNCTION_BEGIN
    i++;
    DEBUG_FUNCTION_END
}

void recursiveFunction(int i) {
    DEBUG_FUNCTION_BEGIN
    LOG << "Doing something";
    if (i < 5) {
        increaseValue(i);
        recursiveFunction(i);
    } else {
        LOG << BACK_RED("Exiting recursion");
    }
    DEBUG_FUNCTION_END
}


int main() {
    recursiveFunction(0);

    DEBUG_SOFT_ASSERT_TRUE(1 == 2)
    assert(1 == 2);
}

````

### Quick reference

| Macro                                 | Description                                                                                   |
|:--------------------------------------|:----------------------------------------------------------------------------------------------|
| DEBUG_FUNCTION_END                    | Decrease indentation and print `Success`                                                      |
| DEBUG_FUNCTION_BEGIN                  | Increase indetation and pring name of the function                                            |
| BACK_BLACK(text),BACK_RED(text) ...   | Use when building loggins message to change background color                                  |
| FOREG_BLACK(text),FOREG_RED(text) ... | Use when building loggins message to change foreground color                                  |
| DEBUG_CALL_INDENTED                   | Increase indentation, execute expression and decrease indentation                             |
| DEBUG_FUNCTION_PRE_RETURN             | Print message and decrease indentation. Use when function has multiple `return` statements    |
| DEBUG_EXPRESSION                      | Print text of the expression, call it with standart indentation and print success after that. |
| DEBUG_ABORT_IF_CALLED                 | Print position in code and call `abort()`                                                     |
| DEBUG_WARN_IF_CALLED                  | Print warning message if called                                                               |
| DEBUG_BASE_CLASS_FUNCTION_WARNING     | Print warning message and funciton name                                                       |
| DEBUG_ASSERT_TRUE                     | If expression evaluated as false print error message and call `abort()`                       |
| DEBUG_ASSERT_FALSE                    | If expression evaluated as true print error message and call `abort()`                        |
| DEBUG_SOFT_ASSERT_TRUE                | If expression evaluated as false print error message.                                         |
| DEBUG_SOFT_ASSERT_FALSE               | If expression evaluated as true print error message.                                          |
| LOG_ONCE(message)                     | If called two or more times in a row with the same arguments ignore all but first call        |
| LOG << ... << ... ;                   | Build debug message                                                                           |

### How to use

This library depends on Qt5 framework for checking if file exist and
showing debug table window (will add example later) and overall indended to
use in Qt projects although ist is quite possible to use it in regular ones
(I will separate it later) because printing is done via `std::cout`. To use
this library you should add `include ($$PWD/debug.pri)` to project file and
add `CONFIG += c++14 console`.

For those who use QtCreator: in some cases (when your just added `console`
to config) it might not get recognised and you will be getting output in
IDE's console. This can be solved by relaunching IDE.

### Suggested reading

+ [https://misc.flogisoft.com/bash/tip_colors_and_formatting](Bash tips: Colors and formatting (ANSI/VT100 Control sequences))
+ [http://terminal-color-builder.mudasobwa.ru](Color Builder for Terminal)



### Todo

- [ ] Check support in most popular terminal emulators
- [ ] Use Buck to generate single include header
    - [https://hackernoon.com/generating-a-single-include-c-header-file-using-buck-827f20be3f9d](buck tuttorial)
- [ ] Add support for using c-style formatting syntax in debug message builder
- [ ] Add support for pretty formatting lists depending on size of the terminal
- [ ] Add support for printing markdown-friendly tables (and possibly adoc) tables too
- [ ] Use [http://ditaa.sourceforge.net/](ditaa) to generate table images
- [ ] Setting table headers and printing them with bold (default formatting)
- [ ] Setting default settings for all new tables
- [ ] Save table to file
- [ ] Remembering indexes of all tables might become very annying when there are more than five of them. It is mandatopry to come up with a solution to resolve this issue
- [ ] Add style settings to select style of the vertical edges(upper part of the table)


#### TO fix

- [ ] Styling works incorrectly in debug table
- [ ] Of macro prefixes are used they increase indentation by two times
- [ ] Backgroudn does not work in the debug table

#### Add support for aligning values in debug table.

For example we have the following code:

```c++
DEBUG_TABLE(1).lastRow(2) << "Starts at" <<= //
    range.begin_int_data;
DEBUG_TABLE(1).lastRow(2) << "Ends at  " <<= //
    range.end_int_data;
```

Currently we have to manually add spaces for table to look good. In some
cases this might be not very convinient way to setupt debug messages,
especially in cases where we have a lot of lines in the same cell. It might
be possible to overload operator `&&` for this pruposes to provide
indentation. In that case code from above might be rewritten as.

```c++
DEBUG_TABLE(1).lastRow(2) << "Starts at" && //
    range.begin_int_data;
DEBUG_TABLE(1).lastRow(2) << "Ends at" && //
    range.end_int_data;
```


### Note

I haven't tested this library on any system other than Ubuntu xenial and
manjaro linux. It uses only Qt and standart library so it should work on
any system that supports those two even though windows has different
coloring tools and you will not get anything other than indentation and
table.
