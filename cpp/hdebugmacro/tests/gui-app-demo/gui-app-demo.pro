include($$PWD/../debug.pri)

QT       += core gui widgets

TARGET = gui-app-demo
TEMPLATE = app

DEFINES += QT_DEPRECATED_WARNINGS

SOURCES += \
        main.cpp \
        mainwindow.cpp

HEADERS += \
        mainwindow.hpp
