//#define DISABLE_ALL
#ifndef DISABLE_ALL

//#    define TABLE_TEST
//#    define MULTIPLE_DEFINITION_ERROR_TEST
//#    define INDENTATION_TEST
//#    define DEBUG_DISABLE_TEST
//#    define TEST_SOFT_ASSERTIONS
//#define QT_TABLE_TEST
//#    define PREFIX_SWAP_DEMO
#    define LOGGING_TEST
//#    define STYLING_EXTRA_TEST
//#    define TABLE_STYLING_TEST

#endif

#ifdef LOGGING_TEST
#    include <hdebugmacro/logging.hpp>

void function_begin_end_print() {
    DEBUG_FUNCTION_BEGIN
    DEBUG_FUNCTION_END
}
#endif

#ifdef DEBUG_DISABLE_TEST
#    include <hdebugmacro/logging.hpp>


void recursive_function_with_debug(int level = 0) {
    DEBUG_DISABLE_IF(level > 5);
    DEBUG_FUNCTION_BEGIN;

    if (level < 10) {
        recursive_function_with_debug(level + 1);
    }

    DEBUG_ENABLE_IF(level <= 5);
    DEBUG_FUNCTION_END;
}
#endif

#ifdef STYLING_EXTRA_TEST
#    include <hdebugmacro/style_macro_extra.hpp>
#endif


#ifdef TABLE_TEST
#    include <hdebugmacro/table.hpp>
#endif

#ifdef TABLE_STYLING_TEST
#    define DMS_USE_FAT_TABLE
#    include <hdebugmacro/style_macro_extra.hpp>
#    include <hdebugmacro/table.hpp>
#endif

#ifdef MULTIPLE_DEFINITION_ERROR_TEST
#    include "demo.hpp"
#endif


#ifdef QT_TABLE_TEST
#    include <QApplication>
#    include <hdebugmacro/all.hpp>
#endif

#ifdef TEST_SOFT_ASSERTIONS
#    include <hdebugmacro/assertions.hpp>

struct Test {
    static void func_test() {
        DEBUG_BASE_CLASS_FUNCTION_WARNING
    }
};

int debug_expresion_return() {
    DEBUG_EXPRESSION_RETURN(2 + 2)
}
#endif
#include <hdebugmacro/logging.hpp>

/// \todo Add easy log class in header-only form
int main(
#ifdef QT_TABLE_TEST
    int    arc,
    char** argv
#endif
) {

//#= Soft assertions test
#ifdef TEST_SOFT_ASSERTIONS
    DEBUG_INTERNAL_POSITION_IN_CODE

    ASSERT_EXPRESSION_INTERNAL(false, "False test")

    DEBUG_WARN_IF_CALLED("Warn test")

    Test::func_test();

    auto func = []() {
        throw std::invalid_argument("Throw invalid argument test");
    };

    DEBUG_EXCEPTION(func())

    DEBUG_EXCEPTION(
        throw std::invalid_argument("Throw invalid argument test");)

    DEBUG_EXPRESSION(INFO_2 << "Test expression print";)


    debug_expresion_return();


#endif


//#= Basic logging test
#ifdef LOGGING_TEST
    ERROR << "DS_ERROR";
    WARN << "DS_WARNING";
    WARN_1 << "DS_WARNING_1";

    INFO << "DS_INFO";
    INFO_1 << "DS_INFO_1";
    INFO_2 << "DS_INFO_2";
    INFO_3 << "DS_INFO_3";
    ERROR_1 << "DS_ERROR_1";

    int hello = 0;
    logvar(hello);
    logexpr(2 + 2);
    logexpr(4 / 3.0);

    function_begin_end_print();
#endif

//#= Prefix swap test
#ifdef PREFIX_SWAP_DEMO


    //#== Macro style swap
    {
        INFO << "Macro style swap test";

#    pragma push_macro("DS_INFO")
#    define DS_INFO DS_INFO_1
        INFO << "Now all INFO messages have the same style as INFO_1";
#    pragma pop_macro("DS_INFO")

        INFO << "Finished macro style test";
    }


    //#== Change default message prefix
    {
        INFO << "Adding prefix for INFO messages";
#    pragma push_macro("DMS_INFO_MESSAGE_PREFIX")
#    define DMS_INFO_MESSAGE_PREFIX "INFO: "
        INFO << "Using new prefix for loggig";
#    pragma pop_macro("DMS_INFO_MESSAGE_PREFIX")
        INFO << "Using old prefix for loggig";
    }


    //#== Custom message prefix
    {
        INFO << "Hack-ish way to add prefix for INFO messages";
#    pragma push_macro("INFO")
#    define INFO DMS_LOG << DS_INFO << "~~>"
        INFO << "Using new prefix for loggig";
#    pragma pop_macro("INFO")
        INFO << "Using old prefix for loggig";
    }

#endif

//#= Indentation test
#ifdef INDENTATION_TEST
    for (uint i = 0; i < 10; ++i) {
        WARN << "Fuck you."
             << "Another string";
        DEBUG_INDENT
    }

    INFO << "Testing multiline string";
    LOG << "###Helloo world\nsadfasdHelloo world\nsadfasdHelloo "
           "world\nsadfasdHelloo world\nsadfasdHelloo "
           "world\nsadfasdHelloo world\nsadfasdHelloo world\nsadfasd";

    LOG << "Separate";

    for (uint i = 0; i < 10; ++i) {
        DEBUG_DEINDENT
        ERROR << "Fuck you.";
    }
#endif

#ifdef DEBUG_DISABLE_TEST
    recursive_function_with_debug();
#endif

//#= Extra styling test
#ifdef STYLING_EXTRA_TEST
    LOG << BG_RED << "Hello" << FG_BLUE << 12 << "32432"
        << "dafsd" << 112.12f << 323.34;

    LOG << BG_RED << "Hello" << FG_BLUE << 12 << 112.12f << 323.34;

    LOG << DS_ERROR_1 << "Very" << DS_DEFAULT << DS_ERROR << "Interesting"
        << DS_DEFAULT << DS_UNDERLINED << DS_ITALIC << BG_LIGHTRED << "Way"
        << DS_DEFAULT << "to" << DS_STRIKETHROUGH << DS_DEFAULT << "print"
        << DS_BOLD << "messages";

#endif

#ifdef HELPER_FUNCTIONS_TEST
    LOG << dbg::justify("Hello", 30);
#endif

//#= Debug table test
#ifdef TABLE_TEST
    DEBUG_TABLE(3)[0][0] << "Random string";
    DEBUG_TABLE(3)[0][1] << "Random string";
    DEBUG_TABLE(3)[2][2] << "Je;;p";
    DEBUG_TABLE(3)[3][2] << "Hello";

    LOG << "Debug table has" << DEBUG_TABLE(3).size() << "rows";

    DEBUG_PRINT_TABLE(3)
#endif

//#= Table styling test
#ifdef TABLE_STYLING_TEST
    { //#== Coloring test
        dbg::DebugTable table;

        /// \note WHen using cell stirng builkding operators be aware of
        /// the evaluation order of used operator: `<< DS_BOLD <<= "TEST"`
        /// will work just fine, but `<<= "TEST" << DS_BOLD` won't even
        /// compile

        int idx = 0;
        for (int i = 0; i < 4; ++i) {
            for (int j = 0; j < 4; ++j) {
                table[i][j] << dbg::idx_to_foreground(idx) << DS_ITALIC
                            << "TEST" << DS_BOLD
                    <<= "TEST";

                table[i + 4][j] << dbg::idx_to_background(idx)
                                << DS_UNDERLINED << "TEST" << DS_BOLD
                    <<= "TEST";
                ++idx;
            }
        }

        table.printTable();
    }

    { //#== Visibility test
        dbg::DebugTable table;

        for (int i = 0; i < 2; ++i) {
            for (int j = 0; j < 2; ++j) {
                table[i][j] << DS_ITALIC << "TEST" << DS_BOLD <<= "TEST";
            }
        }

        INFO << "Erasing VerticalLines";

        table.setVisibility(
            dbg::DebugTable::Visibility::Erase,
            dbg::DebugTable::Part::VerticalLines);

        table.printTable();
        table.clearVisibilitySettings();

        INFO << "Erasing HorizontalLines";

        table.setVisibility(
            dbg::DebugTable::Visibility::Erase,
            dbg::DebugTable::Part::HorizontalLines);

        table.printTable();
    }


#endif

//#= Qt table test
#ifdef QT_TABLE_TEST
    QApplication a(arc, argv);
    dbg::DebugWindow::instance()->show();


    ROW() << "Random string";
    ROW() << "Key";
    ROW(1) << "Value";
    //    ROW(2) << "Value";

    DEBUG_FINALIZE_WINDOW

    return a.exec();
#else
    return 0;
#endif
}
