include($$PWD/debug.pri)
include($$PWD/../Algorithm/algorithm.pri)

QT += core gui widgets
CONFIG += console
QMAKE_CXXFLAGS *= -std=c++17

SOURCES += \
    main.cpp \
    demo.cpp

DISTFILES += \
    README.md

HEADERS += \
    demo.hpp
