/*!
 * \file flip_debug_state.hpp
 * \brief Inlcude this file to turn off debug messages on all lines below.
 * Second inclusion will revert debugging back to normal
 *
 * ````
 * #include <flip_debug_state.hpp>
 *
 * // Code with disabled debug macro
 *
 * #include <flip_debug_state.hpp>
 *
 * // Code with working debug macor
 *
 * #include <flip_debug_state.hpp>
 *
 * // Code with disabled debug macro
 *
 * #include <flip_debug_state.hpp>
 * ````
 */

#pragma once
#    include <debug_undefiner.hpp>
#else
#    undef NO_DEBUG_MESSAGES
#    include <debug_undefiner.hpp>
