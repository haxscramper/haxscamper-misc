/*!
 * \file assertions.hpp
 * \brief Include core macro and assertion macro
 */
#pragma once

#include "../../src/debuglogger/debug_macro_assertions.hpp"
#include "../../src/debuglogger/debug_macro_base.hpp"
#include "../../src/debuglogger/debug_macro_file.hpp"
#include "../../src/debuglogger/debug_macro_style_base.hpp"
#include "../../src/debuglogger/debuglogger.hpp"
