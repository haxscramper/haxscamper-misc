Explanation of coloring and formatting in cecho script. (cecho stands
for colorful echo).

https://misc.flogisoft.com/bash/tip_colors_and_formatting

# Colors

## Style color codes

| Code | Style         |
|------|---------------|
|    1 | bold          |
|    2 | dim           |
|    3 | italic        |
|    4 | underline     |
|    5 | blink         |
|    8 | hidden        |
|    9 | strikethrough |

## Predefined styles

| Style name | Foreg         | Backg | Codes     | Comments      | Prefix |
|------------|---------------|-------|-----------|---------------|--------|
| LOG        | None          | None  |           | Default LOG   | `  >`  |
| INFO       | Green         | None  | 32        | Default INFO  | ` ->`  |
| INFO_1     | Yellow        | None  | 33        |               | ` ->`  |
| INFO_2     | Blue          | None  | 34        |               | `-->`  |
| INFO_3     | Red           | None  | 31        |               | `-->`  |
| WARN       | Magenta, Bold | None  | 35;1      | Default WARN  | `==>`  |
| WARN_1     | Yellow, Bold  | None  | 96;1      | Default WARN  | `==>`  |
| ERROR      | White         | Red   | 97;41     | Default ERROR | `!!!`  |
| ERROR_1    | Green, Bold   | Black | 40;92;5;1 | Blink         | `###`  |

@idea: Markown to manpages converter. `man markdown | grep man` does
not give anything useful;

I will try to stick with this message formatting style in all scripts,
messages etc. when I possibly can. Message Prefixes can be replaced
with their names (`LOG, INFO, INFO_1` etc) if necessary. All prefixes
have to have equal length, padded on right with shitespaces
