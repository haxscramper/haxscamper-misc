#!/usr/bin/env fish
for file in (ls | grep -e ".hpp")
    ./cecho -i1 -p $file
    set relative_2 (grep '#\s*include ".*"$' $file |
    sed 's/#\s*include \"\(.*\)\"$/\1/' |
    xargs dirname 2> /dev/null |
    uniq)

    ./cecho -i2 -p $relative_2
    set relative ".."

    while grep -q '#\s*include ".*"$' $file
        ./cecho -p "Running $file"
        for line in (cat $file)
            if echo $line | grep -q 'include\s*"'
                set includepath (echo $line | sed 's/#\s*include "\(.*\)"/\1/')
                cat $PWD/$relative/$includepath >> $file.tmp
            else
                echo $line >> $file.tmp
            end
        end
        mv $file.tmp $file
        set relative $relative_2
    end
end
