/*!
 * \file all.hpp
 * \brief Include all parts of the debug macro library
 */

// TODO Remove this header

#pragma once

#include "../../src/debuglogger/debug_macro_base.hpp"
#include "../../src/debuglogger/debug_macro_style_base.hpp"
#include "../../src/debuglogger/debug_macro_style_extra.hpp"
#include "../../src/debuglogger/debug_support.hpp"
