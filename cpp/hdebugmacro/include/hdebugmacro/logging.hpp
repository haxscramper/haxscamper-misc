/*!
 * \file logging.hpp
 * \brief Include core library and basic logging functionality (LOG macro
 * and simple styles)
 */

#pragma once

#include "../../src/debuglogger/debug_macro_base.hpp"
#include "../../src/debuglogger/debug_macro_style_base.hpp"
#include "../../src/debuglogger/debuglogger.hpp"
