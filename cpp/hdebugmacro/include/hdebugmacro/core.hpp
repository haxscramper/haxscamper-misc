/*!
 * \file core.hpp
 * \brief Include only core features of the library, no predefined macros
 */

#pragma once

#include "../../debuglogger/debuglogger.hpp"
