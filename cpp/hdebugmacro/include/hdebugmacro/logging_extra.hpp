/*!
 * \file logging_extra.hpp
 * \brief Include all logging functionality
 */

#pragma once

#include "../../src/debuglogger/debug_macro_base.hpp"
#include "../../src/debuglogger/debug_macro_logging.hpp"
#include "../../src/debuglogger/debug_macro_style_base.hpp"
#include "../../src/debuglogger/debuglogger.hpp"
