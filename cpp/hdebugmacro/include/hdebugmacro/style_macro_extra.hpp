/*!
 * \file style_macro_extra.hpp
 * \brief Include all styling macro
 */

#pragma once

#include "../../src/debuglogger/debug_macro_base.hpp"
#include "../../src/debuglogger/debug_macro_style_base.hpp"
#include "../../src/debuglogger/debug_macro_style_extra.hpp"
