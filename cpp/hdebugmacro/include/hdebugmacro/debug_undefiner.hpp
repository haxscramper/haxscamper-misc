/*!
 * \file debug_undefiner.hpp
 * \brief Temporary undefine base debug macro
 *
 * ````
 * #define NO_DEBUG_MESSAGES
 * #include <debug_undefiner.hpp>
 *
 * // Code with disabled debug macro
 *
 * #undef NO_DEBUG_MESSAGES
 * #include <debug_undefiner.hpp>
 * ````
 */


//  ////////////////////////////////////


#ifdef NO_DEBUG_MESSAGES

#    pragma push_macro("DEBUG_FUNCTION_PRE_RETURN")
#    undef DEBUG_FUNCTION_PRE_RETURN
#    define DEBUG_FUNCTION_PRE_RETURN(message)

#    pragma push_macro("DEBUG_INDENT")
#    undef DEBUG_INDENT
#    define DEBUG_INDENT

#    pragma push_macro("DEBUG_DEINDENT")
#    undef DEBUG_DEINDENT
#    define DEBUG_DEINDENT

#    pragma push_macro("LOG")
#    undef LOG
#    define LOG VOID_LOG


#else

#    pragma pop_macro("DEBUG_FUNCTION_PRE_RETURN")
#    pragma pop_macro("DEBUG_INDENT")
#    pragma pop_macro("DEBUG_DEINDENT")
#    pragma pop_macro("LOG")

#endif
