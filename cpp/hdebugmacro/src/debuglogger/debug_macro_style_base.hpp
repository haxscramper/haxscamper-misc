/*!
 * \file debug_macro_style_base.hpp
 * \brief Basic styling macro
 */

#pragma once

#include "debuglogger.hpp"

#define DMS_ERROR(message)
#define DMS_WARNING(message)
#define DMS_WARNING_1(message)

#define DMS_INFO(message)
#define DMS_INFO_1(message)
#define DMS_INFO_2(message)
#define DMS_INFO_3(message)
