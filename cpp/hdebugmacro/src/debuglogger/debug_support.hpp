#pragma once


#ifdef QT_CORE_LIB
#    include <QMouseEvent>
#    if defined(QT_GUI_LIB) && defined(QT_WIDGETS_LIB)
#        include "../debugwindow/debugwindow.hpp"
#    endif
#endif

#include "debug_macro.hpp"
#include "debuglogger.hpp"
#include "debugtable.hpp"


#include <cmath>
#include <string>

/*!
 * \file debug_support.hpp
 * \brief
 *
 * \todo Remove excess indentation when using coloring macro etc. macro
 */

typedef dbg::DebugTable::Visibility DBGPartVisibility;

namespace dbg {
#if defined(QT_GUI_LIB) && defined(QT_WIDGETS_LIB)
/// Helper function to allow macro with optional arguments
inline DebugWindowRowBuilder Row(int column = 0) {
    return DebugWindowRowBuilder::Row(column);
}


[[deprecated("useless")]] inline void addRow(
    LogString first,
    LogString second) {
    Row() << first;
    Row(1) << second;
}


[[deprecated("useless")]] inline void addRow(LogString first, int second) {
    Row() << first;
    Row(1) << second;
}

#endif


inline bool alreadyCalled(LogString logId) {
    static LogString lastLogId;
    if (lastLogId == logId) {
        return true;
    } else {
        lastLogId = logId;
        return false;
    }
}

} // namespace dbg

namespace dbg {

#ifdef QT_CORE_LIB
/// \todo Add macro for justification
inline QString justify(QString source, int targetLength) {
    return source + QString(targetLength - source.length(), ' ');
}

inline QString justify(const char* source, int targetLength) {
    return QString::fromStdString(
        source + std::string(targetLength - strlen(source), ' '));
}

/*!
 * Left pad string with `indent` whitespaces, print name and then
 * right-pad until the `justify` length. Then append `value`
 *
 *
 * Example (dash denotes spaces)
 * ```
 * debug_one_line_value(2, "Hello", 10, "World") ->
 *
 * --Hello:----World
 * ^ Two spaces
 *         ^ Pad right until string has length 10
 *              ^ Print second string
 * ```
 */
inline QString debug_one_line_value(
    uint    indent,  ///< Left indentataion
    QString name,    ///< Left value to print
    uint    justify, ///< Target width
    QString value    ///< Final string
) {
    return QString(' ', indent) + dbg::justify(name + ":", justify) + value
           + "\n";
}
#endif


inline LogString justify(LogString source, int targetLength) {
    return source + std::string(targetLength - source.length(), ' ');
}

#if defined(QT_GUI_LIB) && defined(QT_WIDGETS_LIB)
inline void debugMouseEvent(QMouseEvent* event) {
    DEBUG_INDENT
    LOG << "Event local pos ";
    LOG << "  x:" << event->pos().x() << "y:" << event->pos().y();
    LOG << "Event global pos ";
    LOG << "  x:" << event->globalPos().x()
        << "y:" << event->globalPos().x();
    DEBUG_DEINDENT
}

LogString str(QPoint pos);
LogString str(QSize size);
LogString str(QStringList list);
LogString str(QRect rect);
#endif
} // namespace dbg


#define LOG_BOOL_RETURN_VAL(__message, __expression)                      \
    [&](bool expressionResult) -> bool {                                  \
        LOG << __message << LOG_FROM_BOOL(expressionResult);              \
        return expressionResult;                                          \
    }(__expression)

#define LOG_ONCE(___message)                                              \
    if (!dbg::alreadyCalled(___message)) {                                \
        LOG << ___message;                                                \
    }

#define DEBUG_FINALIZE_WINDOW dbg::DebugWindow::setFinalized();


#define DEBUG_SET_INDENT_STYLE_VERTICAL                                   \
    dbg::DebugLogger::setIndentFront(                                     \
        dbg::DebugLogger::IndentFrontStyle::Vertical);

#define DEBUG_LOGGER_SET_DEFAULT_PREFIX(prefix)                           \
    dbg::DebugLogger::setDefaultPrefix(prefix);

#define DEBUG_LOGGER_USE_GTEST_PREFIX                                     \
    DEBUG_LOGGER_SET_DEFAULT_PREFIX("           ]")

#define DEBUG_FUNCTION_BEGIN_FANCY DEBUG_FUNCTION_BEGIN
#define DEBUG_FUNCTION_END_FANCY DEBUG_FUNCTION_END
