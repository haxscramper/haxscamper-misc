#pragma once

#include "debug_macro_base.hpp"

#define ASSERT_EXPRESSION_INTERNAL(expressionToCall, failedComment)       \
    ERROR << "Assertion" << DS_DEFAULT << #expressionToCall << DS_ERROR   \
          << "has failed";                                                \
    INFO << failedComment;                                                \
    DEBUG_INTERNAL_POSITION_IN_CODE


/// \brief Print line/document/funciton and call abort();
#define DEBUG_ABORT_IF_CALLED(abortMessage)                               \
    ERROR_1 << abortMessage;                                              \
    DEBUG_INTERNAL_POSITION_IN_CODE                                       \
    abort();


#define DEBUG_WARN_IF_CALLED(warnMessage)                                 \
    WARN << warnMessage;                                                  \
    DEBUG_INTERNAL_POSITION_IN_CODE

/// \brief Print warning for virtual base class function
#define DEBUG_BASE_CLASS_FUNCTION_WARNING                                 \
    WARN << "You called" << DS_INFO_1 << __PRETTY_FUNCTION__;             \
    INFO << "which is virtual function of base class";                    \
    DEBUG_INTERNAL_POSITION_IN_CODE


/// \brief Print error message if _expression is evaluated as false.
/// and terminate the program
#define DEBUG_ASSERT_TRUE(_expression)                                    \
    if (!(_expression)) {                                                 \
        ASSERT_EXPRESSION_INTERNAL(_expression, "Evaluated as false")     \
        ERROR << "Aborting program";                                      \
        abort();                                                          \
    }


/// \brief Print error message if _expression is evaluated as true.
/// and terminate the program
#define DEBUG_ASSERT_FALSE(_expression)                                   \
    if ((_expression)) {                                                  \
        ASSERT_EXPRESSION_INTERNAL(_expression, "Evaluated as true")      \
        ERROR << "Aborting program";                                      \
        abort();                                                          \
    }


/// \brief Print error message if _expression is evaluated as false.
#define DEBUG_SOFT_ASSERT_TRUE(_expression)                               \
    if (!(_expression)) {                                                 \
        ASSERT_EXPRESSION_INTERNAL(_expression, "Evaluated as false")     \
    }


/// \brief Print error message if _expression is evaluated as false.
#define DEBUG_SOFT_ASSERT_FALSE(_expression)                              \
    if ((_expression)) {                                                  \
        ASSERT_EXPRESSION_INTERNAL(_expression, "Evaluated as true")      \
    }
