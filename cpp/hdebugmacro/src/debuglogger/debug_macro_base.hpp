/*!
 * \file debug_macro_base.hpp
 * \brief Most important logging commands.
 *
 * Default styles are controlled using DS_DEFAULT, DS_ERROR,
 * DS_WARNING,DS_INFO macro. Redefinition of said macros will change style
 * of all logging commands in the library. Example:
 * `#define DS_ERROR dbg::Background::Red` -- now all error messages will
 * have red background but foreground will remain the same (as in previous
 * message)
 */

#pragma once

#include "debuglogger.hpp"


#ifdef __GNUG__
#    include <cstdlib>
#    include <cxxabi.h>
#    include <memory>

namespace dbg {
static std::string demangle(const char* name) {

    int status = -4; // some arbitrary value to eliminate the compiler
    // warning

    // enable c++11 by passing the flag -std=c++11 to g++
    std::unique_ptr<char, void (*)(void*)> res{
        abi::__cxa_demangle(name, NULL, NULL, &status), std::free};

    return (status == 0) ? res.get() : name;
}
} // namespace dbg

#else

// does nothing if not g++

namespace dbg {
static std::string demangle(const char* name) {
    return name;
}
} // namespace dbg

#endif

namespace dbg {
static std::string ensure_width(const std::string& str, size_t len) {
    if (str.size() <= len) {
        return str + std::string(len - str.size(), ' ');
    } else {
        return std::string(str.begin(), str.begin() + len - 2) + "..";
    }
}
} // namespace dbg


//#= Debug logger settings
#define DEBUG_LOGGER_PRINT_CLASS
#define DEBUG_DEFAULT_INDENT 3

//#= Logging
//#== Basic styles
//#=== Deafult styles
/// Default style
#define DS_DEFAULT dbg::LogSettings()
/// Default error style. White on red
#define DS_ERROR dbg::Foreground::White << dbg::Background::Red
/// Default warning style. Black on green
#define DS_WARNING dbg::Foreground::Magenta << dbg::PrintSettings::Bold
/// Default info style. Green foreground
#define DS_INFO dbg::Foreground::Green

//#=== Additional styles
/// Additional warning style. Black on yellow
#define DS_WARNING_1 dbg::Background::Yellow << dbg::PrintSettings::Bold
/// Additional info style. Yellow foreground
#define DS_INFO_1 dbg::Foreground::Yellow
/// Additional info style. Blue foreground
#define DS_INFO_2 dbg::Foreground::Blue
/// Additional info style. Red foreground
#define DS_INFO_3 dbg::Foreground::Red
/// Additional error style. BLinking bold green on black baclground
#define DS_ERROR_1                                                        \
    dbg::Foreground::Green << dbg::Background::Black                      \
                           << dbg::PrintSettings::Bold                    \
                           << dbg::PrintSettings::Blink

#define DMS_LOG dbg::DebugLogger::log()


//#== Basic logging commands
/// Enable/disable support for message prefixes
//#=== With prefixes
#ifdef DMS_USE_MESSAGE_PREFIXES
#    define DMS_LOG_MESSAGE_PREFIX "  >"

#    define DMS_INFO_MESSAGE_PREFIX " ->"
#    define DMS_INFO_1_MESSAGE_PREFIX " ->"
#    define DMS_INFO_2_MESSAGE_PREFIX "-->"
#    define DMS_INFO_3_MESSAGE_PREFIX "-->"

#    define DMS_WARNING_MESSAGE_PREFIX "==>"
#    define DMS_WARNING_1_MESSAGE_PREFIX "==>"

#    define DMS_ERROR_MESSAGE_PREFIX "!!!"
#    define DMS_ERROR_1_MESSAGE_PREFIX "###"


// ---

#    define LOG DMS_LOG << DS_DEFAULT << DMS_LOG_MESSAGE_PREFIX

#    define INFO DMS_LOG << DS_INFO << DMS_INFO_MESSAGE_PREFIX
#    define INFO_1 DMS_LOG << DS_INFO_1 << DMS_INFO_1_MESSAGE_PREFIX
#    define INFO_2 DMS_LOG << DS_INFO_2 << DMS_INFO_2_MESSAGE_PREFIX
#    define INFO_3 DMS_LOG << DS_INFO_1 << DMS_INFO_3_MESSAGE_PREFIX

#    define WARN DMS_LOG << DS_WARNING << DMS_WARNING_MESSAGE_PREFIX
#    define WARN_1 DMS_LOG << DS_WARNING_1 << DMS_WARNING_1_MESSAGE_PREFIX

#    define ERROR DMS_LOG << DS_ERROR << DMS_ERROR_MESSAGE_PREFIX
#    define ERROR_1 DMS_LOG << DS_ERROR_1 << DMS_ERROR_1_MESSAGE_PREFIX

#else
  //#=== Without prefixes
#    define LOG DMS_LOG << DS_DEFAULT

#    define INFO DMS_LOG << DS_INFO
#    define INFO_1 DMS_LOG << DS_INFO_1
#    define INFO_2 DMS_LOG << DS_INFO_2
#    define INFO_3 DMS_LOG << DS_INFO_1

#    define WARN DMS_LOG << DS_WARNING
#    define WARN_1 DMS_LOG << DS_WARNING_1

#    define ERROR DMS_LOG << DS_ERROR
#    define ERROR_1 DMS_LOG << DS_ERROR_1
#endif


#define VOID_LOG dbg::DebugVoidLog()

#define DMS_POST_ACTION DMS_LOG << " ";

//#= Indentation
//#== Base macro
#define DEBUG_INDENT                                                      \
    dbg::DebugLogger::increaseIndent(DEBUG_DEFAULT_INDENT);
#define DEBUG_DEINDENT                                                    \
    dbg::DebugLogger::decreaseIndent(DEBUG_DEFAULT_INDENT);

//#== Function call
#define DEBUG_FUNCTION_BEGIN                                              \
    INFO_1 << "Function:" << DS_DEFAULT << __PRETTY_FUNCTION__;           \
    DEBUG_INDENT

#define DEBUG_FUNCTION_END                                                \
    DEBUG_DEINDENT                                                        \
    INFO_2 << "Success:" << __FUNCTION__;

#define DEBUG_INTERNAL_POSITION_IN_CODE                                   \
    DEBUG_INDENT                                                          \
    INFO << "Line:    " << DS_DEFAULT << __LINE__;                        \
    INFO << "File:    " << DS_DEFAULT << __NO_GCC__FILENAME__;            \
    INFO << "Function:" << DS_DEFAULT << __FUNCTION__;                    \
    DEBUG_DEINDENT

#define DEBUG_DISABLE_IF(expression)                                      \
    if (expression) {                                                     \
        dbg::DebugLogger::toggleDebug(false);                             \
    }


#define DEBUG_ENABLE_IF(expression)                                       \
    if (expression) {                                                     \
        dbg::DebugLogger::toggleDebug(true);                              \
    }


#define CMP_DBG(...)                                                      \
    [&]() -> bool {                                                       \
        bool res = __VA_ARGS__;                                           \
        if (!res) {                                                       \
            WARN << #__VA_ARGS__ << "-->" << res;                         \
        }                                                                 \
        return res;                                                       \
    }()

namespace dbg {
//#== Optional logging off
class DebugVoidLog
{
  public:
    template <class T>
    DebugVoidLog& operator<<(T) {
        return *this;
    }
};


#define logvar(var)                                                       \
    LOG << dbg::ensure_width(#var, 12)                                    \
        << (std::string("(")                                              \
            + dbg::ensure_width(dbg::demangle(typeid(var).name()), 12)    \
            + ")")                                                        \
        << var;

#define logexpr(expr) LOG << dbg::ensure_width(#expr, 27) << expr;


} // namespace dbg
