#pragma once

#include <codecvt>
#include <iomanip>
#include <iostream>
#include <locale>
#include <string>
#include <vector>


#include "debugger_settings.hpp"
#include "debugger_styles.hpp"

namespace spt {
inline std::string escape_bold() {
    return ESCAPE_SEQ + "1m";
}

inline std::string escape_dim() {
    return ESCAPE_SEQ + "2m";
}

inline std::string escape_italic() {
    return ESCAPE_SEQ + "3m";
}

inline std::string escape_underline() {
    return ESCAPE_SEQ + "4m";
}

inline std::string escape_blink() {
    return ESCAPE_SEQ + "5m";
}

inline std::string escape_inverted() {
    return ESCAPE_SEQ + "7m";
}

inline std::string escape_strikethrough() {
    return ESCAPE_SEQ + "9m";
}

inline std::vector<std::string> split_newlines(std::string message) {
    std::vector<std::string> res;


    std::size_t current, previous = 0;
    current = message.find_first_of('\n');
    while (current != std::string::npos) {
        res.push_back(message.substr(previous, current - previous));
        previous = current + 1;
        current  = message.find_first_of('\n', previous);
    }
    res.push_back(message.substr(previous, current - previous));

    return res;
}


inline std::string to_string(std::string string) {
    std::string res;
    res.assign(string.begin(), string.end());
    return res;
}

template <class T>
void print(T t, bool newline = false) {
    std::cout << t << (newline ? "\n" : "");
}
} // namespace spt
