/*!
 * \file debugtable.hpp
 * \brief description
 *
 * \todo Replace enum with struct to allow colored table parts.
 * You can change appearance of the table's parts by declaring custom enum
 * and assigning it to the DMS_TABLE_ENUM macro.
 *
 * Example of the redefinition:
 *
 * ```
 * enum class ExampleEnum : char
 * {
 *     Lower_Right_Corner = '1',
 *     Lower_Left_Corner  = '2',
 *     Upper_Right_Corner = '3',
 *     Upper_Left_Corner  = '4',
 *     Cross              = '5',
 *     Horizontal         = '6',
 *     Vertical           = '7',
 *     Horizontal_Up      = '8',
 *     Horizontal_Down    = '9',
 *     Vertical_Left      = 'A',
 *     Vertical_Right     = 'B',
 * };
 *
 * #define DMS_TABLE_ENUM ExampleEnum
 * ```
 *
 * ```
 * 46666666666666669666666666666666966666663
 * 7               7               7       7
 * B666666666666666566666666666666656666666A
 * 7               7               7       7
 * B666666666666666566666666666666656666666A
 * 7               7               7       7
 * 26666666666666668666666666666666866666661
 * ```
 *
 * Default availiable styles `DMS_USE_FAT_TABLE` for fat table. Default
 * style is markdown table
 */


#pragma once

//===    Qt    ===//
#ifdef QT_VERSION
#    include <QString>
#endif

//===   Other  ===//
#include <halgorithm/setting_flags.hpp>


//===    STL   ===//
#include <numeric>
#include <unordered_map>
#include <vector>

//#== Internal
#include <halgorithm/rangeoper.hpp>


//=== Sibling  ===//
#include "debugger_settings.hpp"
#include "debugger_styles.hpp"
#include "log_string.hpp"
#include "support.hpp"
//#include <halgorithm/all.hpp>
#include <memory>


//#  /////////////////////////////////////////////////////////////////////
//#  Debug
//#  /////////////////////////////////////////////////////////////////////

namespace dbg {


#ifdef DMS_USE_FAT_TABLE
enum class __Fat_TableEdge : char
{
    Lower_Right_Corner = '#',
    Lower_Left_Corner  = '#',
    Upper_Right_Corner = '#',
    Upper_Left_Corner  = '#',
    Cross              = '@', ///< Internal intersection
    Horizontal         = '=', ///< Internal horizontal
    Vertical           = '$', ///< Internal vertical
    Horizontal_Up      = '#', ///< External horizontal upper edge
    Horizontal_Down    = '#', ///< External horizontal lower edge
    Vertical_Left      = '#', ///< External vertical left edge
    Vertical_Right     = '#', ///< External vertical right edge
};
#    define DMS_TABLE_ENUM __Fat_TableEdge
#else
enum class __Markdown_TableEdge : char
{
    Lower_Right_Corner = '|',
    Lower_Left_Corner  = '|',
    Upper_Right_Corner = '|',
    Upper_Left_Corner  = '|',
    Cross              = '|',
    Horizontal         = '-',
    Vertical           = '|',
    Horizontal_Up      = '|',
    Horizontal_Down    = '|',
    Vertical_Left      = '|',
    Vertical_Right     = '|',
};
#    define DMS_TABLE_ENUM __Markdown_TableEdge
#endif


class DebugTable;


enum class CellStyle
{
    NoStyle,
    UnderlineNon_Empty
};


/// Single cell of the debug table
class TableCell
{
  public:
    void        fitInto(uint cellWidth);
    std::string getLine(uint lineNumber);

    /// Get number if string lines in the cell
    uint lineCount() const {
        return lines.size();
    }

    /// Get width of the cell. Length of the longes string + double spacing
    uint width() const {
        if (lines.size() == 0) {
            return 0;
        }

        return 2 * spacing
               + (*std::max_element(
                      lines.begin(),
                      lines.end(),
                      [](const std::string& lhs, const std::string& rhs) {
                          return lhs.length() < rhs.length();
                      }))
                     .length();
    }

    /// Print one line of the cell. If cell has less lines than requested
    /// it will be printed blank
    void print(
        uint      width,
        uint      line,
        CellStyle style = CellStyle::NoStyle) {
        std::string text;

        if (lines.size() <= line) {
            text = std::string(width, ' ');
        } else {
            text = std::string(spacing, ' ');
            text += lines.at(line);
            text += std::string(
                width                         //
                    - lines.at(line).length() //
                    - 2 * spacing,
                ' ');
            text += std::string(spacing, ' ');
        }
        if (style == CellStyle::UnderlineNon_Empty //
            && lines.size() <= line) {
            spt::print(spt::escape_underline());
        }

        spt::print(settings.getEscapeCodes());
        spt::print(text);
        spt::print(ESCAPE_SEQ_NULL);
    }


    TableCell& operator<<(LogString message) {
        spt::concatenate_copy(
            lines, spt::split_newlines(message.toString()));
        return *this;
    }


    TableCell& operator<<=(LogString message) {
        lines.back() += spacer + message.toString();
        return *this;
    }


    TableCell& operator<<(Background _setting) {
        settings.setBackground(_setting);
        return *this;
    }


    TableCell& operator<<(Foreground _setting) {
        settings.setForeground(_setting);
        return *this;
    }


    TableCell& operator<<(LogSettings _setting) {
        settings += _setting;
        return *this;
    }


    TableCell& operator<<(PrintSettings _setting) {
        settings.setStyle(_setting);
        return *this;
    }

    /// Set left and right spacing for the cell text
    void setSpacing(const uint& value) {
        spacing = value;
    }


  private:
    std::vector<std::string> lines;
    std::string              spacer = " ";
    LogSettings              settings;
    uint                     spacing = 0;
};

/// One row of the debug table
class TableRow
{
  public:
    /// Get column at position. If row contains less cells create all
    /// missing.
    TableCell& at(uint column) {
        if (cells.size() <= column) {
            cells.resize(column + 1);
        }

        return cells.at(column);
    }

    void resize(uint colCount) {
        cells.resize(colCount);
    }

    uint size() const {
        return cells.size();
    }

    uint height() const {
        if (size() == 0) {
            return 0;
        }

        return (*std::max_element(
                    cells.begin(),
                    cells.end(),
                    [](const TableCell& lhs, const TableCell& rhs) {
                        return lhs.lineCount() < rhs.lineCount();
                    }))
            .lineCount();
    }

    /// \todo Rename to getCellsRef()
    std::vector<TableCell>& getCells() {
        return cells;
    }

    TableCell& operator[](uint column) {
        return this->at(column);
    }

  private:
    std::vector<TableCell> cells;
};

/// Print markdown-styled tables (style can be changed) in the terminal.
class DebugTable
{
    enum class SpacerType
    {
        Top,
        Middle,
        Bottom
    };

  public:
    /// Types of the visibility of the table parts
    enum class Visibility
    {
        Show,  ///< Lines will be replaces with  zero-length strings
        Hide,  ///< Lines will be replaced with empty spaces
        Erase, ///< Hide first and last one
        EraseEdges
    };


    enum class Part
    {
        HorizontalLines,
        VerticalLines,
        Edges,
        MiddleCrosses,
        OuterCrosses
    } tablePart;


    /// Hard set table to the target row and columnt count
    void resize(uint rowCount, uint colCount) {
        rows.resize(rowCount);
        for (TableRow& row : rows) {
            row.resize(colCount);
        }
    }

    /// Return reference to the table cell. If target cell does not exist,
    /// create new one
    TableCell& at(uint row, uint column) {
        if (rows.size() <= row) {
            rows.resize(row + 1);
        }

        if (rows.at(row).size() <= column) {
            rows.at(row).resize(column + 1);
        }

        return rows.at(row).at(column);
    }


    /// \brief  Get row located at [row]
    /// \return Reference to the row in table
    TableRow& at(uint row ///< Index of the target row
    ) {
        if (rows.size() <= row) {
            rows.resize(row + 1);
        }

        return rows.at(row);
    }


    /// Print all the content from the table. Pad each print with [indent]
    /// number of whitespaces
    void printTable(uint indent = 0) {
        if (rows.size() == 0) {
            return;
        }

        int col_count = (*std::max_element(
                             rows.begin(),
                             rows.end(),
                             [](TableRow& lhs, TableRow& rhs) {
                                 return lhs.size() < rhs.size();
                             }))
                            .size();

        std::vector<uint> widths(col_count);


        for (TableRow& row : rows) {
            /// \todo Add support for iterating over rows and columns with
            /// range-based for loop
            for (uint cell = 0; cell < row.size(); ++cell) {
                row.at(cell).setSpacing(spacing);
                if (widths.at(cell) < row.at(cell).width()) {
                    widths.at(cell) = row.at(cell).width();
                }
            }
        }

        printSpacer(indent, widths, SpacerType::Top);
        for (uint row = 0; row < rows.size(); ++row) {
            if (rows[row].height() != 0 || printEmptyRows) {
                printRow(row, indent, widths);
                if (row < rows.size() - 1) {
                    printSpacer(indent, widths, SpacerType::Middle);
                } else {
                    printSpacer(indent, widths, SpacerType::Bottom);
                }
            }
        }
    }

    /// Print nth table from the list of tables, padding it with [indent]
    /// whitespaces on left
    void printTable(uint tableIndex, uint indent) {
        if (tables.find(tableIndex) == tables.end()) {
            return;
        } else {
            tables.at(tableIndex)->printTable(indent);
        }
    }

    /// Get reference to the cell last row in the table. If no rows is
    /// present create new one
    TableCell& lastRow(uint column = 0) {
        if (rows.size() == 0) {
            rows.resize(1);
        }
        return rows.back().at(column);
    }

    /// Add new empty row ot the table
    void appendRow() {
        rows.push_back(TableRow());
    }

    /// Get reference to the table at [index]
    static DebugTable& table(uint index);

    /// Set visibility for the table part
    void setVisibility(const Visibility& value, const Part& _tablePart) {
        visibility[_tablePart] = value;
    }

    void clearVisibilitySettings() {
        visibility.clear();
    }

    /// Set global style settings (color, boldness etc) for the table parts
    void setGlobalSettings(const LogSettings& value) {
        globalSettings = value;
    }

    /// Get number of rows in the table
    uint size() const {
        return rows.size();
    }

    /// Access nth row
    TableRow& operator[](uint row) {
        return this->at(row);
    }


  private:
    std::vector<TableRow> rows;
    static inline std::unordered_map<uint, std::unique_ptr<DebugTable>>
        tables;

    bool                         printEmptyColumns = true;
    bool                         printEmptyRows    = false;
    uint                         spacing           = 1;
    spt::SettingFlags<CellStyle> cellSettings      = CellStyle::NoStyle;
    LogSettings                  globalSettings;


    spt::SettingEnum<Part, Visibility> visibility;


    void printRow(int rowIndex, uint indent, std::vector<uint>& widths) {
        TableRow& row = rows.at(rowIndex);

        uint lineCount = 0;

        auto iter = std::max_element(
            row.getCells().begin(),
            row.getCells().end(),
            [](const TableCell& lhs, const TableCell& rhs) {
                return lhs.lineCount() < rhs.lineCount();
            });


        if (iter != row.getCells().end()) {
            lineCount = (*iter).lineCount();
        }


        for (uint line = 0; line < lineCount; ++line) {
            spt::print(std::string(indent, ' '));
            if (visibility[Part::VerticalLines]
                != Visibility::EraseEdges) {
                spt::print(getTablePart(DMS_TABLE_ENUM::Vertical));
            } else if (
                visibility[Part::VerticalLines] == Visibility::EraseEdges
                && visibility[Part::HorizontalLines]
                       != Visibility::Erase) {
                spt::print(' ');
            }

            for (uint col = 0; col < widths.size(); ++col) {
                if (row.at(col).width() != 0 //
                    || printEmptyColumns     //
                    || col == widths.size() - 1) {
                    row.at(col) << globalSettings;
                    row.at(col).print(widths.at(col), line);
                    if (visibility[Part::VerticalLines]
                            == Visibility::EraseEdges //
                        && col == widths.size() - 1) {
                    } else {
                        spt::print(getTablePart(DMS_TABLE_ENUM::Vertical));
                    }
                }
            }
            spt::print("\n");
        }
    }

    void printSpacer(
        uint                     indent,
        const std::vector<uint>& widths,
        DebugTable::SpacerType   type) {
        if (visibility[Part::HorizontalLines] == Visibility::Erase) {
            return;
        } else if (visibility[Part::HorizontalLines] == Visibility::Hide) {
            uint totalWidth = std::accumulate(
                widths.begin(), widths.end(), 0u);
            totalWidth += widths.size() + 1;
            std::string spacer(totalWidth, ' ');

            spt::print(std::string(indent, ' '));
            spt::print(spacer);
            spt::print("\n");
        } else {
            std::string spacer;
            switch (type) {
                case SpacerType::Bottom: {
                    spacer += getTablePart(
                        DMS_TABLE_ENUM::Lower_Left_Corner);
                } break;
                case SpacerType::Top: {
                    spacer += getTablePart(
                        DMS_TABLE_ENUM::Upper_Left_Corner);
                } break;
                case SpacerType::Middle: {
                    spacer += getTablePart(DMS_TABLE_ENUM::Vertical_Right);
                } break;
            }


            for (uint col = 0; col < widths.size(); ++col) {
                uint width = widths.at(col);
                if (printEmptyColumns || width != 0) {
                    for (uint i = 0; i < width; ++i) {
                        spacer += getTablePart(DMS_TABLE_ENUM::Horizontal);
                    }

                    if (col < widths.size() - 1) {
                        switch (type) {
                            case SpacerType::Bottom: {
                                spacer += getTablePart(
                                    DMS_TABLE_ENUM::Horizontal_Up);
                            } break;
                            case SpacerType::Top: {
                                spacer += getTablePart(
                                    DMS_TABLE_ENUM::Horizontal_Down);
                            } break;
                            case SpacerType::Middle: {
                                spacer += getTablePart(
                                    DMS_TABLE_ENUM::Cross);
                            } break;
                        }
                    }
                }
            }

            switch (type) {
                case SpacerType::Bottom: {
                    spacer += getTablePart(
                        DMS_TABLE_ENUM::Lower_Right_Corner);
                } break;
                case SpacerType::Top: {
                    spacer += getTablePart(
                        DMS_TABLE_ENUM::Upper_Right_Corner);
                } break;
                case SpacerType::Middle: {
                    spacer += getTablePart(DMS_TABLE_ENUM::Vertical_Left);
                } break;
            }

            spt::print(std::string(indent, ' '));
            spt::print(spacer);
            spt::print("\n");
        }
    }

    std::string getTablePart(DMS_TABLE_ENUM partType) {
        std::string res;
        switch (partType) {
            case DMS_TABLE_ENUM::Vertical: {
                switch (visibility[Part::VerticalLines]) {
                    case Visibility::Show:
                        res += static_cast<char32_t>(partType);
                        break;
                    case Visibility::Hide: res += " "; break;
                    case Visibility::Erase: res += ""; break;
                    default: res += static_cast<char32_t>(partType); break;
                }

            } break;
            default: res += static_cast<char32_t>(partType); break;
        }

        return res;
    }
};

inline DebugTable& DebugTable::table(uint index) {
    if (tables.find(index) == tables.end()) {
        tables[index] = std::make_unique<DebugTable>();
    }
    return *tables[index];
}
} // namespace dbg

#define DEBUG_PRINT_TABLE(table_id)                                       \
    dbg::DebugTable::table(table_id).printTable(                          \
        dbg::DebugLogger::getIndentSize());

#define ROW(index) dbg::Row(index)

#define DEBUG_TABLE(table_id) dbg::DebugTable::table(table_id)
