#pragma once

#include <string>


//#ifdef QT_CORE_LIB
//#    include <QDir>
//#    include <QPoint>
//#    include <QPointF>
//#    include <QRect>
//#    include <QSize>
//#    include <QString>
//#    include <QStringList>
//#    include <QStringRef>
//#    include <QUuid>
//#endif

namespace dbg {

/*!
 * \brief Proxy class to use for implict conversion of different arguments
 * for debugging.
 *
 * Proxy class to use for implict conversion of different arguments
 * for debugging. Do **not** attempt to use it as regular string in any
 * way. If possible avoid using it at all in any forms. It is intended to
 * be used only for internal purposes
 */
class LogString
{

  private:
    std::string text;

  public:
    // TODO check for whether or not headers has been included instead of
    // incling them. Current solution significantly increases compilation
    // time for absolutely no reason
    //#ifdef QT_CORE_LIB

#ifdef QSTRING_H
    LogString(QString string) {
        text = string.toStdString();
    }

    LogString(QStringRef string) {
        text = string.toString().toStdString();
    }
#endif


#ifdef QRECT_H
    LogString(QRect rect) {
        QString result = "Top: "                                  //
                         + ("X:"                                  //
                            + QString::number(rect.topLeft().x()) //
                            + " "                                 //
                            + "Y:"                                //
                            + QString::number(rect.topLeft().y())
                            //
                            )
                         + "  Bottom: "                               //
                         + ("X:"                                      //
                            + QString::number(rect.bottomRight().x()) //
                            + " "                                     //
                            + "Y:"                                    //
                            + QString::number(rect.bottomRight().y())
                            //
                           );

        text = result.toStdString();
    }

#endif

#ifdef QPOINT_H
    /// For window/widget positions
    LogString(QPoint pos) {
        QString result;
        result += "X:"                       //
                  + QString::number(pos.x()) //
                  + " "                      //
                  + "Y:"                     //
                  + QString::number(pos.y());
        text = result.toStdString();
    }

    /// For coordinates on qgraphicsview
    LogString(QPointF coord) {
        text = "["                         //
               + std::to_string(coord.x()) //
               + ", "                      //
               + std::to_string(coord.y()) //
               + "]";
    }
#endif

#ifdef QSIZE_H
    LogString(QSize size) {
        QString result;
        result += "Width:" + QString::number(size.width()) + " "
                  + "Height:" + QString::number(size.height());
        text = result.toStdString();
    }
#endif

#ifdef QRECT_H
    LogString(QRectF rect) {
        text = "{ X: " + std::to_string(rect.x())
               + " Y: " + std::to_string(rect.y())
               + " H: " + std::to_string(rect.height())
               + " W: " + std::to_string(rect.width()) + " }";
    }

#endif

#ifdef QSTRINGLIST_H
    LogString(QStringList list) {
        QString result = "[";

        for (QString& string : list) {
            result += string + ",";
        }

        result.chop(1);
        result += "]";
        text = result.toStdString();
    }
#endif

#ifdef QDIR_H
    LogString(QDir dir) {
        text = dir.path().toStdString();
    }
#endif

#ifdef QUUID_H
    LogString(QUuid uuid) {
        text = uuid.toString(QUuid::WithoutBraces).toStdString();
    }
#endif


    //#endif

    //#======    Numeric conversions
    // TODO reformat for something more reasonable (disable clang-format
    // and write everything in one line)
    LogString(bool value) {
        text = (value ? "True" : "False");
    }

    LogString(int value) {
        text = std::to_string(value);
    }


    LogString(long value) {
        text = std::to_string(value);
    }


    LogString(long long value) {
        text = std::to_string(value);
    }


    LogString(unsigned value) {
        text = std::to_string(value);
    }


    LogString(unsigned long value) {
        text = std::to_string(value);
    }


    LogString(unsigned long long value) {
        text = std::to_string(value);
    }


    LogString(float value) {
        text = std::to_string(value);
    }


    LogString(double value) {
        text = std::to_string(value);
    }


    LogString(long double value) {
        text = std::to_string(value);
    }

    //#======    C-style string conversions

    LogString(const char* _text) {
        text = _text;
    }

    //#======    STL string conversions

    LogString(std::string _text) {
        text = std::move(_text);
    }

    //#======    String building

    LogString(int repeations, std::string _text) {
        text.resize(repeations * _text.length());
        for (int i = 0; i < repeations; ++i) {
            text.append(_text.begin(), _text.end());
        }
    }

    LogString(int repeations, char _text) {
        text.insert(0, _text, repeations);
    }

    template <class T>
    LogString(T* ptr) {
        text = "ptr: " + std::to_string((size_t)ptr);
    }

    //= Template constructor
    // TODO deprecate this shit and replace it with ostream overload
    template <class StringConvertible>
    LogString(StringConvertible str) {
        text = dbg_get_std_string(str);
    }


    //#======    Class functions

    std::string toString() {
        return text;
    }

    LogString() {
    }

    uint length() {
        return text.size();
    }

    bool operator==(LogString& other) {
        return text == other.text;
    }


    LogString operator+(LogString other) {
        return LogString(text + other.text);
    }
}; // namespace dbg
} // namespace dbg
