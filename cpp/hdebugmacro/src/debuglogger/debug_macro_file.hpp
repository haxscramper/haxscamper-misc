#pragma once

#include "debug_macro_logging.hpp"

#ifdef QFILE_H

/// \todo Add qt-less version
/// If file specified by fileName exists print that it exists, otherwise
/// print warning message and print position in code
#    define CHECK_IF_FILE_EXIST(fileName)                                 \
        QFile temp_file(fileName);                                        \
        if (temp_file.exists()) {                                         \
            INFO << "File" << DS_DEFAULT << fileName << "exists";         \
        } else {                                                          \
            WARN << "Could not open file" << DS_DEFAULT << fileName;      \
            DEBUG_INTERNAL_POSITION_IN_CODE                               \
        }


/// DEBUG_ASSERT_TRUE file exists()
#    define DEBUG_ASSERT_FILE_EXISTS(___filePath)                         \
        DEBUG_ASSERT_TRUE(QFile(___filePath).exists())

/// DEBUG_SOFT_ASSERT_TRUE file exists()
#    define DEBUG_SOFT_ASSERT_FILE_EXISTS(___filePath)                    \
        DEBUG_SOFT_ASSERT_TRUE(QFile(___filePath).exists())               \
        if (!QFile(___filePath).exists()) {                               \
            WARN << "Path:    " << DS_DEFAULT << ___filePath;             \
        }

#endif
