#pragma once


#include "debug_macro_base.hpp"


/// Increase indentation, call expression and decrease indentataion.
#define DEBUG_CALL_INDENTED(expressionToCall)                             \
    DEBUG_INDENT                                                          \
    expressionToCall;                                                     \
    DEBUG_DEINDENT

/// Increase indentation, call expression and `LOG` output and decrease
/// indentataion.
#define DEBUG_LOG_INDENTED(expressionToLog)                               \
    DEBUG_INDENT                                                          \
    LOG << expressionToLog;                                               \
    DEBUG_DEINDENT


/// Decrease indentation, call expression and `LOG` output and increase
/// indentataion.
#define DEBUG_LOG_DEINDENTED(expressionToLog)                             \
    DEBUG_DEINDENT                                                        \
    LOG << expressionToLog;                                               \
    DEBUG_INDENT


/// Print message and then `DEBUG_FUNCTION_END`
#define DEBUG_FUNCTION_PRE_RETURN(message)                                \
    LOG << message;                                                       \
    DEBUG_FUNCTION_END

#define DEBUG_EXCEPTION_INTERNAL_PRE_MESSAGE_TEXT(expressionToCall)       \
    ERROR << "Attempt to call" << DS_DEFAULT << #expressionToCall         \
          << DS_ERROR << "has failed";                                    \
    INFO << "exception has been thrown";


/// Try/catch expression and print error message (if exception is derived
/// from std::exteption), otherwise print `Unknown exception`
#define DEBUG_EXCEPTION(expressionToCall)                                 \
    try {                                                                 \
        INFO << "Calling" << DS_DEFAULT << #expressionToCall;             \
        { expressionToCall; }                                             \
        INFO << "Succesfull";                                             \
        DMS_POST_ACTION                                                   \
    } catch (const std::exception& exc) {                                 \
        DEBUG_EXCEPTION_INTERNAL_PRE_MESSAGE_TEXT(expressionToCall)       \
        INFO_1 << "Exception text:" << DS_DEFAULT << exc.what();          \
        DEBUG_INTERNAL_POSITION_IN_CODE                                   \
        DMS_POST_ACTION                                                   \
    } catch (...) {                                                       \
        DEBUG_EXCEPTION_INTERNAL_PRE_MESSAGE_TEXT(expressionToCall)       \
        INFO_1 << "Exception text:" << DS_DEFAULT << "Unknown exception"; \
        DEBUG_INTERNAL_POSITION_IN_CODE                                   \
        DMS_POST_ACTION                                                   \
    }


/*! \brief Print name of the expression, call it and if call
 * didn't trow any exceptions
 * or crash program Print Success.
 * Exception will be called into new level of indentation
 */
#define DEBUG_EXPRESSION(expressionToCall)                                \
    INFO << "Calling" << DS_DEFAULT << #expressionToCall;                 \
    DEBUG_INDENT                                                          \
    expressionToCall;                                                     \
    DEBUG_DEINDENT                                                        \
    INFO << "Success";


/// Save expression to `temporaryVariable` then execute
/// `DEBUG_FUNCTION_END` and call `return temporaryVariable`
#define DEBUG_EXPRESSION_RETURN(expressionToCall)                         \
    DEBUG_EXPRESSION(auto temporyVariable = expressionToCall;)            \
    /* DEBUG_FUNCTION_END  */                                             \
    return temporyVariable;
