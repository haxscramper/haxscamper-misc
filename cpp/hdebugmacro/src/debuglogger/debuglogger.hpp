#pragma once

#include <iostream>
#include <string>
#ifdef QT_VERSION
#    include <QString>
#endif

#include "debugger_settings.hpp"
#include "debugger_styles.hpp"
#include "log_string.hpp"
#include "support.hpp"


namespace dbg {
class DebugMessageBuilder
{
  public:
    ~DebugMessageBuilder();

    template <class Arg>
    DebugMessageBuilder& operator<<(Arg arg);

  private:
    int skipNextSpacing = 0;
};


class DebugLogger
{
    friend class DebugMessageBuilder;

  public:
    static void increaseIndent([[maybe_unused]] int _indent = 2) {
        if (doPrinting) {
            indent.increaseIndent();
        }
    }

    static void decreaseIndent([[maybe_unused]] int _indent = 2) {
        if (doPrinting) {
            indent.decreaseIndent();
        }
    }

    static DebugMessageBuilder log() {
        instance->setNewline(false);
        return DebugMessageBuilder();
    }


    static void log(LogString message) {
        using string_vec = std::vector<std::string>;

        string_vec newline_split = spt::split_newlines(message.toString());

        std::cout << getSettingsRef().getEscapeCodes();
        if (newline_split.size() == 1) {
            //        std::cout << instance->defaultPrefix;
            if (instance->getSettings().isNewline()) {
                spt::print(indent.toIndentString(), false);
            }
            spt::print(newline_split.front(), false);
        } else {
            spt::print(newline_split.front(), true);
            for (string_vec::iterator iter = (++newline_split.begin());
                 iter != (--newline_split.end());
                 ++iter) {
                spt::print(indent.toIndentString(), false);

                std::cout << instance->defaultPrefix;
                std::cout << getSettingsRef().getEscapeCodes();
                spt::print(*iter, true);
            }

            std::cout << instance->defaultPrefix;
            spt::print(indent.toIndentString(), false);

            std::cout << getSettingsRef().getEscapeCodes();
            spt::print(newline_split.back(), false);
        }

        std::cout << ESCAPE_SEQ + "0m";
    }

    static void setNewline(bool value = true) {
        currentSettings.setStyle(PrintSettings::IsNewline, value);
    }

    /// \brief Set debug style settings according to settings
    static void setSettings(LogSettings settings) {
        int old_indent  = currentSettings.getIndent();
        currentSettings = settings;
        if (old_indent != 0) {
            currentSettings.setIndent(old_indent);
        }
    }

    /// \brief Return current style settings
    static LogSettings getSettings() {
        return instance->currentSettings;
    }

    /// \brief Return reference to current style settings
    static LogSettings& getSettingsRef() {
        return instance->currentSettings;
    }

    static DebugLogger* getInstance() {
        return instance;
    }

    static uint getIndentSize() {
        return indent.getIndentSize();
    }

    static void setDefaultPrefix(std::string value) {
        defaultPrefix = value;
    }

    enum class IndentFrontStyle
    {
        None,
        Rainbow,
        Vertical
    };

    static void setIndentFront(DebugLogger::IndentFrontStyle style) {
        indent.setIndentFront(style);
    }
    static void setFrontModulo() {
    }

    void operator<<(LogString string) {
        if (doPrinting) {
            std::cout << instance->defaultPrefix;
            if (addIndent) {
                spt::print(indent.toIndentString(), false);
                addIndent = false;
            }
            instance->setNewline(false);
            log(string);
            putchar(' ');
            instance->setNewline(true);
        }
    }

    void operator<<(Background value) {
        std::cout << instance->defaultPrefix;
        instance->currentSettings.setBackground(value);
    }

    void operator<<(LogSettings settings) {
        std::cout << instance->defaultPrefix;
        instance->setSettings(settings);
    }

    void operator<<(Foreground _foreg) {
        std::cout << instance->defaultPrefix;
        instance->currentSettings.setForeground(_foreg);
    }

    void operator<<(PrintSettings _settings) {
        instance->currentSettings.setStyle(_settings);
    }

    class Indent
    {
      public:
        struct Step {
            struct Settings {
                int              size      = 2;
                bool             formatted = true;
                IndentFrontStyle style;
            } settings;

            LogSettings formatting;
        };

        void increaseIndent() {
            if (doPrinting) {
                Step newStep;
                indentSteps.push_back(newStep);
            }
        }
        void decreaseIndent() {
            if (doPrinting) {
                if (indentSteps.size() != 0) {
                    indentSteps.pop_back();
                }
            }
        }

        // TODO rename to getIndenCharCount
        uint getIndentSize() {
            uint indent = 0;
            for (Step& step : indentSteps) {
                indent += step.settings.size;
            }
            return indent;
        }

        std::string toIndentString() {
            std::string result;
            for (Step& step : indentSteps) {
                std::string indentFront;
                switch (step.settings.style) {
                    case IndentFrontStyle::None: indentFront = "  "; break;
                    case IndentFrontStyle::Rainbow:
                        indentFront = "  ";
                        break;
                    case IndentFrontStyle::Vertical:
                        indentFront = "  ";
                        break;
                }

                if (indentFront.size() < step.settings.size) {
                    for (uint i = 0;
                         i < indentFront.size() - step.settings.size;
                         ++i) {
                        indentFront += "| ";
                    }
                }
                result += step.formatting.getEscapeCodes() + indentFront;
            }
            return result;
        }

        void setIndentFront(IndentFrontStyle style) {
            for (auto& step : indentSteps) {
                step.settings.style = style;
            }
        }

        std::string indentStart;

      private:
        std::vector<Indent::Step> indentSteps;
    };

    static void enableIndent() {
        addIndent = true;
    }

    static void toggleDebug(bool print = true) {
        doPrinting = print;
    }


  private:
    DebugLogger() {
        instance = new DebugLogger();
    }

    static inline Indent       indent;
    static inline bool         doPrinting     = true;
    static inline bool         addIndent      = true;
    static inline bool         skipNexSpacing = false;
    static inline DebugLogger* instance       = nullptr;
    static inline LogSettings  currentSettings;
    static inline std::string  defaultPrefix = "";
};

inline DebugMessageBuilder::~DebugMessageBuilder() {
    if (DebugLogger::doPrinting) {
        std::cout << "\n";
    }
    DebugLogger::setSettings(LogSettings());
    DebugLogger::enableIndent();
}

template <>
inline DebugMessageBuilder& DebugMessageBuilder::operator<<(
    LogString string) {
    DebugLogger::setNewline(false);
    DebugLogger::log(string);
    DebugLogger::setNewline(true);

    return *this;
}


template <class Arg>
DebugMessageBuilder& DebugMessageBuilder::operator<<(Arg arg) {
    DebugLogger::setNewline(false);
    *DebugLogger::getInstance() << arg;
    DebugLogger::setNewline(true);
    skipNextSpacing = 2;
    return *this;
}
} // namespace dbg
