/*!
 * \file debug_macro_style_extra.hpp
 * \brief Extra styling macro
 */

#include "debuglogger.hpp"

//#= Extra styling macro
//#== Color
#define FG_DEFAULT dbg::Foreground::Default
#define FG_BLACK dbg::Foreground::Black
#define FG_RED dbg::Foreground::Red
#define FG_GREEN dbg::Foreground::Green
#define FG_YELLOW dbg::Foreground::Yellow
#define FG_BLUE dbg::Foreground::Blue
#define FG_MAGENTA dbg::Foreground::Magenta
#define FG_CYAN dbg::Foreground::Cyan
#define FG_LIGHTGRAY dbg::Foreground::LightGray
#define FG_DARKGRAY dbg::Foreground::DarkGray
#define FG_LIGHTRED dbg::Foreground::LightRed
#define FG_LIGHTGREEN dbg::Foreground::LightGreen
#define FG_LIGHTYELLOW dbg::Foreground::LightYellow
#define FG_LIGHTBLUE dbg::Foreground::LightBlue
#define FG_LIGHTMAGENTA dbg::Foreground::LightMagenta
#define FG_LIGHTCYAN dbg::Foreground::LightCyan
#define FG_WHITE dbg::Foreground::White

#define BG_DEFAULT dbg::Background::Default
#define BG_BLACK dbg::Background::Black
#define BG_RED dbg::Background::Red
#define BG_GREEN dbg::Background::Green
#define BG_YELLOW dbg::Background::Yellow
#define BG_BLUE dbg::Background::Blue
#define BG_MAGENTA dbg::Background::Magenta
#define BG_CYAN dbg::Background::Cyan
#define BG_LIGHTGRAY dbg::Background::LightGray
#define BG_DARKGRAY dbg::Background::DarkGray
#define BG_LIGHTRED dbg::Background::LightRed
#define BG_LIGHTGREEN dbg::Background::LightGreen
#define BG_LIGHTYELLOW dbg::Background::LightYellow
#define BG_LIGHTBLUE dbg::Background::LightBlue
#define BG_LIGHTMAGENTA dbg::Background::LightMagenta
#define BG_LIGHTCYAN dbg::Background::LightCyan
#define BG_WHITE dbg::Background::White
//#== Style
#define DS_BOLD dbg::PrintSettings::Bold
#define DS_DIM dbg::PrintSettings::Dim
#define DS_ITALIC dbg::PrintSettings::Italic
#define DS_UNDERLINED dbg::PrintSettings::Underlined
#define DS_BLINK dbg::PrintSettings::Blink
#define DS_INVERTED dbg::PrintSettings::Inverted
#define DS_STRIKETHROUGH dbg::PrintSettings::Strikethrough
