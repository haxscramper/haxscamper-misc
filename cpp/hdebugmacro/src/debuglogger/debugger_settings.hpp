#pragma once

#include "debugger_styles.hpp"
#include <halgorithm/setting_flags.hpp>
#include <utility>
#include <variant>


namespace dbg {


/// \todo Organize functions into groups and write documentation
/// \todo Add setting to default
class LogSettings
{
  public:
    LogSettings& setStyle(PrintSettings setting, bool value = true) {
        if (setting == PrintSettings::Inverted) {
            settings[setting] = !settings.test(setting);
        } else {
            settings[setting] = value;
        }
        return *this;
    }

    void setBackground(Background _background) {
        background = _background;
    }

    void setForeground(Foreground _foreground) {
        foreground = _foreground;
    }

    void clear() {
        settings.clear();
        background = Background::Default;
        foreground = Foreground::Default;
    }


    LogSettings& operator+=(LogSettings other) {
        settings |= other.settings;

        if (!other.isDefaultBackg()) {
            setBackground(other.getBackground());
        }

        if (!other.isDefaultForeg()) {
            setForeground(other.getForeground());
        }

        return *this;
    }

    spt::SettingFlags<PrintSettings> settings = PrintSettings::None;
    int                              indent   = 0;


    Background background = Background::Default;
    Foreground foreground = Foreground::Default;


    //#=== Member access functions
    Background getBackground() const {
        return background;
    }

    Foreground getForeground() const {
        return foreground;
    }

    int getIndent() const {
        return indent;
    }

    void setIndent(int value) {
        indent = value;
    }

    //#=== Value check functions
    bool isSet(PrintSettings value) const {
        return settings.test(value);
    }

    bool isNewline() const {
        return settings.test(PrintSettings::IsNewline);
    }

    bool isDefaultBackg() const {
        return background == Background::Default;
    }

    bool isDefaultForeg() const {
        return foreground == Foreground::Default;
    }


    // Uncomment to debug escape sequence generation
    //#define ESCAPE_SEQ std::string("[")
    //#=== Escape code generation
    std::string getEscapeCodes() const {
        std::string res;

        if (!isDefaultBackg()) {
            res += ESCAPE_SEQ + spt::to_string(background) + "m";
        }

        if (!isDefaultForeg()) {
            res += ESCAPE_SEQ + spt::to_string(foreground) + "m";
        }

        std::string styles;
        int         count = 0;
        for (const std::pair<const PrintSettings, bool>& setting :
             settings) {
            if (setting.second && setting.first != PrintSettings::None) {
                styles += spt::to_string(setting.first) + ";";
                ++count;
            }
        }

        if (count > 0) {
            styles.resize(styles.size() - 1);
            res += ESCAPE_SEQ + styles + "m";
        }
        return res;
    }
};
} // namespace dbg
