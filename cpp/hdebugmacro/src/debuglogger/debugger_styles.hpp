#pragma once

#include <cstdint>
#include <iostream>
#include <string>
#include <vector>

//#== Helper macro
#define ESCAPE_SEQ std::string("\033[")
#define ESCAPE_SEQ_NULL std::string("\033[0m")
#define TEMPORARY_UNUSED(x) (void)x;
#define BASE_CLASS_UNUSED(x) (void)x;

#define __NO_GCC__FILENAME__                                              \
    ({                                                                    \
        const char* filename_start = __FILE__;                            \
        const char* filename       = filename_start;                      \
        while (*filename != '\0')                                         \
            filename++;                                                   \
        while ((filename != filename_start) && (*(filename - 1) != '/'))  \
            filename--;                                                   \
        filename;                                                         \
    })


namespace dbg {
enum class Foreground
{
    Default      = 39,
    Black        = 30,
    Red          = 31,
    Green        = 32,
    Yellow       = 33,
    Blue         = 34,
    Magenta      = 35,
    Cyan         = 36,
    LightGray    = 37,
    DarkGray     = 90,
    LightRed     = 91,
    LightGreen   = 92,
    LightYellow  = 93,
    LightBlue    = 94,
    LightMagenta = 95,
    LightCyan    = 96,
    White        = 97
};

/// Return n'th foreground enum item starting from Foreground::Black.
/// Numeration starts from 0, if supplied idex is wrong Foreground::Default
/// is returned
inline Foreground idx_to_foreground(int idx) {
    switch (idx) {
        case 0: return Foreground::Black;
        case 1: return Foreground::Red;
        case 2: return Foreground::Green;
        case 3: return Foreground::Yellow;
        case 4: return Foreground::Blue;
        case 5: return Foreground::Magenta;
        case 6: return Foreground::Cyan;
        case 7: return Foreground::LightGray;
        case 8: return Foreground::DarkGray;
        case 9: return Foreground::LightRed;
        case 10: return Foreground::LightGreen;
        case 11: return Foreground::LightYellow;
        case 12: return Foreground::LightBlue;
        case 13: return Foreground::LightMagenta;
        case 14: return Foreground::LightCyan;
        case 15: return Foreground::White;
        default: return Foreground::Default;
    }
}


enum class Background
{
    Default      = 49,
    Black        = 40,
    Red          = 41,
    Green        = 42,
    Yellow       = 43,
    Blue         = 44,
    Magenta      = 45,
    Cyan         = 46,
    LightGray    = 47,
    DarkGray     = 100,
    LightRed     = 101,
    LightGreen   = 102,
    LightYellow  = 103,
    LightBlue    = 104,
    LightMagenta = 105,
    LightCyan    = 106,
    White        = 107
};

/// Return n'th background enum item starting from Background::Black.
/// Numeration starts from 0, if supplied idex is wrong Background::Default
/// is returned
inline Background idx_to_background(int idx) {
    switch (idx) {
        case 0: return Background::Black;
        case 1: return Background::Red;
        case 2: return Background::Green;
        case 3: return Background::Yellow;
        case 4: return Background::Blue;
        case 5: return Background::Magenta;
        case 6: return Background::Cyan;
        case 7: return Background::LightGray;
        case 8: return Background::DarkGray;
        case 9: return Background::LightRed;
        case 10: return Background::LightGreen;
        case 11: return Background::LightYellow;
        case 12: return Background::LightBlue;
        case 13: return Background::LightMagenta;
        case 14: return Background::LightCyan;
        case 15: return Background::White;
        default: return Background::Default;
    }
}

enum class PrintSettings
{
    None,
    Bold          = 1,
    Dim           = 2,
    Italic        = 3,
    Underlined    = 4,
    Blink         = 5,
    Inverted      = 7,
    Strikethrough = 9,
    IsNewline
};

} // namespace dbg

namespace spt {
template <class T, typename = std::enable_if_t<std::is_enum<T>::value>>
std::string to_string(T t) {
    return std::to_string(static_cast<int>(t));
}
} // namespace spt
