#pragma once

#if defined(QT_GUI_LIB) && defined(QT_WIDGETS_LIB)
#    include "../debuglogger/debuglogger.hpp"
#    include <QAbstractItemView>
#    include <QColor>
#    include <QHeaderView>
#    include <QList>
#    include <QMainWindow>
#    include <QString>
#    include <QTableWidget>
#    include <vector>

namespace dbg {
/// \todo Class has no out-of-line virtual functions
class DebugWindow : public QMainWindow
{
  public:
    static DebugWindow* instance() {
        if (windowInstance == nullptr) {
            windowInstance = new DebugWindow;
        }
        return windowInstance;
    }

    static void addRow() {
        if (windowInstance->isFinalized) {
            table().clear();
            while (table().rowCount() > 0) {
                table().removeRow(0);
            }
            instance()->isFinalized = false;
        }
        int newRow = table().rowCount();

        table().insertRow(newRow);
        table().setItem(newRow, 0, new QTableWidgetItem);
        table().setItem(newRow, 1, new QTableWidgetItem);
        table().update();
    }

    static void setFinalized() {
        instance()->isFinalized = true;
    }

    static QTableWidget& table() {
        return instance()->tableWidget;
    }

  private:
    DebugWindow();

    bool                       isFinalized    = false;
    static inline DebugWindow* windowInstance = nullptr;
    QTableWidget               tableWidget;
};

inline DebugWindow::DebugWindow() {
    this->resize(400, 600);
    tableWidget.setColumnCount(2);
    tableWidget.horizontalHeader()->hide();
    tableWidget.verticalHeader()->hide();
    tableWidget.horizontalHeader()->setSectionResizeMode(
        QHeaderView::Stretch);
    this->setCentralWidget(&tableWidget);
}


class DebugWindowRowBuilder
{
  public:
    DebugWindowRowBuilder(int _column) {
        column = _column;
    }
    ~DebugWindowRowBuilder() {
        if (column == 0) {
            DebugWindow::addRow();
        }
        int row = table().rowCount() - 1;
        table().item(row, column)->setBackgroundColor(background);
        table().item(row, column)->setForeground(foreground);
        table().item(row, column)->setData(Qt::DisplayRole, content);
    }
    static DebugWindowRowBuilder Row(int column = 0) {
        return DebugWindowRowBuilder(column);
    }

    DebugWindowRowBuilder& operator<<(LogString data) {
        content = QString::fromStdString(data.toString());
        return *this;
    }

    DebugWindowRowBuilder& operator<<(Background _background) {
        switch (_background) {
            case Background::Default: {
                background = QColor(Qt::white);
            } break;
            case Background::Black: {
                background = QColor(Qt::black);
            } break;
            case Background::Red: {
                background = QColor(Qt::darkRed);
            } break;
            case Background::Green: {
                background = QColor(Qt::darkGreen);
            } break;
            case Background::Yellow: {
                background = QColor(Qt::darkYellow);
            } break;
            case Background::Blue: {
                background = QColor(Qt::darkBlue);
            } break;
            case Background::Magenta: {
                background = QColor(Qt::darkMagenta);
            } break;
            case Background::Cyan: {
                background = QColor(Qt::darkCyan);
            } break;
            case Background::LightGray: {
                background = QColor(Qt::lightGray);
            } break;
            case Background::DarkGray: {
                background = QColor(Qt::darkGray);
            } break;
            case Background::LightRed: {
                background = QColor(Qt::red);
            } break;
            case Background::LightGreen: {
                background = QColor(Qt::green);
            } break;
            case Background::LightYellow: {
                background = QColor(Qt::yellow);
            } break;
            case Background::LightBlue: {
                background = QColor(Qt::blue);
            } break;
            case Background::LightMagenta: {
                background = QColor(Qt::magenta);
            } break;
            case Background::LightCyan: {
                background = QColor(Qt::cyan);
            } break;
            case Background::White: {
                background = QColor(Qt::white);
            } break;
        }
        return *this;
    }

    DebugWindowRowBuilder& operator<<(Foreground _foreground) {
        switch (_foreground) {
            case Foreground::Default: {
                foreground = QColor(Qt::white);
            } break;
            case Foreground::Black: {
                foreground = QColor(Qt::black);
            } break;
            case Foreground::Red: {
                foreground = QColor(Qt::darkRed);
            } break;
            case Foreground::Green: {
                foreground = QColor(Qt::darkGreen);
            } break;
            case Foreground::Yellow: {
                foreground = QColor(Qt::darkYellow);
            } break;
            case Foreground::Blue: {
                foreground = QColor(Qt::darkBlue);
            } break;
            case Foreground::Magenta: {
                foreground = QColor(Qt::darkMagenta);
            } break;
            case Foreground::Cyan: {
                foreground = QColor(Qt::darkCyan);
            } break;
            case Foreground::LightGray: {
                foreground = QColor(Qt::lightGray);
            } break;
            case Foreground::DarkGray: {
                foreground = QColor(Qt::darkGray);
            } break;
            case Foreground::LightRed: {
                foreground = QColor(Qt::red);
            } break;
            case Foreground::LightGreen: {
                foreground = QColor(Qt::green);
            } break;
            case Foreground::LightYellow: {
                foreground = QColor(Qt::yellow);
            } break;
            case Foreground::LightBlue: {
                foreground = QColor(Qt::blue);
            } break;
            case Foreground::LightMagenta: {
                foreground = QColor(Qt::magenta);
            } break;
            case Foreground::LightCyan: {
                foreground = QColor(Qt::cyan);
            } break;
            case Foreground::White: {
                foreground = QColor(Qt::white);
            } break;
        }
        return *this;
    }

    DebugWindowRowBuilder& operator<<(int _number) {
        content = QString::number(_number);
        return *this;
    }


  private:
    int           column = 0;
    QTableWidget& table() {
        return DebugWindow::table();
    }
    QColor  background = QColor(Qt::white);
    QColor  foreground = QColor(Qt::black);
    QString content;
};


} // namespace dbg

#endif // QT_VERSION
