#pragma once

#define TEMPORARY_UNUSED(x) (void)x;
#define BASE_CLASS_UNUSED(x) (void)x;

#define __FILENAME__                                                      \
    ({                                                                    \
        const char* filename_start = __FILE__;                            \
        const char* filename       = filename_start;                      \
        while (*filename != '\0')                                         \
            filename++;                                                   \
        while ((filename != filename_start) && (*(filename - 1) != '/'))  \
            filename--;                                                   \
        filename;                                                         \
    })

#define CHECK_IF_FILE_EXIST(fileName)                                     \
    QFile temp_file(fileName);                                            \
    if (temp_file.exists()) {                                             \
        DebugLogger::log() << "File" << fileName << "exists";             \
    } else {                                                              \
        DebugLogger::log() << Style::Error << "Could not open file"       \
                           << Style::Default << fileName;                 \
        DEBUG_INDENT                                                      \
        DebugLogger::log() << Style::EmphasisDark                         \
                           << "Line:" << Style::Default << __LINE__;      \
        DebugLogger::log() << Style::EmphasisDark                         \
                           << "File:" << Style::Default << __FILENAME__;  \
        DEBUG_DEINDENT                                                    \
    }


