cmake_minimum_required(VERSION 3.6)
find_package(Qt5Core CONFIG REQUIRED)
find_package(Qt5Xml CONFIG REQUIRED)
find_package(Qt5Gui CONFIG REQUIRED)

file(GLOB_RECURSE sources "src/*.cpp")

message(${sources})

add_library(hsupport ${sources})

set_property(
  TARGET
  hsupport
  PROPERTY
  CXX_STANDARD
  17)

target_include_directories(
  hsupport PUBLIC
  ${Qt5Core_INCLUDE_DIRS}
  ${Qt5Xml_INCLUDE_DIRS}
  ${Qt5Gui_INCLUDE_DIRS}
  # QDELib
  ../halgorithm/include
  ../hdebugmacro/include
  )

target_link_libraries(
  hsupport
  ${Qt5Core_LIBRARIES}
  ${Qt5Xml_LIBRARIES}
  ${Qt5Gui_LIBRARIES}
  )
