#include "version.hpp"
#include "xml.hpp"

namespace spt {
//#= Version
QDomElement Version::toXML(QDomDocument& document, QString version_name)
    const {
    QDomElement root = document.createElement(version_name);

    root.appendChild(spt::textElement(
        document, "major-version", QString::number(majorVersion)));
    root.appendChild(spt::textElement(
        document, "minor-version", QString::number(minorVersion)));
    root.appendChild(spt::textElement(
        document, "patch-version", QString::number(patchVersion)));
    root.appendChild(
        spt::textElement(document, "description", description));

    return root;
}

void Version::readXML(QXmlStreamReader* xmlStream) {
    while (xmlStream->readNextStartElement()) {
        if (xmlStream->name() == "major-version") {
            majorVersion = xmlStream->readElementText().toUInt();
        } else if (xmlStream->name() == "minor-version") {
            minorVersion = xmlStream->readElementText().toUInt();
        } else if (xmlStream->name() == "patch-version") {
            patchVersion = xmlStream->readElementText().toUInt();
        } else if (xmlStream->name() == "description") {
            description = xmlStream->readElementText();
        } else {
            xmlStream->skipCurrentElement();
        }
    }
}
} // namespace spt
