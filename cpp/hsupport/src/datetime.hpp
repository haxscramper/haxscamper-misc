#pragma once
#include <QDateTime>
#include <QDomDocument>
#include <QDomElement>

namespace spt {

struct DateTimeRange {
    QDateTime   start;
    QDateTime   end;
    QDomElement toXML(QDomDocument& document, QString rangeName = "range")
        const;

    bool operator==(const DateTimeRange& other) const;
};


class DateTimeInterval
{
    uint _days;
    uint _hours;
    uint _minutes;
    uint _seconds;

  public:
    void rebalance();
    uint days() const;
    void days(const uint& days);
    uint hours() const;
    void hours(const uint& hours);
    uint minutes() const;
    void minutes(const uint& minutes);
    uint seconds() const;
    void seconds(const uint& seconds);

    QDomElement toXML(
        QDomDocument& document,
        QString       rangeName = "date-time-interval") const;
};

typedef std::vector<DateTimeRange> DateTimeRanges;


} // namespace spt
