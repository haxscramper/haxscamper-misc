#ifndef SETTING_FLAGS_HPP
#define SETTING_FLAGS_HPP


/*!
 * \file setting_flags.hpp
 * \brief
 */


//===    STL   ===//
#include <bitset>
#include <limits>


//  ////////////////////////////////////

namespace spt {
/*!
 * \brief Typesagfe wrapper for bitset
 *
 * Bitset mask for storing enum flags state
 */
template <typename Enum>
struct SettingFlags {
    SettingFlags() {
    }

    SettingFlags(const Enum& flag) {
        setFlag(flag, true);
    }

    SettingFlags& operator+=(const Enum& flag) {
        setFlag(flag, true);
        return *this;
    }

    SettingFlags& operator-=(const Enum& flag) {
        setFlag(flag, false);
        return *this;
    }

    SettingFlags& operator=(const Enum& flag) {
        bitset.reset();
        setFlag(flag, true);
        return *this;
    }

    bool operator==(const Enum& flag) {
        return getFlag(flag);
    }

    bool operator!=(const Enum& flag) {
        return !getFlag(flag);
    }

    typename std::bitset<
        static_cast<typename std::underlying_type_t<Enum>>(
            std::numeric_limits<Enum>::max()) +
        1>::reference
        operator[](const Enum& flag) {
        return bitset[static_cast<utype>(flag)];
    }

    bool test(const Enum& flag) const {
        return bitset.test(static_cast<utype>(flag));
    }


  private:
    void setFlag(const Enum& flag, bool val) {
        bitset[static_cast<utype>(flag)] = val;
    }

    bool getFlag(const Enum& flag) {
        return bitset[static_cast<utype>(flag)];
    }


    using utype = std::underlying_type_t<Enum>;
    std::bitset<static_cast<utype>(std::numeric_limits<Enum>::max()) + 1>
        bitset;
};
} // namespace spt

#endif // SETTING_FLAGS_HPP
