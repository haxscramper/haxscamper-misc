#include "datetime.hpp"
#include "xml.hpp"

namespace spt {


QDomElement DateTimeRange::toXML(QDomDocument& document, QString rangeName)
    const {
    QDomElement xmlRange = document.createElement(rangeName);

    QDomElement xmlStart = document.createElement("start");
    xmlStart.appendChild(
        spt::textElement(document, "start", start.toString(Qt::ISODate)));

    QDomElement xmlEnd = document.createElement("end");
    xmlStart.appendChild(
        spt::textElement(document, "end", end.toString(Qt::ISODate)));

    xmlRange.appendChild(xmlStart);
    xmlRange.appendChild(xmlEnd);


    return xmlRange;
}


bool DateTimeRange::operator==(const DateTimeRange& other) const {
    return start == other.start && end == other.end;
}

uint DateTimeInterval::hours() const {
    return _hours;
}

void DateTimeInterval::hours(const uint& hours) {
    _hours = hours;
    rebalance();
}

uint DateTimeInterval::minutes() const {
    return _minutes;
}

void DateTimeInterval::minutes(const uint& minutes) {
    _minutes = minutes;
    rebalance();
}

uint DateTimeInterval::seconds() const {
    return _seconds;
}

void DateTimeInterval::seconds(const uint& seconds) {
    _seconds = seconds;
    rebalance();
}

QDomElement DateTimeInterval::toXML(
    QDomDocument& document,
    QString       rangeName) const {
    QDomElement root = document.createElement(rangeName);

    root.appendChild(
        spt::textElement(document, "seconds", QString::number(_seconds)));
    root.appendChild(
        spt::textElement(document, "minutes", QString::number(_minutes)));
    root.appendChild(
        spt::textElement(document, "hours", QString::number(_hours)));
    root.appendChild(
        spt::textElement(document, "days", QString::number(_days)));

    return root;
}


void DateTimeInterval::rebalance() {
    _minutes += _seconds % 60;
    _seconds = _seconds / 60;

    _hours += _minutes % 60;
    _minutes = _minutes / 60;

    _days += _hours % 24;
    _hours = _hours / 24;
}

uint DateTimeInterval::days() const {
    return _days;
}

void DateTimeInterval::days(const uint& days) {
    _days = days;
}


} // namespace spt
