#include "xml.hpp"


namespace spt {
QDomElement listVariantToXML(
    boost::variant<QStringList, QStringList*> list,
    QDomDocument&                             document,
    QString                                   listName,
    QString                                   elementName) {
    if (list.which() == 0) {
        return listToXML(
            boost::get<QStringList>(list),
            document,
            listName,
            elementName);
    } else {
        return listToXML(
            *boost::get<QStringList*>(list),
            document,
            listName,
            elementName);
    }
}


QDomElement listToXML(
    const QStringList& list,
    QDomDocument&      document,
    QString            listName,
    QString            elementName) {
    QDomElement XMLList = document.createElement(listName);
    for (const QString& item : list) {
        QDomElement XMLItem = document.createElement(elementName);
        XMLItem.appendChild(document.createTextNode(item));
        XMLList.appendChild(XMLItem);
    }

    return XMLList;
}


void writeDocument(QDomDocument& document, QString path) {
    QFile file(path);
    file.open(QIODevice::WriteOnly | QIODevice::Text);
    QTextStream stream(&file);
    stream << document.toString();
    file.close();
}


void writeDocument(QDomDocument& document, QFile& file) {
    QTextStream stream(&file);
    stream << document.toString();
    file.close();
}


void overrideFile(QDomDocument& document, QString path) {
    QFile file(path);
    if (file.remove()) {
        writeDocument(document, path);
    }
}

QStringList listFromXML(
    QXmlStreamReader* xmlStream,
    QString           listName,
    QString           itemName) {
    QStringList result;
    if (xmlStream->name() == listName) {
        while (xmlStream->readNextStartElement()) {
            if (xmlStream->name() == itemName) {
                result.append(xmlStream->readElementText());
            } else {
                xmlStream->skipCurrentElement();
            }
        }
    }
    return result;
}


QDomElement textElement(
    QDomDocument& document,
    QString       elementName,
    QString       elementText,
    bool          isCDATA) {
    QDomElement domElement = document.createElement(elementName);
    if (isCDATA) {
        domElement.appendChild(document.createCDATASection(elementText));
    } else {
        domElement.appendChild(document.createTextNode(elementText));
    }

    return domElement;
}


} // namespace spt
