#pragma once

//#== Declare

#define ___SET(memberName, suffix)                                        \
    ;                                                                     \
                                                                          \
  public:                                                                 \
    inline void set##suffix(const decltype(memberName)& val) {            \
        memberName = val;                                                 \
    }                                                                     \
                                                                          \
  private:

#define ___GET(memberName, suffix)                                        \
    ;                                                                     \
                                                                          \
  public:                                                                 \
    inline decltype(memberName) get##suffix() const {                     \
        return memberName;                                                \
    }                                                                     \
                                                                          \
  private:


/*!
 Add macro for setting variables on reference object This is useful if
 you don't want to write parametrized contructors and also want to use
 named parameters when building an object. Using this macro you will be
 able to do things like `Objc()_field1(val2).field2(val2)` to set
 values of the fields without the need to use getters and setters or
 create additional variable.

 \todo Add support for working with classes that do not support copying
*/
#define ___NAM(memberName)                                                \
    ;                                                                     \
                                                                          \
  public:                                                                 \
    auto& _##memberName(const decltype(memberName)& val) {                \
        memberName = val;                                                 \
        return *this;                                                     \
    }                                                                     \
                                                                          \
  private:

#define ___SET_GET(memberName, suffix)                                    \
    ___SET(memberName, suffix);                                           \
    ___GET(memberName, suffix);

#define ___SET_GET_NAM(memberName, suffix)                                \
    ___SET(memberName, suffix);                                           \
    ___GET(memberName, suffix);                                           \
    ___NAM(memberName);

//#== Aggregate get/set declare


#define ___SET_F(field, memberName, suffix)                               \
    ;                                                                     \
                                                                          \
  public:                                                                 \
    inline void set##suffix(const decltype(field.memberName)& val) {      \
        field.memberName = val;                                           \
    }                                                                     \
                                                                          \
  private:

#define ___GET_F(field, memberName, suffix)                               \
    ;                                                                     \
                                                                          \
  public:                                                                 \
    inline decltype(field.memberName) get##suffix() const {               \
        return field.memberName;                                          \
    }                                                                     \
                                                                          \
  private:


#define ___SET_GET_F(field, memberName, suffix)                           \
    ___SET_F(field, memberName, suffix);                                  \
    ___GET_F(field, memberName, suffix);

//#== Define

#define ___GET_DEF(memberName, suffix)                                    \
    ;                                                                     \
                                                                          \
  public:                                                                 \
    decltype(memberName) get##suffix() const;                             \
                                                                          \
  private:

#define ___SET_DEF(memberName, suffix)                                    \
    ;                                                                     \
                                                                          \
  public:                                                                 \
    void set##suffix(const decltype(memberName)& val);                    \
                                                                          \
  private:


#define ___SET_GET_DEF(memberName, suffix)                                \
    ___SET_DEF(memberName, suffix);                                       \
    ___GET_DEF(memberName, suffix);


#define ___SET_CP(memberName, suffix)                                     \
    ;                                                                     \
                                                                          \
  public:                                                                 \
    inline void set##suffix(decltype(memberName) val) {                   \
        memberName = std::move(val);                                      \
    }                                                                     \
                                                                          \
  private:
