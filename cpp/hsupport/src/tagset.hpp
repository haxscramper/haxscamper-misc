#ifndef TAGLIST_HPP
#define TAGLIST_HPP

#include "misc.hpp"
#include <QDomDocument>
#include <QDomElement>
#include <QString>
#include <QStringList>
#include <unordered_set>

#include "tag.hpp"

namespace std {
template <>
struct hash<spt::Tag> {
    std::size_t operator()(const spt::Tag& tag) const {
        using std::hash;
        using std::size_t;
        using std::string;

        // Compute individual hash values for first,
        // second and third and combine them using XOR
        // and bit shifting:

        // !!! TODO Add hash function implementation

        return 0;
    }
};

} // namespace std

namespace spt {
/*!
 * \brief TagList class represents list of strings that are used for
 * tagging.
 * \todo Move to spt namespace
 */
class TagSet
{
  public:
    TagSet() = default;
    TagSet(QStringList&& tags);

    bool contains(Tag tag);
    void addTag(Tag tag, bool force = false);

    void addTags(TagSet tags);

    bool isEmpty() const;
    void removeTag(Tag tag);

    static TagSet parseFromTagList(const std::vector<std::string> tags);

    bool   operator==(const TagSet& other) const;
    size_t size() const;

    std::unordered_set<Tag>        getTags() const;
    const std::unordered_set<Tag>& getTagsCRef() const;

    std::unordered_set<Tag>::const_iterator begin() const;
    std::unordered_set<Tag>::const_iterator end() const;


    QDomElement toXML(
        QDomDocument& document,
        QString       listName = "tags",
        QString       tagName  = "tagitem") const;

    QStringList toStringList() const;

    static TagSet fromXML(
        QXmlStreamReader* xmlStream,
        QString           listName = "tags",
        QString           tagName  = "tagitem");

    std::string toDebugString(
        size_t indentation     = 0,
        bool   printEverything = false) const;

  private:
    std::unordered_set<Tag> list;
};
} // namespace spt


#endif // TAGLIST_HPP
