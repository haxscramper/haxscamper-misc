/*!
 * \file support.cpp
 * \brief
 */

//===   Qt    ===//
#include <QDebug>
#include <QDir>
#include <QDomDocument>
#include <QFile>
#include <QFileInfo>
#include <QImage>
#include <QMouseEvent>
#include <QPixmap>
#include <QStringList>
#include <QTextStream>
#include <QTimer>
#include <QUrl>
#include <QVector>
#include <QXmlStreamReader>

//===   STL   ===//
#include <functional>
//#include <filesystem>
//#include <system_error>

//===  Local  ===//
#include <halgorithm/all.hpp>
#include <hdebugmacro/all.hpp>

//=== Sibling ===//
#include "misc.hpp"


//  ////////////////////////////////////


//#  //////////////////////////////////////////////////////////////////////
//#  Support namespace
//#  //////////////////////////////////////////////////////////////////////

namespace spt {
QString getRadomString(QString alphabet, int length) {
    QString randomString;
    for (int i = 0; i < length; ++i) {
        int   index    = qrand() % alphabet.length();
        QChar nextChar = alphabet.at(index);
        randomString.append(nextChar);
    }
    return randomString;
}


QString getRandomBase32(int length) {
    return getRadomString("ABCDEFGHIJKLMNOPQRSTUVWXYZ234567", length);
}


QString getRandomBase64(int length) {
    return getRadomString(
        "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwx "
        "yz0123456789+/",
        length);
}

QStringList getRandomBase64List(int listLength, int itemSize) {
    QStringList res;
    for (size_t i = 0; i < listLength; ++i) {
        res.append(spt::getRandomBase32(itemSize));
    }
    return res;
}


// FIXME Can be implemented faster using hashset
QString extensionsToFilter(QStringList extensionList) {
    QString     result = "All files (*.*);;";
    QStringList filters;
    for (const QString& extension : extensionList) {
        if (extension == "jpg" || extension == "jpeg") {
            if (!filters.contains("JPEG (*.jpg, *.jpeg);;")) {
                filters.append("JPEG (*.jpg, *.jpeg);;");
            }
        }
    }

    result += filters.join("");
    return result;
}


QMargins zeroMargins() {
    return QMargins(0, 0, 0, 0);
}

QMargins equalMargins(int size) {
    return QMargins(size, size, size, size);
}


QSize scale(QSize size, qreal ratio) {
    QSize result;
    result.setHeight(size.height() * ratio);
    result.setWidth(size.width() * ratio);
    return result;
}

QString getNewScratchFile(int length, QString prefix, QString suffix) {
    return prefix + spt::getRandomBase32(length) + suffix;
}


void saveToFile(
    QPixmap& pixmap,
    QString  absoluteFileName,
    QString  ext,
    int      quality) {
    const char* extString = ext.toStdString().c_str();
    absoluteFileName += "." + QString(extString);
    pixmap.save(absoluteFileName, extString, quality);
}


bool removeDirContent(QString absolutePath) {
    bool success = removeDir(absolutePath);
    if (success) {
        QDir().mkpath(absolutePath);
    }
    return success;
}


bool removeDir(QString absoltePath) {
    bool result = true;
    QDir dir(absoltePath);

    if (dir.exists(absoltePath)) {
        Q_FOREACH (
            QFileInfo info,
            dir.entryInfoList(
                QDir::NoDotAndDotDot //
                    | QDir::System   //
                    | QDir::Hidden   //
                    | QDir::AllDirs  //
                    | QDir::Files,
                QDir::DirsFirst)) {
            if (info.isDir()) {
                result = removeDir(info.absoluteFilePath());
            } else {
                result = QFile::remove(info.absoluteFilePath());
            }

            if (!result) {
                return result;
            }
        }
        result = dir.rmdir(absoltePath);
    }

    return result;
}

/// Return minimum of {width, height} for QSize
int minDim(const QSize& size) {
    return size.height() < size.width() ? size.height() : size.width();
}


[[noreturn]] void throw_file_does_not_exist(QString& path) {
    throw std::invalid_argument(
        "File file does not exist" + path.toStdString());
}


[[noreturn]] void throw_file_cannot_be_opened(QString& path) {
    throw std::invalid_argument(
        "File cannot be opened" + path.toStdString());
}

bool createFilePath(QString absoluteTargetPath) {
    QFileInfo fileInfo(absoluteTargetPath);
    if (!fileInfo.exists()) {
        if (!QFileInfo(absoluteTargetPath).dir().exists()) {
            if (!QDir().mkpath(fileInfo.absoluteDir().absolutePath())) {
                return false;
            }
        }

        if (!QFile(absoluteTargetPath).open(QIODevice::WriteOnly)) {
            return false;
        } else {
            return true;
        }
    } else {
        return true;
    }
}


/*!
 * \todo Implement ExtensionTypeMapping
 */
bool isTextExtension(
    QString                                extension,
    [[maybe_unused]] ExtensionTypeMapping* mapping) {
    return spt::contains<std::string>(
        {"txt", "pdf", "md", "html"}, extension.toStdString());
}


/*!
 * \todo Implement ExtensionTypeMapping
 */
bool isImageExtension(
    QString                                extension,
    [[maybe_unused]] ExtensionTypeMapping* mapping) {
    return spt::contains<std::string>(
        {"jpg", "png", "jpeg"}, extension.toStdString());
}

} // namespace spt


double rad(double deg) {
    return deg * pi / 180;
}
