#include "tagset.hpp"
#include <QXmlStreamReader>
#include <halgorithm/all.hpp>
#include <hdebugmacro/all.hpp>

namespace spt {
TagSet::TagSet(QStringList&& tags) {
    for (QString& tag : tags) {
        list.insert(Tag::parseFromString(tag));
    }
}

bool TagSet::contains(Tag tag) {
    return spt::contains(list, tag);
}


void TagSet::addTag(Tag tag, bool force) {
    if (force || !spt::contains(list, tag)) {
        list.insert(tag);
    }
}

void TagSet::addTags(TagSet tags) {
    for (const Tag& tag : tags) {
        list.insert(tag);
    }
}


std::unordered_set<Tag> TagSet::getTags() const {
    return list;
}


const std::unordered_set<Tag>& TagSet::getTagsCRef() const {
    return list;
}


void TagSet::removeTag(Tag tag) {
    list.erase(list.find(tag));
}

TagSet TagSet::parseFromTagList(const std::vector<std::string> tags) {
    spt::TagSet set;

    for (const std::string& tag : tags) {
        set.addTag(spt::Tag::parseFromString(QString::fromStdString(tag)));
    }

    return set;
}


QStringList TagSet::toStringList() const {
    QStringList res;
    for (const Tag& tag : list) {
        res.push_back(tag.toString());
    }
    return res;
}

bool TagSet::isEmpty() const {
    return list.size() == 0;
}


/// \todo Abstract away notion of the List in API to enable more space-time
/// effective comparison and remove \f$O(n)\f$-time checks for unique tags
bool TagSet::operator==(const TagSet& other) const {
    bool res = std::all_of(
        list.begin(), list.end(), [&other](const Tag& tag) -> bool {
            return other.list.find(tag) != other.list.end();
        });

    return res;
}

size_t TagSet::size() const {
    return list.size();
}

std::unordered_set<Tag>::const_iterator TagSet::begin() const {
    return list.begin();
}


std::unordered_set<Tag>::const_iterator TagSet::end() const {
    return list.end();
}


QDomElement TagSet::toXML(
    QDomDocument& document,
    QString       listName,
    QString       tagName) const {

    QDomElement root = document.createElement(listName);
    for (const Tag& tag : list) {
        root.appendChild(tag.toXML(document, tagName));
    }

    return root;
}


TagSet TagSet::fromXML(
    QXmlStreamReader* xmlStream,
    QString           listName,
    QString           tagName) {
    TagSet res;


    if (xmlStream->name() == listName) {
        while (xmlStream->readNextStartElement()) {
            if (xmlStream->name() == tagName) {
                res.list.insert(Tag::fromXML(xmlStream, tagName));
            } else {
                xmlStream->skipCurrentElement();
            }
        }
    }

    return res;
}

std::string TagSet::toDebugString(size_t indentation, bool printEverything)
    const {

    if (list.size() == 0 && !printEverything) {
        return "";
    } else {
        QString padding(indentation, ' ');
        QString res = padding
                      + QString("spt::TagSet { %1 }\n").arg(list.size());
        // TODO why std string instead of Qstring as return type?

        for (const Tag& tag : list) {
            res += padding
                   + QString(" -- [ %1 ]\n").arg(tag.toDebugString());
        }

        return res.toStdString();
    }
}


} // namespace spt
