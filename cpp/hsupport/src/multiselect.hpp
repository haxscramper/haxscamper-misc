#pragma once
#include "selectoption.hpp"
#include <halgorithm/all.hpp>
#include <variant>
namespace spt {
/*!
 * \brief MultiSelect class represents list of selected strings together
 * with template (or pointer to template) of all possible items.
 *
 * This is a
 * convinience class made to wrap vector of strings + another vector of
 * strings in single class. Allows to select multiple items at once. For
 * select-only-one see \ref SingleSelect
 */
class MultiSelect
{
  public:
    MultiSelect();

    QDomElement toXML(QDomDocument& document) const {
        QDomElement root = document.createElement("multi-select-root");
        return root;
    }

    void fromXML(QXmlStreamReader* xmlStream) {
    }

  private:
    std::variant<SelectTemplate, SelectTemplate*> options;
    std::vector<SelectOption>                     selected;
};
} // namespace spt
