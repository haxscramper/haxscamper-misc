#pragma once

#include <QDomDocument>
#include <QDomElement>
#include <QFile>
#include <QTextStream>
#include <QXmlStreamReader>
#include <boost/variant.hpp>
#include <functional>
#include <map>


class QDomDocument;
class QXmlStreamReader;


namespace spt {

template <class List>
QDomElement list_to_xml(
    const List&   list,
    QDomDocument& document,
    // TODO Remove std::function to allow to use lambda
    std::function<QDomElement(const typename List::value_type&)> getter,
    QString                                                      listName,
    QString elementName) {
    QDomElement XMLList = document.createElement(listName);
    typedef typename List::value_type valueType;
    for (const valueType& item : list) {
        QDomElement XMLItem = document.createElement(elementName);
        XMLItem.appendChild(getter(item));
        XMLList.appendChild(XMLItem);
    }

    return XMLList;
}


template <class List>
QDomElement list_to_xml(
    const List&   list,
    QDomDocument& document,
    // TODO Remove std::function to allow to use lambda
    std::function<QString(const typename List::value_type&)> getter,
    QString                                                  listName,
    QString                                                  elementName) {
    QDomElement XMLList = document.createElement(listName);
    typedef typename List::value_type valueType;
    for (const valueType& item : list) {
        QDomElement XMLItem = document.createElement(elementName);
        XMLItem.appendChild(document.createTextNode(getter(item)));
        XMLList.appendChild(XMLItem);
    }

    return XMLList;
}


template <class List>
QDomElement list_to_xml(
    const List&   list,
    QDomDocument& document,
    // TODO Remove std::function to allow to use lambda
    std::function<QDomElement(const typename List::value_type&)> getter,
    QString listName) {
    QDomElement XMLList = document.createElement(listName);
    typedef typename List::value_type valueType;
    for (const valueType& item : list) {
        XMLList.appendChild(getter(item));
    }

    return XMLList;
}

template <class KeyType, class ValueType>
QDomElement map_to_xml(
    /// Map to get values from
    const std::map<KeyType, ValueType>& map,
    QDomDocument&                       document,
    /// Function to convert map pair into
    std::function<QDomElement(
        const typename std::map<KeyType, ValueType>::value_type&)> getter,

    /// Name of the XML element
    QString mapName) {
    QDomElement res = document.createElement(mapName);

    for (auto& pair : map) {
        res.appendChild(getter(pair));
    }

    return res;
}


template <class KeyType, class ValueType, class Callback>
void map_from_xml(
    std::map<KeyType, ValueType>& map,
    QXmlStreamReader*             xmlStream,
    Callback                      callback,
    QString                       mapName,
    QString                       pairName  = "pair",
    QString                       keyName   = "key",
    QString                       valueName = "value") {
    if (xmlStream->name() != mapName) {
        return;
    }

    const auto pairReader =
        [&pairName, &keyName, &valueName](
            QXmlStreamReader* xmlStream) -> std::pair<QString, QString> {
        std::pair<QString, QString> pair;
        while (xmlStream->readNextStartElement()) {
            if (xmlStream->name() == keyName) {
                pair.first = xmlStream->readElementText();
            } else if (xmlStream->name() == valueName) {
                pair.second = xmlStream->readElementText();
            } else {
                xmlStream->skipCurrentElement();
            }
        }
        return pair;
    };

    while (xmlStream->readNextStartElement()) {
        if (xmlStream->name() == pairName) {
            std::pair<QString, QString> pair = pairReader(xmlStream);
            callback(map, pair);
        } else {
            xmlStream->skipCurrentElement();
        }
    }
}


QDomElement listToXML(
    const QStringList& list,
    QDomDocument&      document,
    QString            listName,
    QString            elementName);

QDomElement listVariantToXML(
    boost::variant<QStringList, QStringList*> list,
    QDomDocument&                             document,
    QString                                   listName,
    QString                                   elementName);

QDomElement textElement(
    QDomDocument& document,
    QString       elementName,
    QString       elementText,
    bool          isCDATA = false);

QStringList listFromXML(
    QXmlStreamReader* xmlStream,
    QString           listName,
    QString           itemName = "tag");


void writeDocument(QDomDocument& document, QString path);
void writeDocument(QDomDocument& document, QFile& file);
void overrideFile(QDomDocument& document, QString path);

} // namespace spt
