#include "singleselect.hpp"


namespace spt {
SingleSelect::SingleSelect() {
}

/*!
 * \brief Get *copy* of all possible options. No difference
 * between self-contained and external (using pointer) list
 */
std::vector<SelectOption> SingleSelect::getOptions() const {
    if (options.index() == 0) {
        return std::get<0>(options).getOptionsRef();
    } else {
        return std::get<1>(options)->getOptionsRef();
    }
}

/*!
 * \brief Get reference to possible selection options. No difference
 * between self-contained and external (using pointer) list
 */
std::vector<SelectOption>& SingleSelect::getOptionsRef() {
    if (options.index() == 0) {
        return std::get<0>(options).getOptionsRef();
    } else {
        return std::get<1>(options)->getOptionsRef();
    }
}


/*!
 * \brief Get reference to possible selection options. No difference
 * between self-contained and external (using pointer) list
 */
const std::vector<SelectOption>& SingleSelect::getOptionsCRef() const {
    if (options.index() == 0) {
        return std::get<0>(options).getOptionsRef();
    } else {
        return std::get<1>(options)->getOptionsRef();
    }
}


/*!
 * \brief Copy all data int QDomElement. If objects does not contain all
 * selection options (i.e. all possible selection options are taken from
 * another list and supplied as pointer to this object) they are *not*
 * expoted.
 */
QDomElement SingleSelect::toXML(
    QDomDocument& document,
    QString       rootName,
    QString       optionListName) const {
    QDomElement root = document.createElement(rootName);
    root.setAttribute(
        "referenced", (options.index() == 1 ? "true" : "false"));


    QDomElement xmlOptions = document.createElement(optionListName);
    if (options.index() == 0) {
        for (const SelectOption& opt : getOptionsCRef()) {
            xmlOptions.appendChild(opt.toXML(document));
        }
    }

    QDomElement xmlSelected = document.createElement("selected");
    root.appendChild(xmlOptions);
    root.appendChild(xmlSelected);

    return root;
}
} // namespace spt
