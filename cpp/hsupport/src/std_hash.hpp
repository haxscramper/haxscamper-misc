#if defined(_GLIBCXX_UNORDERED_MAP) && defined(QUUID_H)
namespace std {
template <>
struct hash<QUuid> {
    std::size_t operator()(const QUuid& u) const {
        return qHash(u);
    }
};


} // namespace std
#endif
