#include "tag.hpp"
#include "misc.hpp"
#include "xml.hpp"
#include <hdebugmacro/all.hpp>

namespace spt {
QDomElement Tag::toXML(
    QDomDocument& document,
    QString       tagItemName,
    QString       subtagName) const {

    QDomElement root = document.createElement(tagItemName);

    if (subtags.size() == 1) {
        root.setAttribute(subtagName, subtags.front());
    } else {
        for (const QString& tag : subtags) {
            root.appendChild(spt::textElement(document, subtagName, tag));
        }
    }

    return root;
}

Tag Tag::fromXML(
    QXmlStreamReader*        xmlStream,
    [[maybe_unused]] QString tagItemName,
    QString                  subtagName) {
    Tag tag;

    if (xmlStream->name().isEmpty()) {
        xmlStream->readNext();
    }

    if (xmlStream->name() == tagItemName) {
        QXmlStreamAttributes attributes = xmlStream->attributes();
        if (attributes.hasAttribute(subtagName)) {
            tag.subtags.push_back(
                attributes.value("", subtagName).toString());
            xmlStream->skipCurrentElement();
        } else {
            while (xmlStream->readNextStartElement()) {
                if (xmlStream->name() == subtagName) {
                    QString val = xmlStream->readElementText();
                    tag.subtags.push_back(val);
                } else {
                    xmlStream->skipCurrentElement();
                }
            }
        }
    }

    return tag;
}


QString Tag::toDebugString() const {
    QString res = "#";

    for (size_t i = 0; i < subtags.size();) {
        res += subtags[i];
        if (++i < subtags.size()) {
            res += " ## ";
        }
    }

    return res;
}

static std::vector<QString> parseFromStringImpl(
    QString subtags,
    QString delimiter = "##") {
    std::vector<QString> res;


    for (QString& str : subtags.split(delimiter)) {
        if (str.startsWith("#")) {
            str = str.remove(0, 1);
        }

        res.push_back(str);
    }
    return res;
}

Tag Tag::parseFromString(std::string subtags, QString delimiter) {
    Tag res;
    res.subtags = parseFromStringImpl(
        QString::fromStdString(subtags), delimiter);
    return res;
}

Tag Tag::parseFromString(QString subtags, QString delimiter) {
    Tag res;
    res.subtags = parseFromStringImpl(subtags, delimiter);
    return res;
}

void Tag::setTopTag(
    [[maybe_unused]] QString tag,
    [[maybe_unused]] bool    discard_other) {
}


Tag::Tag(QString tag) {
    subtags = parseFromStringImpl(tag);
}

Tag::Tag(std::string tag) {
    subtags = parseFromStringImpl(QString::fromStdString(tag));
}

Tag::Tag(const char* tag) {
    subtags = parseFromStringImpl(QString::fromStdString(tag));
}

bool Tag::operator==(const Tag& other) const {
    bool res = std::equal(
        subtags.begin(), subtags.end(), other.subtags.begin());
    return res;
}


QString Tag::toString() const {
    QString res = "#";
    for (const QString& tag : subtags) {
        res += "##" + tag;
    }

    res.remove(0, 2);

    return res;
}

std::vector<QString> Tag::getSubtags() const {
    return subtags;
}

const std::vector<QString>* Tag::getSubtagsCPtr() const {
    return &subtags;
}


} // namespace spt
