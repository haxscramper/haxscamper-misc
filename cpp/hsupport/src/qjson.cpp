#include "qjson.hpp"


json jsonFromFile(QString path) {
    QFileInfo     fileInfo(path);
    std::ifstream in;
    json          _json;
    in.open(path.toStdString(), std::fstream::in);
    in >> _json;
    in.close();
    return _json;
}

void jsonToFile(const json& _json, QString path) {
    std::ofstream out;
    out.open(path.toStdString(), std::fstream::out);
    out << _json.dump(4);
    out.close();
}

std::optional<json> safeGetValue(json& _json, QString name) {
    std::optional<json> res;
    try {
        res = _json[name.toStdString()];
    } catch (...) {}

    return res;
}

void to_json(json& j, QString& str) {
    j = str.toStdString();
}

void from_json(const json& j, QString& str) {
    str = QString::fromStdString(j);
}
