#ifndef SUPPORT_ICONS_HPP
#define SUPPORT_ICONS_HPP

#include <QIcon>

namespace spt {
/*!
 * \brief Enumerations for Icon naming enums Version 0.8.90
 */
namespace icons {
    enum class Standard_Action_Icons
    {
        address_book_new, ///< The icon used for the action to create a new
                          ///< address book.
        application_exit, ///< The icon used for exiting an application.
                          ///< Typically this is seen in the application's
                          ///< menus as File->Quit.
        appointment_new,  ///< The icon used for the action to create a new
                          ///< appointment in a calendaring application.
        call_start,  ///< The icon used for initiating or accepting a call.
                     ///< Should be similar to the standard cellular call
                     ///< pickup icon, a green handset with ear and mouth
                     ///< pieces facing upward.
        call_stop,   ///< The icon used for stopping a current call. Should
                     ///< be similar to the standard cellular call hangup
                     ///< icon, a red handset with ear and mouth pieces
                     ///< facing downward.
        contact_new, ///< The icon used for the action to create a new
                     ///< contact in an address book application.
        document_new,  ///< The icon used for the action to create a new
                       ///< document.
        document_open, ///< The icon used for the action to open a
                       ///< document.
        document_open_recent, ///< The icon used for the action to open a
                              ///< document that was recently opened.
        document_page_setup,  ///< The icon for the page setup action of a
                              ///< document editor.
        document_print,       ///< The icon for the print action of an
                              ///< application.
        document_print_preview, ///< The icon for the print preview action
                                ///< of an application.
        document_properties,    ///< The icon for the action to view the
                                ///< properties of a document in an
                                ///< application.
        document_revert,  ///< The icon for the action of reverting to a
                          ///< previous version of a document.
        document_save,    ///< The icon for the save action. Should be an
                          ///< arrow pointing down and toward a hard disk.
        document_save_as, ///< The icon for the save as action.
        document_send,    ///< The icon for the send action. Should be an
                          ///< arrow pointing up and away from a hard disk.
        edit_clear,       ///< The icon for the clear action.
        edit_copy,        ///< The icon for the copy action.
        edit_cut,         ///< The icon for the cut action.
        edit_delete,      ///< The icon for the delete action.
        edit_find,        ///< The icon for the find action.
        edit_find_replace,  ///< The icon for the find and replace action.
        edit_paste,         ///< The icon for the paste action.
        edit_redo,          ///< The icon for the redo action.
        edit_select_all,    ///< The icon for the select all action.
        edit_undo,          ///< The icon for the undo action.
        folder_new,         ///< The icon for creating a new folder.
        format_indent_less, ///< The icon for the decrease indent
                            ///< formatting action.
        format_indent_more, ///< The icon for the increase indent
                            ///< formatting action.
        format_justify_center, ///< The icon for the center justification
                               ///< formatting action.
        format_justify_fill,   ///< The icon for the fill justification
                               ///< formatting action.
        format_justify_left,   ///< The icon for the left justification
                               ///< formatting action.
        format_justify_right,  ///< The icon for the right justification
                               ///< action.
        format_text_direction_ltr, ///< The icon for the left-to-right text
                                   ///< formatting action.
        format_text_direction_rtl, ///< The icon for the right-to-left
                                   ///< formatting action.
        format_text_bold,      ///< The icon for the bold text formatting
                               ///< action.
        format_text_italic,    ///< The icon for the italic text formatting
                               ///< action.
        format_text_underline, ///< The icon for the underlined text
                               ///< formatting action.
        format_text_strikethrough, ///< The icon for the strikethrough text
                                   ///< formatting action.
        go_bottom,   ///< The icon for the go to bottom of a list action.
        go_down,     ///< The icon for the go down in a list action.
        go_first,    ///< The icon for the go to the first item in a list
                     ///< action.
        go_home,     ///< The icon for the go to home location action.
        go_jump,     ///< The icon for the jump to action.
        go_last,     ///< The icon for the go to the last item in a list
                     ///< action.
        go_next,     ///< The icon for the go to the next item in a list
                     ///< action.
        go_previous, ///< The icon for the go to the previous item in a
                     ///< list action.
        go_top,      ///< The icon for the go to the top of a list action.
        go_up,       ///< The icon for the go up in a list action.
        help_about,  ///< The icon for the About item in the Help menu.
        help_contents, ///< The icon for Contents item in the Help menu.
        help_faq,      ///< The icon for the FAQ item in the Help menu.
        insert_image,  ///< The icon for the insert image action of an
                       ///< application.
        insert_link,   ///< The icon for the insert link action of an
                       ///< application.
        insert_object, ///< The icon for the insert object action of an
                       ///< application.
        insert_text,   ///< The icon for the insert text action of an
                       ///< application.
        list_add,      ///< The icon for the add to list action.
        list_remove,   ///< The icon for the remove from list action.
        mail_forward, ///< The icon for the forward action of an electronic
                      ///< mail application.
        mail_mark_important, ///< The icon for the mark as important action
                             ///< of an electronic mail application.
        mail_mark_junk,    ///< The icon for the mark as junk action of an
                           ///< electronic mail application.
        mail_mark_notjunk, ///< The icon for the mark as not junk action of
                           ///< an electronic mail application.
        mail_mark_read,    ///< The icon for the mark as read action of an
                           ///< electronic mail application.
        mail_mark_unread, ///< The icon for the mark as unread action of an
                          ///< electronic mail application.
        mail_message_new, ///< The icon for the compose new mail action of
                          ///< an electronic mail application.
        mail_reply_all,   ///< The icon for the reply to all action of an
                          ///< electronic mail application.
        mail_reply_sender, ///< The icon for the reply to sender action of
                           ///< an electronic mail application.
        mail_send, ///< The icon for the send action of an electronic mail
                   ///< application.
        mail_send_receive, ///< The icon for the send and receive action of
                           ///< an electronic mail application.
        media_eject, ///< The icon for the eject action of a media player
                     ///< or file manager.
        media_playback_pause, ///< The icon for the pause action of a media
                              ///< player.
        media_playback_start, ///< The icon for the start playback action
                              ///< of a media player.
        media_playback_stop,  ///< The icon for the stop action of a media
                              ///< player.
        media_record,        ///< The icon for the record action of a media
                             ///< application.
        media_seek_backward, ///< The icon for the seek backward action of
                             ///< a media player.
        media_seek_forward,  ///< The icon for the seek forward action of a
                             ///< media player.
        media_skip_backward, ///< The icon for the skip backward action of
                             ///< a media player.
        media_skip_forward,  ///< The icon for the skip forward action of a
                             ///< media player.
        object_flip_horizontal, ///< The icon for the action to flip an
                                ///< object horizontally.
        object_flip_vertical,   ///< The icon for the action to flip an
                                ///< object vertically.
        object_rotate_left,     ///< The icon for the rotate left action
                                ///< performed on an object.
        object_rotate_right,    ///< The icon for the rotate rigt action
                                ///< performed on an object.
        process_stop, ///< The icon used for the “Stop” action in
                      ///< applications with actions that may take a while
                      ///< to process, such as web page loading in a
                      ///< browser.
        system_lock_screen, ///< The icon used for the “Lock Screen” item
                            ///< in the desktop's panel application.
        system_log_out, ///< The icon used for the “Log Out” item in the
                        ///< desktop's panel application.
        system_run, ///< The icon used for the “Run Application...” item in
                    ///< the desktop's panel application.
        system_search, ///< The icon used for the “Search” item in the
                       ///< desktop's panel application.
        system_reboot, ///< The icon used for the “Reboot” item in the
                       ///< desktop's panel application.
        system_shutdown, ///< The icon used for the “Shutdown” item in the
                         ///< desktop's panel application.
        tools_check_spelling, ///< The icon used for the “Check Spelling”
                              ///< item in the application's “Tools” menu.
        view_fullscreen, ///< The icon used for the “Fullscreen” item in
                         ///< the application's “View” menu.
        view_refresh, ///< The icon used for the “Refresh” item in the
                      ///< application's “View” menu.
        view_restore, ///< The icon used by an application for leaving the
                      ///< fullscreen view, and returning to a normal
                      ///< windowed view.
        view_sort_ascending, ///< The icon used for the “Sort Ascending”
                             ///< item in the application's “View” menu, or
                             ///< in a button for changing the sort method
                             ///< for a list.
        view_sort_descending, ///< The icon used for the “Sort Descending”
                              ///< item in the application's “View” menu,
                              ///< or in a button for changing the sort
                              ///< method for a list.
        window_close, ///< The icon used for the “Close Window” item in the
                      ///< application's “Windows” menu.
        window_new, ///< The icon used for the “New Window” item in the
                    ///< application's “Windows” menu.
        zoom_fit_best, ///< The icon used for the “Best Fit” item in the
                       ///< application's “View” menu.
        zoom_in,       ///< The icon used for the “Zoom in” item in the
                       ///< application's “View” menu.
        zoom_original, ///< The icon used for the “Original Size” item in
                       ///< the application's “View” menu.
        zoom_out, ///< The icon used for the “Zoom Out” item in the
                  ///< application's “View” menu.

    };

} // namespace icons


QIcon toIcon(icons::Standard_Action_Icons icon);
} // namespace spt

#endif // SUPPORT_ICONS_HPP
