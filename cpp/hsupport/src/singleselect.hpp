#ifndef SINGLESELECT_HPP
#define SINGLESELECT_HPP


//===    Qt    ===//
#include <QColor>
#include <QDomDocument>
#include <QDomElement>
#include <QString>
#include <QXmlStreamReader>


//===    STL   ===//
#include <set>
#include <variant>
#include <vector>


#include "selectoption.hpp"


namespace spt {
/*!
 * \brief SingleSelect class represents one selected strings together
 * with template (or pointer to template) of all possible items.
 *
 * This is a
 * convinience class made to wrap string + another vector of
 * strings in single class. Allows to select only one item at once. For
 * multiple selections \ref MultiSelect
 */
class SingleSelect
{
  public:
    SingleSelect();
    std::vector<SelectOption>        getOptions() const;
    std::vector<SelectOption>&       getOptionsRef();
    const std::vector<SelectOption>& getOptionsCRef() const;

    QDomElement toXML(
        QDomDocument& document,
        QString       rootName       = "single-select-root",
        QString       optionListName = "options") const;

    std::pair<uint, QString> selected;

  private:
    std::variant<SelectTemplate, SelectTemplate*> options;
};
} // namespace spt


#endif // SINGLESELECT_HPP
