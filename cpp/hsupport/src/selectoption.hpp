#pragma once

//===    Qt    ===//
#include <QColor>
#include <QDomDocument>
#include <QDomElement>
#include <QString>
#include <QXmlStreamReader>

//=== STL ===//
#include <variant>

//=== Sibling  ===//
#include "misc.hpp"
#include "xml.hpp"

namespace spt {

struct SelectOption {
    QString     name;
    QColor      color;
    QDomElement toXML(QDomDocument& document) const {
        QDomElement root = document.createElement("select-option");
        root.appendChild(spt::textElement(document, "name", name));
        QDomElement xmlColor = document.createElement("color");
        {
            int red;
            int green;
            int blue;
            int alpha;
            color.toRgb().getRgb(&red, &green, &blue, &alpha);
            xmlColor.setAttribute("red", red);
            xmlColor.setAttribute("green", green);
            xmlColor.setAttribute("blue", blue);
            xmlColor.setAttribute("alpha", alpha);
        }
        root.appendChild(xmlColor);
        return root;
    }

    void fromXML(QXmlStreamReader* xmlStream) {
        if (xmlStream->name() != "select-option") {
            return;
        }

        while (xmlStream->readNextStartElement()) {
            if (xmlStream->name() == "name") {
                name = xmlStream->readElementText();
            } else if (xmlStream->name() == "color") {
                for (QXmlStreamAttribute& colorValue :
                     xmlStream->attributes()) {
                    if (colorValue.name() == "red") {
                        color.setRed(colorValue.value().toInt());
                    } else if (colorValue.name() == "green") {
                        color.setGreen(colorValue.value().toInt());
                    } else if (colorValue.name() == "blue") {
                        color.setBlue(colorValue.value().toInt());
                    } else if (colorValue.name() == "alpha") {
                        color.setAlpha(colorValue.value().toInt());
                    }
                }
            }
        }
    }
};

class SelectTemplate
{
  public:
    std::vector<SelectOption>& getOptionsRef() {
        return options;
    }

    const std::vector<SelectOption>& getOptionsRef() const {
        return options;
    }

  private:
    std::vector<SelectOption> options;
};
} // namespace spt
