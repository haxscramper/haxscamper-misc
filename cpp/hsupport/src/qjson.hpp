#ifndef QJSON_HPP
#define QJSON_HPP

#include "nlohmann_json.hpp"

#include <QFileInfo>
#include <QString>
#include <QVariant>
#include <fstream>
#include <ios>
#include <optional>

using json = nlohmann::json;


void jsonToFile(const json& _json, QString path);
json jsonFromFile(QString path);

std::optional<json> safeGetValue(json& _json, QString name);

void to_json(json& j, QString& str);
void from_json(const json& j, QString& str);

Q_DECLARE_METATYPE(json)

#endif
