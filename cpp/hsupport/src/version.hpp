#pragma once
#include <QDomElement>
// TODO I only include this because function returns value of this
// type. Is it possible to somehow avoid including this (probably)
// very long header file only for function return value?
#include <QString>

class QXmlStreamReader;

namespace spt {
struct Version {
    Version(
        unsigned int _majorVersion = 1,
        unsigned int _minorVersion = 0,
        unsigned int _patchVersion = 0,
        QString      _description  = "")
        : majorVersion(_majorVersion)
        , minorVersion(_minorVersion)
        , patchVersion(_patchVersion)
        , description(_description) {
    }

    unsigned int majorVersion;
    unsigned int minorVersion;
    unsigned int patchVersion;
    QString      description;

    bool operator==(const Version& other) const {
        return majorVersion == other.majorVersion
               && minorVersion == other.minorVersion
               && patchVersion == other.patchVersion;
    }

    bool isDefault() const {
        return majorVersion == 1 && minorVersion == 0 && patchVersion == 0
               && description == "";
    }

    std::string toDebugString() const {
        return std::to_string(majorVersion) + " "
               + std::to_string(minorVersion) + " "
               + std::to_string(patchVersion);
    }

    QDomElement toXML(QDomDocument& document, QString version_name) const;
    void        readXML(QXmlStreamReader* xmlStream);
};

} // namespace spt
