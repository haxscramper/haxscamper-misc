#include <stddef.h>

using u32 = unsigned int;

#ifdef _GLIBCXX_VECTOR

template <class T>
using Vec = std::vector<T>;

#endif

#ifdef _GLIBCXX_UNORDERED_MAP

template <class K, class V>
using Hash = std::unordered_map<K, V>;

#endif

#ifdef _GLIBCXX_OPTIONAL

template <class T>
using Opt = std::optional<T>;

#endif

#ifdef _GLIBCXX_FUNCTIONAL

template <typename... Args>
using Func = std::function<Args...>;

#endif

#ifdef _GLIBCXX_STRING

using Str = std::string;

#endif

#ifdef _GLIBCXX_MEMORY

template <class T>
using Uptr = std::unique_ptr<T>;

#endif

namespace spt {

#ifdef _GLIBCXX_MEMORY

template <class T>
using uptr = std::unique_ptr<T>;


template <class T>
uptr<T> mk_up() {
    return std::make_unique<T>();
}


#endif


#if defined(_GLIBCXX_VARIANT) && defined(_GLIBCXX_MEMORY)

template <class T>
using ptr_or_uptr = std::variant<T*, std::unique_ptr<T>>;


template <class T>
T* get_ptr(ptr_or_uptr<T>& val) {
    if (val.index() == 0) {
        return std::get<T*>(val);
    } else {
        return std::get<uptr<T>>(val).get();
    }
}

template <class T>
bool is_uptr(ptr_or_uptr<T>& ptr) {
    return ptr.index() == 1;
}

#endif

#if defined(_GLIBCXX_OPTIONAL) && defined(_GLIBCXX_MEMORY)

template <class T>
using opt_uptr = std::optional<std::unique_ptr<T>>;

#endif


} // namespace spt
