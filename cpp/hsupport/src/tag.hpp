#pragma once
#include <QDomElement>
#include <QString>
#include <vector>

class QXmlStreamReader;

namespace spt {
/// Tags class represents single tags with all it's parent tags.
class Tag
{
    /// List of nested tags, each item represents one level of nesting
    std::vector<QString> subtags;

  public:
    Tag() = default;
    Tag(QString tag);     ///< implicit conversion constructor
    Tag(std::string tag); ///< implicit conversion constructor
    Tag(const char* tag); ///< implicit conversion constructor

    /// Set first of one of the subtags
    void setTopTag(QString tag, bool discard_other = true);
    /// Parse list of subtags from the string separated by `delimiter`;
    static Tag parseFromString(QString subtags, QString delimiter = "##");
    /// Parse list of subtags from the string separated by `delimiter`;
    static Tag parseFromString(
        std::string subtags,
        QString     delimiter = "##");
    /// Return copy of the subtags
    std::vector<QString> getSubtags() const;
    /// Return constant pointer to the subtags list
    const std::vector<QString>* getSubtagsCPtr() const;
    /// Create debug string
    QString toDebugString() const;
    QString toString() const;

    bool operator==(const Tag& other) const;

    QDomElement toXML(
        QDomDocument& document,
        QString       tagItemName = "tagitem",
        QString       subtagName  = "st") const;

    static Tag fromXML(
        QXmlStreamReader* xmlStream,
        QString           tagItemName = "tagitem",
        QString           subtagName  = "st");
};
} // namespace spt
