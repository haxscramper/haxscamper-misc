#ifndef SUPPORT_H
#define SUPPORT_H


#include <algorithm>
// TODO remove
#include <boost/math/constants/constants.hpp>
// TODO remove
#include <boost/variant.hpp>
#include <functional>
#include <iostream>

#include <QDateTime>
#include <QDomElement>
#include <QMargins>
#include <QSize>
#include <QString>
#include <QXmlStreamReader>


class QXmlStreamReader;
class QPixmap;
class QFile;

/// Helper functions and classes
namespace spt {

class ExtensionTypeMapping;

bool isImageExtension(
    QString               extension,
    ExtensionTypeMapping* mapping = nullptr);

bool isTextExtension(
    QString               extension,
    ExtensionTypeMapping* mapping = nullptr);


[[noreturn]] void throw_file_does_not_exist(QString& path);
[[noreturn]] void throw_file_cannot_be_opened(QString& path);

#define unless_nullptr(var_name, get_from)                                \
    auto var_name = get_from;                                             \
    if (var_name != nullptr)


#define throw_inv_arg_if(...)                                             \
    if (__VA_ARGS__) {                                                    \
        throw std::invalid_argument(                                      \
            std::string(__PRETTY_FUNCTION__) + " fail: " + #__VA_ARGS__); \
    }


void saveToFile(
    QPixmap& pixmap,
    QString  absoluteFileName,
    QString  ext     = "png",
    int      quality = -1);

QString     extensionsToFilter(QStringList extensionList);
int         minDim(const QSize& size);
QMargins    zeroMargins();
QMargins    equalMargins(int size);
QString     getRandomBase64(int length = 10);
QString     getRandomBase32(int length = 10);
QStringList getRandomBase64List(int listLength = 10, int itemSize = 20);
QString     getRadomString(QString alphabet, int length);
QString     getNewScratchFile(
        int     length = 20,
        QString prefix = "scratch___",
        QString suffix = "");
QSize scale(QSize size, qreal ratio);


bool removeDir(QString absoltePath);
bool removeDirContent(QString absolutePath);
bool createFilePath(QString absoluteTargetPath);


template <class Enum>
QString enum_to_string(Enum _enum);

template <class Enum>
Enum enum_from_string(QString string);

// TODO Should be deprecated in favor of struct/class
typedef QString SimpleNote;

} // namespace spt

// FIXME do we need this?
struct SptErrorCodeCategory : std::error_category {
    const char* name() const noexcept override;
    std::string message(int ev) const override;
};

// FIXME Remove dependency on boost
const double pi = boost::math::constants::pi<double>();

double rad(double deg);

#endif // SUPPORT_H
