from PyPreprocessor.parser_main import build_file
import os

build_file("source/build_headers/qiulsp_full.tex"
           , build_dir="build/full"
           , target_file="include/qiulsp/full.sty"
           , docs_dir="docs")

build_file("source/build_headers/qiulsp_macro_only.tex"
           , build_dir="build/macro_only"
           , target_file="include/qiulsp/macro_only.sty"
           , docs_dir="docs")

