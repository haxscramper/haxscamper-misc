import string

trig_func = ["sin", "sinh", "arcsin", "asin", "cos", "cosh",  \
             "arccos", "acos", "tan", "tanh", "arctan", "atan", \
             "csc", "csch", "arccsc", "acsc", "sec", "sech",  \
             "arcsec", "asec", "cot", "coth", "arccot", "acot"]

