import string

def get_style_command(style: str, lowercase:bool) -> str:
    style_command = ""

    if style == "fr":
        style_command = "mathfrak"

    elif style == "cl":
        style_command = "mathcal"

    elif style == "bb":
        if lowercase:
            style_command = "mathbbl"
        else:
            style_command = "mathbb"

    elif style == "tt":
        style_command = "operatorname"

    return style_command

def get_latex_command(style:str, letter:str, lowercase:bool) -> str:
    result =                                                      \
        "\\newcommand {{ \\{}{} }}".format(style,letter) +        \
        " {{ \\{}".format(get_style_command(style, lowercase)) +  \
        "{{ {} }} }}".format(letter)

    return result

def add_command(command:str) -> None:
    preprocessor.append_text(command + "\n")


for style in ["fr", "cl", "bb", "tt"]:
    for letter in string.ascii_uppercase:
        if (style == "fr" and letter == "G") \
        or (style == "bb" and letter == "G") \
        or (style == "tt" and letter == "G") \
        or (style == "cl" and letter == "G") :
            continue

        add_command(get_latex_command(style, letter, False))

    for letter in string.ascii_lowercase:
        if (style == "fr" and letter == "g") \
        or (style == "fr" and letter == "q") \
        or (style == "cl" and letter == "g") \
        or (style == "bb" and letter == "g") \
        or (style == "tt" and letter == "g"):
            continue

        add_command(get_latex_command(style, letter, True))


