#!/usr/bin/env bash
ts=$(date +%s%N) ; $@ ; tt=$((($(date +%s%N) - $ts)/1000)) ; echo "Time taken: $tt microseconds"
