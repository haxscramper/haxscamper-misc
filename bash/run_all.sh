#!/usr/bin/env bash

cd "$(dirname "$0")"

../setup.sh
./ensure_dir_structure.sh
./mega-cmd_configure.sh
