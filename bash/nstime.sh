#!/usr/bin/env bash
ts=$(date +%s%N) ; $@ ; tt=$((($(date +%s%N) - $ts))) ; echo "Time taken: $tt microseconds"
