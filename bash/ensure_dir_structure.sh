#!/usr/bin/env bash
cd $HOME
colecho -i0 "Updating directory structure"
mkdir -p qdelib code defaultdirs shared

cd defaultdirs

mkdir -p input automatic transient images music documents templates \
      videos public desktop autosync

colecho "Created defaultdirs"

xdg-user-dirs-update --set DESKTOP $HOME/defaultdirs/desktop
xdg-user-dirs-update --set DOCUMENTS $HOME/defaultdirs/documents
xdg-user-dirs-update --set DOWNLOAD $HOME/defaultdirs/input
xdg-user-dirs-update --set MUSIC $HOME/defaultdirs/music
xdg-user-dirs-update --set PICTURES $HOME/defaultdirs/images
xdg-user-dirs-update --set PUBLICSHARE $HOME/defaultdirs/public
xdg-user-dirs-update --set TEMPLATES $HOME/defaultdirs/templates
xdg-user-dirs-update --set VIDEOS $HOME/defaultdirs/videos

cd autosync
mkdir -p pdf code archives documents old images videos
cd ..

colecho "Updated XDG dirs"

mkdir -p automatic

colecho "Created automatic dir"

cd $HOME/shared

mkdir -p pdf personal input output code transient images

colecho "Created autosync directory"

move () {
    mkdir -p "${@:$#}"
    movement="${@:1:$(($#-1))} ${@:$#}"
    mv -v --backup=t -- $movement 2>/dev/null | sed 's/(.*)/    \1/'
}

remove_standard_defaultdirs () {
    move ~/Downloads ~/defaultdirs/input
    move ~/Videos ~/defaultdirs/videos
    move ~/Pictures ~/defaultdirs/images
}

while getopts "C" opt; do
    case "$opt" in
        C)
            colecho -w "Moving content from old defaultdirs"
            # remove_standard_defaultdirs
            ;;
        *)
            colecho -E "Invalid flat $opt"
            exit 1
            ;;
    esac
done


colecho -i3  "Done updating directory structure"
