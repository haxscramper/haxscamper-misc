#!/usr/bin/env bash
colecho -i1 "Updating mega-cmd exclude rules"
mega-exclude -a "*.auctex" "*.out" "*.fls" "*.synctex.gz" \
             "*.el" ".git/**" "*.toc" "*.tmp.*/**" "*.tmp" "*.tmp/**" \
             "*.log" "*.aux" "*.fdb_latexmk" > /dev/null

mega-exclude -d ".*" > /dev/null

mega-exclude --restart-syncs |
    sed 's/\(.*\)/    \1/' |
    sed "s/names:/patters:/"

colecho -i3 "Updated mega-cmd exclude rules"
