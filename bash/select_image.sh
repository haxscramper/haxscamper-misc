#!/usr/bin/env bash
cd "$(dirname "$0")"

config_dir=$(realpath ../..)
target_dir=$HOME/$(
    rofi -dmenu < $config_dir/lists/image_directories |
        awk '{print $2; }')

# TODO Launch as floating window, centered, occupies part of the screen
fd . -e png -e jpg "$target_dir" |
    sxiv -tio -N "request_floating_SXIV" |
    xargs xclip -sel cli -t image/png -i
