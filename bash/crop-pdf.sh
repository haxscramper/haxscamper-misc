#!/usr/bin/env bash
# -*- coding: utf-8 -*-
# bash
set -o nounset
set -o errexit

file=$1
file_out=${2:-"cropped_$file"}

echo "Input file $file"
echo "Output file $file_out"

pdfcrop $file "tmp_$file_out"
pdftk "$file" dump_data output metadata.txt
grep -A3 BookmarkBegin > outline.txt < metadata.txt
pdftk "tmp_$file_out" update_info outline.txt output $file_out
rm metadata.txt outline.txt "tmp_$file_out"
