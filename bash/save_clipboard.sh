#!/usr/bin/env bash
# -*- coding: utf-8 -*-
# bash
cd "$(dirname "$0")"
set -o nounset
set -o errexit

__download_dir="~/defaultdirs/input/cliboard_images/"
mkdir -p $__download_dir

path=$(ls $__download_dir | rofi -dmenu -p "Enter Text > ")

xclip -sel cli -t image/png -o > "$__download_dir/$path.png"

