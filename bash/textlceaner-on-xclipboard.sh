#!/usr/bin/env bash
cd "$(dirname "$0")"
set -o nounset
set -o errexit

colecho -i1 "Outputting image to file"
xclip -sel cli -t image/png -o > selection.png
colecho -i1 "Converting image"

textcleaner   -g -e stretch -f 15 -o 5 -s 1  selection.png selection_cleaned.png
xclip -sel cli -t image/png -i selection_cleaned.png
rm selection.png
rm selection_cleaned.png

colecho -i3 "Done converting image"
