#!/usr/bin/env bash


if [[ "$1" = "-h" ]]; then
cat << EOF | groff -man -Tascii
.TH create-script.sh 1 'March 8, 2019' 'create-script.sh version 1'
.SH NAME
create-script.sh
.SH SYNOPSIS
create-script.sh [-h] file_name file_extension
.SH DESCRIPTION
Create script file and make it executable.
Add shebang (determined by the file_extension)
.SS Options
-h
Print this message and exit
EOF
exit 1
fi

msg="colecho -v"

if [[ $# -ne 2 ]]; then
    $msg -e1 "Invalid number of arguments. Expected 2 got $#."
    $msg -i2 "Specify name of the file and extension separated by space"
    exit 1
fi

file_name=$1.$2
case "$2" in
    sh)
        cat << EOF > $file_name
#!/usr/bin/env bash
# -*- coding: utf-8 -*-
# bash
cd "\$(dirname "\$0")"
set -o nounset
set -o errexit

EOF
        $msg "Created bash shell script"
        $msg -w1 "cd to file path is enabled"
        ;;
    elv)
        cat << EOF > $file_name
#!/usr/bin/env elvish
# -*- coding: utf-8 -*-
# elvish

EOF
        $msg "Created elvish shell script"
        ;;
    sh++)
        file_name="$1.sh"
        cat << EOF > $file_name
#!/usr/bin/env bash
# -*- coding: utf-8 -*-
# bash

if [ -z "${BASH_VERSINFO}" ] || [ -z "${BASH_VERSINFO[0]}" ] || [ ${BASH_VERSINFO[0]} -lt 4 ]; then
   colecho -e1 "This script requires Bash version >= 4"
   exit 1
fi

cd "\$(dirname "\$0")"
set -o nounset
set -o errexit
# Prevent overwriting of files by redirection (may be overridden by >|)
set -o noclobber
# Enable exteneded glob https://mywiki.wooledge.org/glob#extglob
shopt -s extglob
# globstar recursively repeats a pattern containing '**'.
shopt -s globstar

EOF
        $msg "Created enhanced bash shell script"
        $msg -w1 "cd to file path is enabled"
        ;;
    nim)
        cat << EOF > $file_name
# -*- coding: utf-8 -*-
# nim

import strformat
import sequtils
import strutils
import osproc
import os

EOF
        $msg "Created enhanced bash shell script"
        ;;
    pl)
        cat << EOF > $file_name
#!/usr/bin/env perl
# -*- coding: utf-8 -*-
# perl

use warnings;
use strict;

# Command line argument parsing
use Pod::Usage;
use Getopt::Long;

# Allow to use true and false as named constants
use constant false => 0;
use constant true  => 1;

# Allow to use functions with parameter signatures
use v5.20;
use feature qw(signatures);
no warnings qw(experimental::signatures);

sub log1($message) {
    system("colecho -- '$message'");
}

sub err1($message) {
    system("colecho -e2 -- '$message'");
}

sub info1($message) {
    system("colecho -i1 -- '$message'");
}

sub warn1($message) {
    system("colecho -w1 -- '$message'");
}

EOF
        $msg "Created perl script"
        ;;
    py)
        cat << EOF > $file_name
#!/usr/bin/env python
# -*- coding: utf-8 -*-
# python

EOF
        $msg "Created python script file"
        ;;
    r)
        cat << EOF > $file_name
#!/usr/bin/env Rscript

EOF
        $msg "Created R script file"
        ;;
    lisp)
        cat << EOF > $file_name
#!/usr/bin/env -S sbcl --script
(load "~/.sbclrc")

EOF
        $msg "Created sbcl common lisp script file"
        ;;
    el)
        cat << EOF > $file_name
#!/usr/bin/env -S emacs --script

EOF
        $msg "Created emacs lisp script file"
        ;;
    rkt)
        cat << EOF > $file_name
#!/usr/bin/racket
#lang racket/base
EOF
        $msg "Created racket script"
        ;;
    *)
        $msg -w2 "Cannot create script file. Unknown extension"
        exit 1
esac


chmod +x $file_name
$msg -i1 "Created  $file_name"
