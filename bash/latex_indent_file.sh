#!/usr/bin/env bash
# -*- coding: utf-8 -*-
# bash
set -o nounset
set -o errexit

colecho -i3 -- "Indentation on file $1"
latexindent --local="~/.config/hax-config/latexindent.yaml" --silent --overwrite "$1"
rm indent.log
rm *.bak*
