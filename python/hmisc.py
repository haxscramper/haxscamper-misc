from IPython.display import display, SVG, Image
import re
from fractions import Fraction
import pandas as pd
import numpy as np
import sympy as sp
#import lcapy as lc
from math import *
import cmath as cm
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt

import itertools

# TODO determine rounding for each value separately to produce best results
def round_expr_floats(expression, round_digits = 3):
    """Round all float literals to n decimal digits `1.21234x -> 1.212x`"""
    ex2 = expression
    for a in sp.preorder_traversal(expression):
        if isinstance(a, sp.Float):
            ex2 = ex2.subs(a, round(a, round_digits))

    return ex2

def print_sols_rounded(solution, round_digits = 3, as_latex= False):
    if as_latex:
        print("\\begin{aligned}")

    eqn_equal = ":"
    eqn_end = ""
    if as_latex: eqn_equal = " & ="
    if as_latex: eqn_end = "\\\\"

    for variable in solution:
        sol = round_expr_floats(solution[variable], round_digits)
        if isinstance(sol, sp.Float):
            print(f"{variable}{eqn_equal} {sol:.{round_digits}f} {eqn_end}")
        else:
            print(f"{variable}{eqn_equal}", sol, f"{eqn_end}")

    if as_latex: print("\\end{aligned}\\\\")

def subst_netlist_vals(netlist_string, val_dict):
    """Substitute values of components for netlist from value dictionary"""
    netlist_w_vals = ""
    str_vals = {}
    for k,v in val_dict.items():
        str_vals[str(k) + ""] = v

    for line in netlist_string.split("\n"):
        words = line.split(" ")
        result = []
        for word in words:
            if word.endswith(";") and words[0] in str_vals:
                word = word[0:-1] + " " + str(str_vals[words[0]]) + ";"

            result.append(word)

        netlist_w_vals += " ".join(result) + "\n"

    return netlist_w_vals

def showcircuit(circuit, name):
    """convert circuit image to png and show in full size"""
    circuit.draw("/tmp/" + name + ".png", style="american")
    display(Image("/tmp/" + name + ".png"))

def memoize(f):
    """Decorator for output memoization"""
    memo = {}
    def helper(x):
        if x not in memo:
            memo[x] = f(x)
        return memo[x]
    return helper


def sym(arg):
    return sp.symbols(arg)

def subst_dataframe(dataframe, expression, series_name, as_series = True):
    """subsitute values of each row of dataframe into symbolic expression"""

    symbolic = {sym(name): name for name in dataframe.columns}
    values = []
    for name, row in dataframe.iterrows():
        val_dict = {
            var_sym: row[var_name] for var_sym, var_name in symbolic.items()
        }

        values.append(expression.subs(val_dict))

    if as_series:
        return pd.Series(values, name=series_name)
    else:
        return values

def chain_subst_df(expression, series_name, dataframes,
                   as_series = True):

    expressions = [expression]

    for frame in dataframes:
        substituted = []
        for expr in expressions:
            substituted.append(subst_dataframe(frame, expr, series_name, as_series=False))

        expressions = list(itertools.chain(*substituted))

    if as_series:
        return pd.Series(expressions, name=series_name)
    else:
        return expressions


def smart_round(num, digits = 2):
    """
    Leave only `digit` significant digits in and convert number to string.
    This means it has to be used as last item in number cleanup.  Not useful
    when you want to use it somewhere else.

    :return: rounded number
    :rtype: float
    """
    res = 0

    if abs(num) == 0:
        return "0"

    if abs(num) <= 1:
        left_shift = 0
        while abs(num) < (10 ** digits) - 1:
            num *= 10
            left_shift += 1

        num /= 10
        res = round(num, 0) * pow(10, -left_shift + 1)
    else:
        right_shift = 0
        while abs(num) > 1:
            num /= 10
            right_shift += 1

        res = round(num, 2) * pow(10, right_shift)

    res = re.sub(r"0+\d$", "", str(res))
    return float(res)


def clean(num):
    """
    Remove unnecessary precision from number.  Leave only two meaningful digits
    and thow avay everything else.  So `1.123124122` will be converted to `1.1`
    which is much easier to read.  Complex numbers are also supported.
    """
    if isinstance(num, complex):
        if (num.real != 0) and (abs(num.imag / num.real) < (1 / (10 ** 6))):
            return smart_round(num.real)
        else:
            return complex(smart_round(num.real), smart_round(num.imag))
    else:
        return smart_round(num)

def e_d(deg):
    """
    Return $e^{j\\cdot deg}$ where `deg` is angle in radians
    """
    return cm.exp(complex(0, radians(deg)))

def magnitude(num):
    """Return magnitude of the complex number"""
    return sqrt(num.real ** 2 + num.imag ** 2)

def showval(val, l = None):
    """
    Pretty-print argument value

    #TODO add support for printint sympy expressions, equations, pandas
    dataframe etc.  Make it single entry point for pretty-printing of different
    values
    """
    # TODO print pandas dataframe

    if l:
        # l = f"{l:<5}: "
        pass
    else:
        l = ""

    if isinstance(val, complex):
        mod = magnitude(val)
        arg = cm.phase(val)

        print(f":{l}{clean(val):^12} |z| = {clean(mod):<6} arg(z) = {clean(arg)}")

    elif isinstance(val, float) or isinstance(val, int):
        print(f"{l}{clean(val):^12}")
    else:
        print(val)

def tryclean(val):
    """
    Try to clean argument and if exception is thrown return it as is
    """
    try:
        return msc.clean(val)
    except Exception as e:
        return val


def test_clean(num):
    print(clean(num))



def contour_2d(x_range, y_range, func,
               cmap = 'Spectral',
               figsize = (16, 12),
               ctype = 'filled',
               rotate_axis = False
              ):
    """
    Draw 2d contour plot of two-argument function

    :param x_range: range of x arguments for function
    :param y_range: range of y arguments for function
    :param func: function to apply to values
    :param cmap: style of drawn contour map.  Passed as `cmap` argument to
        `pyplot.contour`, see more in matplotlib documentation
    :param figsize: size of the plot figure
    :param ctype: contour type of the plot, can be either `'filled'` or
        `'line'`.  Based on value either `contourf` or `contour` will be
        called.
    :param rotate_axis: transpose `x_range` and `y_range` when evaluating
        function
    """
    w1_val = None
    if rotate_axis:
        w1_val = np.array([[func(x, y) for y in y_range] for x in x_range])
    else:
        w1_val = np.array([[func(x, y) for x in x_range] for y in y_range])


    if figsize:
        plt.figure(figsize=figsize)


    if rotate_axis:
        x_range, y_range = y_range, x_range

    if ctype == 'filled':
        plt.contourf(x_range, y_range, w1_val, 20, cmap = cmap)
    elif ctype == 'line':
        plt.contour(x_range, y_range, w1_val, 20, cmap = cmap)


def surface_3d(x_range, y_range, func,
               figsize = (16, 12)):
    """
    Draw 3d surface
    """
    fig = plt.figure(figsize=figsize)

    ax = fig.add_subplot(111, projection='3d')
    X, Y = np.meshgrid(x_range, y_range)
    fun = np.vectorize(func)
    zs = np.array(fun(np.ravel(X), np.ravel(Y)))
    Z = zs.reshape(X.shape)

    ax.plot_surface(X, Y, Z, cmap='viridis')


def lsp(start, end, subdiv = 5):
    """
    Wrap `np.linspace` but generate `(end-start) * subdiv` points instead of
    fixed number
    """
    return np.linspace(start, end, int(abs(end - start) * subdiv))

def to_latex_eqn(eqn, direct = False, prettify = True):
    """
    Convert `eqn` to latex equation.

    :param prettify: make resulting latex better suited for display.  Convert
        `frac` to `cfrac` and make other adjustments.
    :param direct: convert to latex without doing anything else
    :param eqn: equation to convert.  Can be either list or single equation.
        In case of a list first and second elemenst are converted into latex.
        In case of single equation left side is assumend to be equal zero so
        `0=` is returned as left side and converted equation is retuned as
        right side.
    """
    if direct:
        return (None, sp.latex(eqn))


    eqnlhs = ""
    eqntext = ""
    if isinstance(eqn, list):
        if isinstance(eqn[0], str):
            eqnlhs = eqn[0]
        else:
            eqnlhs = sp.latex(eqn[0])

        eqntext = sp.latex(eqn[1])
    else:
        eqnlhs = "0"
        eqntext = sp.latex(eqn)

    # TODO use replacement for list pairs and prettify both sides of eqnation
    if prettify:
      eqntext = re.sub(r"\\frac", r"\\cfrac", eqntext)

    return (eqnlhs, eqntext)


def printsystem(
        eqns,
        alignon = [r"( -| \+)"],
        name = None,
        add_brace = True
):
    """
    Print system of equations.

    :param eqns: list of equations
    :param alignon: list of regular expressions.  Repeatedly substitute each
        regexp's match with `& \1 &`.  Default value converts all plus and
        minus signs into `&=&` basically aligning each equation on these signs.
        THis does not respect fractions and other complex structures that might
        have signs inside them so you need to know what kind of latex code your
        equation might expand into.  Setting this value to `None` or `False`
        will effectively disable all aligning
    :param add_brace: whether or not to add left brace for equation.  Can be
        either `True/False` or list of two elements - left and right brace
        (will be substituted into `\\left` and `\\right` for equation wrapper).
        If there is only one element in list `.` will be used as value for
        `\\right`
    :param name: string to print before the equation.  Can be anything, it will
        be just printed before equatino without any wrappers and convertions
    """
    if name:
        print(name, "\n")

    print("\\[")
    if add_brace == True:
        print("\\left\\{")
    elif isinstance(add_brace, list) and add_brace.len > 0:
        print(f"\\left{add_brace[0]}")

    print("\\begin{aligned}")
    for eqn in eqns:
        lhs, rhs = to_latex_eqn(eqn)
        if alignon:
            for pattern in alignon:
              rhs = re.sub(pattern, r"& \1 &", rhs)

        print(f"{lhs} &= {rhs} \\\\")
    print("\\end{aligned}")

    if add_brace:
        print("\\right.")
    elif isinstance(add_brace, list):
        if add_brace.len > 1:
            print(f"\\right{add_brace[1]}")
        else:
            print(f"\\right.")

    print("\\]\n\\\\")

def to_latex(
        eqn,
        enclosed = True,
        name = None,
        do_print = True,
        direct = False
):
    """
    Convert `eqn` to latex
    :param eqn: equation to convert
    :param direct: passted to `to_latex_eqn`
    :param name: printed before exported latex code
    :param do_print: print value or return it as string
    :param enclosed: wrap resulting equation into `\\[\\]`
    """
    lhs, rhs = to_latex_eqn(eqn, direct)
    text = ""
    if lhs:
        text = f"{lhs} = {rhs}"
    else:
        text = rhs

    if enclosed:
        text = f"\\[{text}\\]"

    if name != None:
        text = name + "\n" + text

    text += "\n"

    if do_print:
        print(text)
    else:
        return text


if __name__ == '__main__':
    print(clean(1.1+2.123121314j))
    print(clean(0.2 + 0.1))

# test
