#!/usr/bin/env perl
# -*- coding: utf-8 -*-
# perl

use warnings;
use strict;

# Command line argument parsing
use Pod::Usage;
use Getopt::Long;
use Switch;
use Cwd;

# Allow to use true and false as named constants
use constant false => 0;
use constant true  => 1;

# Allow to use functions with parameter signatures
use v5.20;
use feature qw(signatures);
no warnings qw(experimental::signatures);
my $dry_run;

my $help;

sub err1($message) {
    system("colecho -e2 -- $message");
}

sub log1($message) {
    system("colecho -- $message");
}

sub sys_proxy($command) {
    system("colecho -l2 -su -- $command");
    unless ( defined $dry_run ) {
        system("$command");
    }
}


sub create_rust_bin_project($project_name) {
    log1("Creating project name [ $project_name ]");
    log1(sprintf("Current path [ %s ]", getcwd));
    system("rm -r $project_name");
    sys_proxy("cargo new $project_name --bin");
    chdir $project_name;
    log1(sprintf("Current path [ %s ]", getcwd));
    sys_proxy("setup-dev.pl -n --target-files=src/main.rs,test.sh,Cargo.toml --build-target='cargo build' \\
 --run-target='target/debug/$project_name'");
}

my $project_type = $ARGV[$#ARGV - 1];
my $project_name = $ARGV[$#ARGV];

if ($project_name =~ /^-.*$/) {
    err1 "Invalid project name.";
    die;
}


if ($project_type =~ /^-.*$/) {
    err1 "Invalid project type.";
    die;
}

GetOptions(
    "dry-run" => \$dry_run,
) or pod2usage( -verbose => 1 ) && exit 0;


log1("Project type $project_type");
log1("Project name $project_name");

if(defined $dry_run) {
    log1("Dry-run mode");
}

switch($project_type) {
    case "rust-bin" { create_rust_bin_project($project_name) }
    else { err1 ("Undefined project type. Aborting" ); die; }
}
