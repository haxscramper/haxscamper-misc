#!/usr/bin/env perl
# -*- coding: utf-8 -*-
# perl

use warnings;
use strict;

# Command line argument parsing
use Pod::Usage;
use Getopt::Long;

# Allow to use true and false as named constants
use constant false => 0;
use constant true  => 1;

# Allow to use functions with parameter signatures
use v5.20;
use feature qw(signatures);
no warnings qw(experimental::signatures);

my $parser;

sub log1($message) {
    system "colecho -- '$message'";
}

sub err1($message) {
    system "colecho -e2-- '$message'";
}

sub info1($message) {
    system "colecho -i1 -- '$message'";
}

sub uniq {
    my %seen;
    grep !$seen{$_}++, @_;
}
my $read_stdin;

#TODO Add support for reading file list from stdin for mass-tagging
GetOptions( "input" => \$read_stdin, )
  or pod2usage( -debug => 1 ) && exit 0;

sub add_tags ( $file, @new_tags ) {
    $file =~ m/(.*)(#+((\w*,?)*))?(\.(\w*))+/;
    # info1 "Tagging $file";

    my $name = defined $1 ? $1 : $file;
    my $tags = defined $3 ? $3 : "";
    my $extn = defined $6 ? $6 : "";

    # log1 "Name: $name ---- Ext: $extn";

    my $restags = join ",", uniq( split( ",", $tags ), @new_tags );
    my $res = "$name#$restags.$extn";

    # log1 "res:  $res";

    return $res;
}

my @new_tags = grep {$_ =~ m/^.+$/; } split ",", $ARGV[$#ARGV];

if ( defined $read_stdin ) {
    while (<STDIN>) {
        chomp;
        say add_tags($_, @new_tags );
    }
}
else {
    my $file = $ARGV[ $#ARGV - 1 ];
    say add_tags( $file, @new_tags );
}
