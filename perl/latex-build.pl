#!/usr/bin/env perl

use warnings;
use strict;
use v5.20;
use File::Copy;
use Cwd;
use File::Path qw(make_path remove_tree);
use Switch;

use Pod::Usage;
use Getopt::Long;

use constant false => 0;
use constant true  => 1;

use feature qw(signatures);
no warnings qw(experimental::signatures);

my $help;
my $filename = $ARGV[$#ARGV];
my $latex_pdf;
my $latex_luatex;
my $latex_rebuild;
my $format;
(my $file = $filename) =~ s/\.[^.]+$//;


GetOptions(
    "help"       => \$help,
    "pdftex" => \$latex_pdf,
    "luatex" => \$latex_luatex,
    "rebuild" => \$latex_rebuild,
    "format" => \$format,
) or pod2usage( -verbose => 1 ) && exit 0;

if ( defined $help ) {
    pod2usage( -verbose => 1 ) && exit 0;
}


say "Building $filename";
die "File does not exist" if not defined $filename or not -e $filename ;

system("latexindent --silent --overwrite --logfile='latexindent-log.log' $filename");
system("rm $file.bak*");
system("rm latexindent-log.log");

if(defined $format) {
    exit 0;
}

if (not defined $latex_luatex and not defined $latex_pdf) {
    $latex_luatex = true;
}

if (defined $latex_rebuild) {
    say "Cleaning all temporary files";
    system ("latexmk -C $filename");
}

if(defined $latex_pdf) {
    say "Building pdflatex";
    system ("latexmk -pdf --interaction=nonstopmode -latexoption=--shell-escape $filename ")
} elsif (defined $latex_luatex) {
    say "Building lualatex";
    system ("latexmk -lualatex --interaction=nonstopmode -latexoption=--shell-escape $filename ")
}


=head1 NAME

latex-build.pl

=head1 SYNOPSIS

latex-build.pl [OPTIONS] filename

Build latex file at `filename`

=head1 OPTIONS

 --help -h    Print help message
 --pdf        Build using pdflatex
 --luatex     Build using lualatex
 --rebuild    Clean temporary files and then build
 --format     Format only

=cut
