#!/usr/bin/env perl
use warnings;
use strict;

# Command line argument parsing
use Pod::Usage;
use Getopt::Long;

# Allow to use true and false as named constants
use constant false => 0;
use constant true  => 1;

# Allow to use functions with parameter signatures
use v5.20;
use feature qw(signatures);
no warnings qw(experimental::signatures);

my $help;
my $no_interactive;
my $target_files;
my $dry_run;
my $run_target;
my $build_target;

# TODO remove test.sh from run targets using fzf.
# TODO include only executable files in run targets

GetOptions(
    "help"           => \$help,
    "no-interactive" => \$no_interactive,
    "target-files=s" => \$target_files,
    "run-target=s"     => \$run_target,
    "build-target=s"   => \$build_target,
    "dry-run"        => \$dry_run,
) or pod2usage( -verbose => 1 ) && exit 0;


sub err1($message) {
    system("colecho -e2 -- $message");
}

sub log1($message) {
    system("colecho -- $message");
}

sub select_watched_files {
    my $files;
    if ($no_interactive) {
        unless ( defined $target_files ) {
            err1("Target files are not defined. Specify --target-files");
        }

        $files = join "\n", split( ",", $target_files );
    }
    else {
        print "Select files to listen for changes.
Multiple values can be selected using Shift+Tab
test.sh will be added automatically
Press escape to select nothing\n\n";
        $files = `fd . --type f | fzf -m --height=30%`;
        print "Selected\n $files\n";
        chomp $files;
    }

    my $cat_config = <<"EOF_TTT";
cat << EOF | entr sh -c "clear ; colecho -i1 'Running entr ...' ; ./test.sh"
$files
test.sh
EOF
EOF_TTT

    return $cat_config;
}

sub select_run_target {
    if ( defined $no_interactive ) {
        unless (defined $run_target) {
            err1("Missing list of run targets. Provide --run-target");
            exit 1;
        }
        return join "\n", map {"./$_"} split ("," , $run_target );
    }
    else {
        say "Select target to run after change
Target to run also includes build script by default.
Those targes will be placed in test.sh and will run
after each change. NOTE:!!! do not include test.sh
in the run targets as it will cause inifinite recursive
execution";
        my $run_target = `fd . --type f | sed '/run-dev/d' | fzf --height=30%`;
        chomp $run_target;
        say "Selected [ $run_target ]";
        return $run_target;
    }
}

sub select_build_target {
##== Selecting build command
    if ( defined $no_interactive ) {
        unless ( defined $build_target ) {
            err1("Missing list of build targets. Aborting");
            exit 1;
        }

        return $build_target;
    }
    else {
        my %build_options = (
            "cargo build"       => "cargo build",
            "rustc src/main.rs" => "rustc src/main.rs",
            "sbcl --load" => "sbcl --load",    # TODO select file to load
            "clang++" => "clang++",
            "none" => "",
        );

        print "Select build command\n";
        my $build_types = join( "\n", keys %build_options );
        my $build_type = `echo "$build_types" | fzf --height=30%`;
        chomp $build_type;

        my $build_target = $build_options{$build_type};
        say "Target will be built using [ $build_target ] command";
        return $build_target;
    }
}

sub prompt {
    my ($query) = @_;    # take a prompt string as argument
    local $| = 1;        # activate autoflush to immediately show the prompt
    print $query;
    chomp( my $answer = <STDIN> );
    return $answer;
}

sub prompt_yn {
    my ($query) = @_;
    my $answer = prompt("$query (Y/N): ");
    return lc($answer) eq 'y';
}

sub write_file ( $file_name, $extension, $string ) {
    my $file = "$file_name.$extension";

    system("colecho -i3 $file");
    if ( defined $dry_run ) {
        log1("Performing dry run. No files will be generated");
        say "$string";
    }
    else {
        system("create-script.sh $file_name $extension");
        open( FH, ">>", "$file" ) or die $!;
        print FH "$string\n";
        close FH;
        log1("$file content");
        system("bat $file");
    }
}

if ( defined $no_interactive or  prompt_yn("Create files?")) {
    my $cat_config = select_watched_files();
    write_file( "run-dev", "sh",
                "colecho -i1 'Running run-dev.sh'\n\n$cat_config" );

    my $build_target = select_build_target();
    write_file( "build", "sh",
        "colecho -i1 'Running build.sh'\n\n$build_target\n" );

    my $run_target = select_run_target();
    write_file(
        "test", "sh", <<"EOF"
# This file file will run on each update
colecho -i1 "Running test.sh"
##== Only edit lines after this comment\n
# Build target
./build.sh

# Run target
./$run_target
EOF
      );

    exit 0;
}
else {
    say "File creation cancelled. Exiting";
    exit 1;
}
