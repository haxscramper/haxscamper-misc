#!/usr/bin/env perl

use warnings;
use strict;

# Command line argument parsing
use Pod::Usage;
use Getopt::Long;

# Allow to use true and false as named constants
use constant false => 0;
use constant true  => 1;

# Allow to use functions with parameter signatures
use v5.20;
use feature qw(signatures);
no warnings qw(experimental::signatures);

my $input = <<'EOF';
The electric potential at a point r in a static electric field E is given by the line integral

    V E = − ∫ C E ⋅ d ℓ \displaystyle V_{\mathbf {E }=-\int _{C}\mathbf E \cdot \mathrm d {\boldsymbol \ell }}\, V_\mathbf{E} = - \int_C \mathbf{E} \cdot \mathrm{d} \boldsymbol{\ell} \,

where C is an arbitrary path connecting the point with zero potential to r. When the curl ∇ × E is zero, the line integral above does not depend on the specific path C chosen but only on its endpoints. In this case, the electric field is conservative and determined by the gradient of the potential:

    E = − ∇ V E . \displaystyle \mathbf {E =-\mathbf \nabla  V_{\mathbf E }.\,} \mathbf{E} = - \mathbf{\nabla} V_\mathbf{E}. \,

Then, by Gauss's law, the potential satisfies Poisson's equation:

    ∇ ⋅ E = ∇ ⋅ ( − ∇ V E ) = − ∇ 2 V E = ρ / \varepsilon 0 , \displaystyle \mathbf {\nabla  \cdot \mathbf E =\mathbf \nabla  \cdot \left(-\mathbf \nabla  V_{\mathbf E }\right)=-\nabla ^{2}V_{\mathbf E }=\rho /\varepsilon _{0},\,} \mathbf{\nabla} \cdot \mathbf{E} = \mathbf{\nabla} \cdot \left (- \mathbf{\nabla} V_\mathbf{E} \right ) = -\nabla^2 V_\mathbf{E} = \rho / \varepsilon_0, \,

where ρ is the total charge density (including bound charge) and ∇\cdot denotes the divergence.

The concept of electric potential is closely linked with potential energy. A test charge q has an electric potential energy UE given by

    U E = q V . \displaystyle U_{\mathbf {E }=q\,V.\,} U_ \mathbf{E} = q\,V. \,

The potential energy and hence also the electric potential is only defined up to an additive constant: one must arbitrarily choose a position where the potential energy and the electric potential are zero.
EOF

#$input = `xclip -sel cli -o`;


system "colecho -i3 'Running replacement'";

$input = `xclip -sel cli -o`;

system "colecho  'Copied from clipboard'";

$input =~ s!:<math>(.*?)</math>!\\[\n$1\n\\]!g;
$input =~ s!<math>(.*?)</math>!\$$1\$]!g;
$input =~ s!\{\{mvar\|(.*?)\}\}!\$$1\$!g;
$input =~ s!\{\{math\|(.*?)\}\}!\$$1\$!g;
$input =~ s!<ref>.*</ref>!!g;
$input =~ s!\[\[.*\|(.*?)\]\]!$1!g;
$input =~ s!\[\[(.*?)\]\]!$1!g;
$input =~ s!'''(.*?)'''!{$1}!g;
$input =~ s!''(.*?)''!{$1}!g;
$input =~ s!<sub>(.*?)</sub>!_{$1}!g;
$input =~ s!<ref(.*)/>!!g;

$input =~ s!Φ!\\Phi!g;
$input =~ s!ε!\\varepsilon!g;
$input =~ s!·!\\cdot!g;
$input =~ s!⋅!\\cdot!g;
$input =~ s!∇!\\nabla!g;
$input =~ s!∫!\\int!g;
$input =~ s!ρ!\\rho!g;
$input =~ s!×!\\times!g;
$input =~ s!ℓ!\\ell!g;
# $input =~ s!!\\cdot!g;
# $input =~ s!!\\cdot!g;
# $input =~ s!!\\cdot!g;

$input =~ s!\{\{(.*?)\}\}!{$1}!g;
$input =~ s! \{(.*?)\}([ |\.])! $1$2!g;

system "colecho  'Completed replacement'";

open TMP, '| xclip -sel cli';
print TMP $input;
close TMP;


system "colecho -i0 'Done'";
