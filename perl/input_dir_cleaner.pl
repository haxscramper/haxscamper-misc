#!/usr/bin/env perl

use warnings;
use strict;

# Command line argument parsing
use Pod::Usage;
use Getopt::Long;
use File::Copy;
use File::Path;

# Allow to use true and false as named constants
use constant false => 0;
use constant true  => 1;

# Allow to use functions with parameter signatures
use v5.20;
use feature qw(signatures);
no warnings qw(experimental::signatures);


my $HOME = $ENV{"HOME"};

my $from = "$HOME/defaultdirs/input";
my $to   = "$HOME/defaultdirs/automatic";

sub colecho ( $type, $text ) {
    system("colecho $type $text");
}

sub colecho1($text) {
    system("colecho $text");
}

sub remove_files ($files_glob) {
    my @files = glob $files_glob;
    foreach my $file (@files) {
        colecho1 ("Deleting $file");
        unlink $file;
    }
}

sub move_dir ( $from_glob, $to_dir, $message ) {
    my @files = glob $from_glob;

    colecho '-i2', "Moving $message files";

    if (scalar @files == 0) {
        colecho1 "No files to move";
        return;
    } else {
        colecho1 "Moving to $to_dir";
    }

    chomp $to_dir;

    unless ( -d $to_dir) {
        mkpath $to_dir;
    }


    foreach my $file (@files) {
        if ( -f $file ) {
            say "    $file";
            move( $file, $to_dir );
        }
    }

    colecho1 "Moved $message files";
}

my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime();

my $auto = "auto_" . `date --iso-8601`;

colecho -i3, "Cleaning up input directory";
colecho1 "Date dir $auto";

foreach my $file (glob "$from/*.jpg:large.jpe") {
    (my $new = $file) =~ s!:large\.jpe!!;
    move "$file", "$new";
}

colecho -i3, "Moving files from autosync input";
move_dir "$HOME/defaultdirs/autosync/input/*", "$from", "autosync";

colecho -i3, "Moving files from default directories";
move_dir "$HOME/{Downloads,Videos,Pictures}/*", "$HOME/defaultdirs/input",
  "default directory";

move_dir "$from/*.{pdf,PDF}", "$to/pdf/$auto", "pdf";

move_dir "$from/*.{jpg,png,jpeg,gif,webp,jpg_large}", "$to/images/$auto",
  "image";

move_dir "$from/*.{rar,zip,7z,tar.xz,tar.gz,tar}", "$to/archives/$auto", "archive";

move_dir "$from/*.{txt,TXT,xlsx,docx,tex,doc,djvu,epub,fb2,mobi,odt,html,htm}",
         "$to/documents/$auto",
         "document";

move_dir "$from/*.{webm,mp4}", "$to/videos/$auto", "video";

move_dir "$from/*.{mp3,wav}", "$to/videos/$auto", "music";

move_dir "$from/*.{tex,cpp,sh,pl}", "$to/code/$auto", "code";

colecho '-i2', "Done moving files";
colecho '-w2', "Deleting files";

remove_files("$from/*.{toc,fls,out,fdb_latexmk,bak,aux,synctex.gz,log,crdownload,exe,jar}");

colecho '-i3', "Done cleaning input directory";

colecho1 "Input directory content:";
system("exa $from");
