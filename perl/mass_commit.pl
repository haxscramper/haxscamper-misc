#! /usr/bin/perl

use strict;
use warnings;
use 5.020;
use Cwd;
use Getopt::Long;

# TODO Invole text editor if no -m is supplied
# TODO Use git template for commit message

my $topdir = getcwd;
my @changed    = `git --work-tree=. status --porcelain`;
my @submodules = ();


my $message;
my $replace;
GetOptions(
    "message=s" => \$message,
    "replace"   => \$replace
);

die("Requires commit message") if not defined $message;

for my $item (@changed) {
    if ( $item =~ m/ M (.*)/ and !$replace ) {
        push @submodules, $1;
    } elsif ($item =~ m/M (.*)/ and $replace ) {
        push @submodules, $1;
    }
}

for my $submodule (@submodules) {
    $submodule =~ /\s*(.*)/;
    my $subdir = "$topdir/$1";
    chdir $subdir;
    say "Working in submodule $1";
    if ($replace) {
        `git commit --amend -m \"$message\"`;
        system(" git --no-pager log -1");
    } else {
        `git add .`;
        `git commit -m $message`;
        system(" git --no-pager log -1");
    }
}

