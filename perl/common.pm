package scripts::common;
use strict;
use warnings;

use Term::ANSIColor;
use v5.20;

use constant false => 0;
use constant true  => 1;

use feature qw(signatures);
no warnings qw(experimental::signatures);

use Exporter qw(import);
our @EXPORT_OK = qw(print_error print_status_small print_status_big print_warning print_status);


sub print_error($message) {
    print color('bold red');
    say "--- $message";
    print color('reset');
}

sub print_status_small($message) {
    print color('white');
    say "--- $message";
    print color('reset');
}

sub print_status($message) {
    print color('bold green');
    say "--- $message";
    print color('reset');
}

sub print_status_big($message) {
    print color('bold blue');
    say "!!! $message";
    print color('reset');
}

sub print_warning($message) {
    print color('yellow');
    say "*** $message";
    print color('reset');
}

1;
