#!/usr/bin/perl
use strict;
use warnings;
use File::Find;
use Cwd;
use Modern::Perl;
use File::Path qw(make_path remove_tree);


sub prompt {
    my ($prompt, $default_path) = @_;
    print ("$prompt \033[32m[\033[0m ./$default_path \033[32m] \033[0m");

    my $input = <STDIN>;
    chomp $input;

    if ($input eq "") {
        $input = $default_path;
    }

    return $input;
}

my $folders = [
    ["Main source", "source"],
    ["Documentation", "docs"],
    ["Notes", "notes"],
    ["Data", "tests"],
    ["External git modules", "external"],
    ["Work-in-progres temporary files", "wip"],
    ["Auto-generated temporary files", "tmp"],
    ["Build directory", "build"],
    ["Old files folder", "old"]
    ];


foreach my $folder (@$folders) {
    unless (-d @$folder[1]) {
        my $final_path = prompt(@$folder[0], @$folder[1]);
        make_path($final_path);
    }
}
