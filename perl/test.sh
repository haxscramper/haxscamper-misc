#!/usr/bin/env bash
# -*- coding: utf-8 -*-
# bash
set -o nounset
set -o errexit

./tag_file.pl  Robert\ J.\ Chassell\ -\ An\ introduction\ to\ programming\ in\ emacs\ lisp-Free\ Software\ Foundation\ \(2001\).pdf hell,e,e


./tag_file.pl  "hhhkkk.dd.ee" "dd,.df.asd..sadf.s,sad#d"
./tag_file.pl  -- "hhhkkk.dd.ee" "dd,.df.asd..sadf.s,sad#d"

./tag_file.pl "hello.pdf" a,b,c

echo "hello" | ./tag_file.pl -i -- "tag,t,e,a,,a,as,"

ls -1 | ./tag_file.pl -i 'test,tag'
