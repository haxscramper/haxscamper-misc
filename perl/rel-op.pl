#!/usr/bin/env perl

use Getopt::Long;
use Cwd 'abs_path';
use File::Basename;
use strict;

use Pod::Usage;
use autodie;

# VERSION

=head1 SYNOPSIS

    rel-op.pl -f [target file] -s [relative path]

    -f relative name of input file
    -r relative position to first file
    -c command to execute on resulting path

=head1 DESCRIPTION

...

=cut

my $file      = "";
my $relative  = "";
my $command   = "bat";
my $help      = "";

GetOptions (
    "relative=s"=> \$relative,
    "file=s"    => \$file,
    "command=s" => \$command,
    "help"      => \$help
    )

or pod2usage(q(-verbose) => 1);
pod2usage(q(-verbose) => 1) if $help;
pod2usage(q(-verbose) => 1) if not $file;
pod2usage(q(-verbose) => 1) if not $relative;

my $dirname = dirname(abs_path($file));
my $relative_target = "$dirname/$relative";

$command = "colorls" if $command == "ls";

system("$command $relative_target");
