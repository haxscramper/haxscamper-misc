#!/usr/bin/env bash
cd "$(dirname "$0")"
set -o errexit

cecho -i1 "Running build.sh"\n
RUSTFLAGS="$RUSTFLAGS -A dead_code" cargo build
