#![allow(dead_code)]
#![allow(unused_variables)]
#![allow(unused_imports)]
#![allow(unused_mut)]
#![allow(unused_must_use)]

#[macro_use]
extern crate clap;
use colored::*;
use std::cmp::min;
use std::io::{stdin, stdout, Write};
use terminal_size::{terminal_size, Width};
extern crate colecho_lib;
use colecho_lib::a::*;
use colecho_lib::termformat::*;
use std::fs::File;
use std::io::prelude::*;

// TODO Add support for string-indexed lists

// TODO Add support for lists with description (long description that
// takes multiple lines)

enum SelectionType {
    Singular,
    Multiple,
}

fn select_options_with_description(
    options: &Vec<(u16, String, String)>,
    sel_type: SelectionType,
) -> Option<Vec<u16>> {
    println!(
        "\nSelect {} from the list.\n",
        match sel_type {
            SelectionType::Singular => {
                "single option".bold().yellow().dimmed()
            }
            SelectionType::Multiple => {
                "one or more options".bold().yellow().dimmed()
            }
        },
    );


    for (idx, name, value) in options.iter() {
        let descr_lines = justify_fit_terminal((5,4),&value);

        println!(
            "{:>04}) {}\n{}",
            idx.to_string().bold().green(),
            name,
            descr_lines.join("\n").bright_black().dimmed()
        );
    }

    colecho(MessageType::Log, 3, "", "\n");
    std::io::stdout().flush();

    let mut buffer = String::new();

    if let Ok(_) = std::io::stdin().read_line(&mut buffer) {
        let mut res: Vec<u16> = buffer
            .split(",")
            .map(|s| s.trim())
            .filter(|s| !s.is_empty())
            .map(|s| s.parse())
            .filter_map(Result::ok)
            .collect();

        // TODO show warning on input that cannot be parsed
        if res.len() == 0 {
            colecho(
                MessageType::Error,
                0,
                "Failed to parse all arguments.\n",
                "\n  ",
            );
            return None;
        } else {
            return Some(res);
        }
    } else {
        return None;
    }
}

fn select_options(
    options: &Vec<(u16, String)>,
    sel_type: SelectionType,
) -> Option<Vec<u16>> {
    println!(
        "\nSelect {} from the list.\n",
        match sel_type {
            SelectionType::Singular => {
                "single option".bold().yellow().dimmed()
            }
            SelectionType::Multiple => {
                "one or more options".bold().yellow().dimmed()
            }
        },
    );

    let max_width: usize = options
        .iter() //
        .map(|(_, descr)| descr.len())
        .max()
        .unwrap();

    let widht_increase = 12;

    let (items_per_row, need_wrap) =
        if let Some((Width(term_width), _)) = terminal_size() {
            let term_width: usize = min(term_width as usize, 75);
            if max_width + 12 > term_width {
                (1, true)
            } else {
                (term_width / (max_width + 12), false)
            }
        } else {
            (1, false)
        };

    let row_count = options.len() / items_per_row;

    for row in 0..=row_count {
        for item in 0..items_per_row {
            if let Some((k, v)) = &options.get(row * items_per_row + item)
            {
                print!("{:>4}) {}", k.to_string().bold().green(), v);
            }
        }
        println!("");
    }

    colecho(MessageType::Log, 3, "", "\n");
    std::io::stdout().flush();

    let mut buffer = String::new();

    if let Ok(_) = std::io::stdin().read_line(&mut buffer) {
        let mut res: Vec<u16> = buffer
            .split(",")
            .map(|s| s.trim())
            .filter(|s| !s.is_empty())
            .map(|s| s.parse())
            .filter_map(Result::ok)
            .collect();

        // TODO show warning on input that cannot be parsed
        if res.len() == 0 {
            colecho(
                MessageType::Error,
                0,
                "Failed to parse all arguments.\n",
                "\n",
            );
            return None;
        } else {
            return Some(res);
        }
    } else {
        return None;
    }
}

fn get_yes_no_reply(previous_error: bool) -> bool {
    let mut buffer = String::new();

    if previous_error {
        colecho(
            MessageType::Warn,
            1,
            "Only yes or no is accepted\n",
            "\n",
        );
    }

    colecho(MessageType::Log, 3, "", "\n  ");
    if let Ok(reply) = std::io::stdin().read_line(&mut buffer) {
        match &buffer[..] {
            "yes" | "y" | "Yes" => true,
            "no" | "n" => false,
            _ => get_yes_no_reply(true),
        }
    } else {
        get_yes_no_reply(true)
    }
}

fn prompt_yes_no() -> bool {
    print!("\n   Anwer [{}/{}]", "yes".green(), "no".red());

    get_yes_no_reply(false)
}

fn stdin_to_vec() -> Vec<String> {
    let mut buffer: String = String::new();
    let mut opts: Vec<String> = Vec::new();

    loop {
        if let Ok(_) = std::io::stdin().read_line(&mut buffer) {
            if buffer.contains("<><><><><>") {
                break;
            }

            buffer.pop();
            opts.push(buffer);
            buffer = String::new();
        }
    }

    return opts;
}

fn main() {
    let args: clap::ArgMatches = clap_app!(myapp =>
                   (version: "0.1.0")
                   (@subcommand sel =>
                    (@arg single_select: -s --single "Number of selection options")
                    (@arg description_select: -d --description "Provide description for each item")
                    (@arg stdin_read: -i --input
         "Read names from the standard input until character
sequence '<><><><><>' is reached") // TODO determine size of the terminal and format string to fit width
                    (@arg OPTIONS:
                     +required
                     ...
                     conflicts_with("stdin_read")
                     "Set of strings (or pairs if description is added)")
                   )
                   (@subcommand ask =>
                    (@arg number: -n --number "Ask for signed number")
                   )
                   (about: "Print colored text with optional prefixes")
                )
    .get_matches();

    if let Some(sel_matches) = args.subcommand_matches("sel") {
        let mut selected_options: Vec<u16> = Vec::new();

        if sel_matches.is_present("description_select") {
            let mut opts: Vec<(u16, String, String)> = Vec::new();

            let options: Vec<String> =
                if sel_matches.is_present("stdin_read") {
                    stdin_to_vec()
                } else {
                    sel_matches
                        .values_of("OPTIONS")
                        .unwrap()
                        .map(|s| s.to_string())
                        .collect()
                };

            if options.len() % 2 != 0 {
                colecho(
                    MessageType::Error,
                    2,
                    "Odd number of option items\n",
                    "",
                );
            }

            for i in 0..(options.len() / 2) {
                opts.push((
                    i as u16,
                    options[2 * i].to_string(),
                    options[2 * i + 1].to_string(),
                ));
            }

            if let Some(sel) = select_options_with_description(
                &opts,
                if sel_matches.is_present("single_select") {
                    SelectionType::Singular
                } else {
                    SelectionType::Multiple
                },
            ) {
                println!("{:?}", sel);
                selected_options = sel;
            }
        } else {
            let options: Vec<(u16, String)> =
                if sel_matches.is_present("stdin_read") {
                    stdin_to_vec()
                } else {
                    sel_matches
                        .values_of("OPTIONS")
                        .unwrap()
                        .map(|s| s.to_string())
                        .collect()
                }
                .iter()
                .enumerate()
                .map(|(i, s)| (i as u16, s.to_string()))
                .collect();

            if let Some(sel) = select_options(
                &options,
                if sel_matches.is_present("single_select") {
                    SelectionType::Singular
                } else {
                    SelectionType::Multiple
                },
            ) {
                selected_options = sel;
            }
        }

        let sel_result = selected_options
            .iter()
            .map(|i| i.to_string())
            .collect::<Vec<String>>()
            .join(",");

        let mut file =
            File::create("UPROMPT_SELECTION_TMP").expect("Fuck rust");
        file.write_all(sel_result.as_bytes());
    } else if let Some(prompt_matches) = args.subcommand_matches("ask") {
        if prompt_matches.is_present("number") {
            println!("{}", prompt_yes_no());
        }
    }
}
