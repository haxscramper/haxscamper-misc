#!/usr/bin/env bash
cd "$(dirname "$0")"
set -o nounset
set -o errexit

colecho -i1 "Running test.sh"
##== Only edit lines after this comment

./build.sh

# exit 0;

# echo -ne "1" | ./target/debug/uprompt sel -s
# echo -ne "2" | ./target/debug/uprompt sel
# echo -ne "2,3,4" | ./target/debug/uprompt sel
# echo -ne "wr" | ./target/debug/uprompt sel
#
#
# echo -ne "1" | ./target/debug/uprompt sel -sd
# echo -ne "2" | ./target/debug/uprompt sel -d
# echo -ne "2,3,4" | ./target/debug/uprompt sel -d
# echo -ne "wr" | ./target/debug/uprompt sel -d
#
# echo -ne "yes" | ./target/debug/uprompt ask -n
# echo -ne "no" | ./target/debug/uprompt ask -n
# echo -ne "sadf\nyes" | ./target/debug/uprompt ask -n

echo -ne "1,2,3" | ./target/debug/uprompt sel -d "[REPO]"
echo -ne "1,2,3" | ./target/debug/uprompt sel -d "[REPO]" "Changes related to the repository have beed made"

cat << EOF | ./target/debug/uprompt sel -di
[!!!]
Breaking change
[>>>]
Not build-able, broken code
[WIP]
Work in progress
[???]
No idea how to describe changes
[CLEAN]
Usually file-related cleanup
[FEATURE]
New feature
[FIX(type)]
Bug fix (text in parenthesis can be any of: [compile, runtime] \
(runtime errors can be [rutime:segfault, runtime:logic \
runtime:algorithm, runtime:edgecase] and so on. Try to be as precise \
as possible when choosing fix type)
[REFACTOR]
Refactoring production code
[STYLE]
Formatting, missing semi colons, etc; no code change
[DOC]
Changes to documentation
[TEST]
Adding or refactoring tests; no production code change
[HACK]
Temporary fix to make things move forward; please avoid it
[REPO]
Related to management of git repository (submodules, tags, merges etc.)
[VERSION]
More or less complete version of product
<><><><><><>
1,2,3
EOF

colecho -i1 "Done selection"
IFS=',' read -r -a sel_res <<< $(cat "UPROMPT_SELECTION_TMP")
rm UPROMPT_SELECTION_TMP

for idx in "${sel_res[@]}"
do
    colecho "$idx"
done

colecho -i1 "Done"
