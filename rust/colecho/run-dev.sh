#!/usr/bin/env bash
cd "$(dirname "$0")"
set -o nounset


cat << EOF | entr -c "./test.sh"
test.sh
run-dev.sh
src/main.rs
Cargo.toml
$(ls -1 ../colecho_lib/src/*)
EOF
