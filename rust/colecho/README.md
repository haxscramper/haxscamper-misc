Command line utility that simplifies printing of the colored prefixes
from the scripts: instead of specifying escape codes each time you can
use on of the default styles (log, info, warn, error) and four levels
for each style (0-3) as well as prefix types (short, verbose, one that
is emulates gtest framework prefix and one that is easy to parse).

Images are availiable
[here](https://gitlab.com/haxscamper-misc/settings-public/wikis/colecho-print-examples)
