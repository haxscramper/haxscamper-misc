#!/usr/bin/env bash
cd "$(dirname "$0")"

RUST_BACKTRACE=1

cargo build

c=target/debug/colecho

# $c -i5 "Info fail test"

# $c -s='b' "Bold selected"
# $c -s='u' "Underline selected"
# $c -s='b' -s='u' "Bold+underline"
# $c -s='f' "Flicker"
# # $c -f='q' "Foreground color fail test"
# $c -f='r' "Red foreground"
# $c -u -f='r' "Red foreground, uniform coloring"
# $c -u -f='r' -e1 "Red foreground, uniform coloring, error 1 prefix"
# $c -u -f='r' -b='d' -e1 "Red on black, uniform coloring, error 1 prefix"

# styles=( "default" "verbose" "gtest" "parser" "bright")
styles=( "default" )

$c -e2 -- $(cat << EOF
  $c -l3 --$style "$style log 3"

  $c -i0 --$style "$style info 0"
  $c -i1 --$style "$style info 1"
  $c -i2 --$style "$style info 2"
  $c -i3 --$style "$style info 3"

  $c -w0 --$style "$style warn 0"
  $c -w1 --$style "$style warn 1"
  $c -w2 --$style "$style warn 2"
  $c -w3 --$style "$style warn 3"

  $c -e0 --$style "$style error 0"
  $c -e1 --$style "$style error 1"
  $c -e2 --$style "$style error 2"
  $c -e3 --$style "$style error 3"
done

for val in `seq 0 3`
EOF
)

exit 0


for style in "${styles[@]}"
do
  $c -l0 --$style "$style log 0"
  $c -l1 --$style "$style log 1"
  $c -l2 --$style "$style log 2"
  $c -l3 --$style "$style log 3"

  $c -i0 --$style "$style info 0"
  $c -i1 --$style "$style info 1"
  $c -i2 --$style "$style info 2"
  $c -i3 --$style "$style info 3"

  $c -w0 --$style "$style warn 0"
  $c -w1 --$style "$style warn 1"
  $c -w2 --$style "$style warn 2"
  $c -w3 --$style "$style warn 3"

  $c -e0 --$style "$style error 0"
  $c -e1 --$style "$style error 1"
  $c -e2 --$style "$style error 2"
  $c -e3 --$style "$style error 3"
done

for val in `seq 0 3`
do
    $c -e0 -I$val "$val"
done

for val in `seq 3 -1 0`
do
    $c -I$val "$val"
done
