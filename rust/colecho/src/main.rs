#[macro_use]
extern crate clap;
extern crate colecho_lib;
extern crate colored;
use colecho_lib::a::*;
use colecho_lib::termformat::*;
use colored::*;

fn get_text_color(
    args: &clap::ArgMatches,
    mut res: colored::ColoredString,
) -> colored::ColoredString {
    if let Some(styles) = args.values_of("style_options") {
        for opt in styles {
            res = match opt {
                "b" | "bold" => res.bold(),
                "u" | "underline" => res.underline(),
                "i" | "italic" => res.italic(),
                "d" | "dimmed" => res.dimmed(),
                "r" | "reversed" => res.reversed(),
                "f" | "flicker" | "blink" => res.blink(),
                "h" | "hidden" => res.hidden(),
                "s" | "strikethrough" => res.strikethrough(),
                _ => res,
            };
        }
    }

    if let Some(color) = args.value_of("foreground_color") {
        res = match color {
            "d" | "black" => res.black(),
            "r" | "red" => res.red(),
            "g" | "green" => res.green(),
            "y" | "yellow" => res.yellow(),
            "b" | "blue" => res.blue(),
            "m" | "magenta" => res.magenta(),
            "c" | "cyan" => res.cyan(),
            "w" | "white" => res.white(),
            _ => res,
        };
    }

    if let Some(color) = args.value_of("background_color") {
        res = match color {
            "d" | "black" => res.on_black(),
            "r" | "red" => res.on_red(),
            "g" | "green" => res.on_green(),
            "y" | "yellow" => res.on_yellow(),
            "b" | "blue" => res.on_blue(),
            "m" | "magenta" => res.on_magenta(),
            "c" | "cyan" => res.on_cyan(),
            "w" | "white" => res.on_white(),
            _ => res,
        };
    }

    return res;
}

fn args_to_enum(
    args: &clap::ArgMatches,
) -> (MessageType, MessageStyle, u8) {
    let (message_type, message_level): (MessageType, u8) =
        if let Some(info_level) = args.value_of("info_style") {
            (MessageType::Info, info_level.parse::<u8>().unwrap_or(0))
        //
        } else if let Some(warn_level) = args.value_of("warn_style") {
            (MessageType::Warn, warn_level.parse::<u8>().unwrap_or(0))
        //
        } else if let Some(error_level) = args.value_of("error_style") {
            (MessageType::Error, error_level.parse::<u8>().unwrap_or(0))
        //
        } else if let Some(log_level) = args.value_of("log_style") {
            (MessageType::Log, log_level.parse::<u8>().unwrap_or(0))
        //
        } else {
            (MessageType::Log, 0)
        };

    let message_style = args.value_of("prefix_type").unwrap_or("default");
    let message_style = if args.is_present("enable_parser")
        || message_style == "parser"
    {
        MessageStyle::Parser
    } else if args.is_present("enable_gtest") || message_style == "gtest" {
        MessageStyle::Gtest
    } else if args.is_present("enable_verbose")
        || message_style == "verbose"
    {
        MessageStyle::Verbose
    } else if args.is_present("enable_bright") || message_style == "bright"
    {
        MessageStyle::Bright
    } else if args.is_present("enable_default")
        || message_style == "default"
    {
        MessageStyle::Default
    } else {
        MessageStyle::Default
    };

    return (message_type, message_style, message_level);
}

fn prefix_from_enum(
    message_type: &MessageType,
    message_style: &MessageStyle,
    message_level: &u8,
) -> colored::ColoredString {
    let pref: colored::ColoredString =
        from_enum(&message_type, &message_level, &message_style);
    let pref = color_from_enum(pref, &message_type, &message_level);

    if message_style == &MessageStyle::Parser {
        pref.clear()
    } else {
        pref
    }
}

fn prefix_style(args: &clap::ArgMatches) -> colored::ColoredString {
    let (message_type, message_style, message_level) = args_to_enum(args);

    prefix_from_enum(&message_type, &message_style, &message_level)
}

fn main() {
    let args: clap::ArgMatches = clap_app!(myapp =>
       (version: "0.3.2")
       (about: "Print colored text with optional prefixes")
       (@arg style_options: -s --style +takes_value ...
        "Set additional styling")
       (@arg foreground_color: -f --foreground +takes_value
        possible_values(&[
            "d" , "black",
            "r" , "red",
            "g" , "green",
            "y" , "yellow",
            "b" , "blue",
            "m" , "magenta",
            "c" , "cyan",
            "w" , "white"])
        "Set foreground color")
       (@arg background_color: -b --background +takes_value
        possible_values(&[
                "d" , "black",
                "r" , "red",
                "g" , "green",
                "y" , "yellow",
                "b" , "blue",
                "m" , "magenta",
                "c" , "cyan",
                "w" , "white"])
        "Set background color")
       (@arg info_style: -i --info +takes_value
        possible_values(&["0", "1", "2", "3"])
        "Use info style")
       (@arg indent: -I --Indent +takes_value
        "Use increase indentation")
       (@arg warn_style: -w --warn +takes_value
        "Use warn style")
       (@arg log_style: -l --log +takes_value
        default_value("0")
        possible_values(&["0", "1", "2", "3"])
        "Use log style")
       (@arg error_style: -e --error +takes_value
        "Use error style")
       (@arg uniform_style: -u --uniform
        "Color all string on one style, not just prefix")
       (@arg prefix_disabled: -P --pfxdisabled
        "Do not add prefix")
       (@arg show_all: --("show-all")
        "Show all possibly styles (excluding custom ones)")
       (@arg prefix_type: -p --pfxtype +takes_value
        possible_values(&["verbose", "parser", "gtest", "default", "bright"])
        "Which prefix type to use (verbose, parser etc.)")
       (@arg enable_verbose: -v --verbose "Shorthand for verbose prefixtype")
       (@arg enable_gtest: -g --gtest "Shorthand for gtest prefixtype")
       (@arg enable_parser: --parser  "Shorthand for parser prefixtype")
       (@arg enable_default: --default  "Shorthand for default prefixtype")
       (@arg enable_bright: --bright "Shorthand for bright prefixtype")
       (@arg STRING: +required ...
        "String to format")
    )
    .get_matches();

    let res: colored::ColoredString = args
        .values_of("STRING")
        .unwrap()
        .map(|s| s.to_string())
        .collect::<Vec<String>>()
        .join(" ")
        .normal();

    if args.is_present("show_all") {
        let types: Vec<(MessageType, String)> = vec![
            (MessageType::Log, String::from("l")),
            (MessageType::Info, String::from("i")),
            (MessageType::Warn, String::from("w")),
            (MessageType::Error, String::from("e")),
        ];

        let styles: Vec<(MessageStyle, String)> = vec![
            (MessageStyle::Gtest, String::from("-g")),
            (MessageStyle::Default, String::from("--default")),
            (MessageStyle::Verbose, String::from("-v")),
            (MessageStyle::Bright, String::from("--bright")),
            (MessageStyle::Parser, String::from("--parser")),
        ];

        for (message_style, style_arg) in &styles {
            for (message_type, type_arg) in &types {
                for message_level in 0..=3 {
                    let prefix: colored::ColoredString = prefix_from_enum(
                        &message_type,
                        &message_style,
                        &message_level,
                    );

                    println!(
                        "{} type:level -{}{} style: {}",
                        prefix, type_arg, message_level, style_arg
                    );
                }
            }
        }
        return;
    }

    let mut message = justify_fit_terminal((4, 10), &res);
    let mut res = message[0].trim().normal();

    // TODO remove mut
    let mut prefix: colored::ColoredString = prefix_style(&args);

    if args.is_present("uniform_style") {
        prefix = get_text_color(&args, prefix);
    }

    if args.is_present("enable_bright")
        || args.value_of("prefix_type").unwrap_or("default") == "bright"
    {
        let (message_type, _, message_level) = args_to_enum(&args);
        res = color_from_enum(res, &message_type, &message_level);
    } else {
        res = get_text_color(&args, res);
    }

    let indent = " ".repeat(
        args.value_of("indent").unwrap_or("0").parse().unwrap_or(0) * 4,
    );

    if args.is_present("prefix_disabled") {
        println!("{}{}", indent, res);
    } else {
        println!("{}{} {}", indent, prefix, res);
    }

    for line in 1..message.len() {
        if args.is_present("prefix_disabled") {
            println!("{}{}", indent, message[line]);
        } else {
            println!(
                "{}{}| {}",
                indent,
                " ".repeat(prefix.len() - 1),
                message[line].trim()
            );
        }
    }
}
