pub mod termformat;

pub mod a {
    extern crate colored;
    use colored::*;

//as asdf

    #[derive(Debug, PartialEq)]
    pub enum MessageType {
        Log,
        Info,
        Warn,
        Error,
    }

    #[derive(Debug, PartialEq)]
    pub enum MessageStyle {
        Default,
        Verbose,
        Gtest,
        Parser,
        Bright,
    }

    pub fn from_enum(
        message_type: &MessageType,
        level: &u8,
        style: &MessageStyle,
    ) -> colored::ColoredString {
        //println!("{}", match message_type{//    MessageType::Log =
        //});

        //    print!("{:?} ", message_type);
        //   print!("{:?} ", style);

        let res = match message_type {
            MessageType::Log => match level {
                0 => match style {
                    MessageStyle::Default => "  >",
                    MessageStyle::Verbose => "[ LOG 0   ]:",
                    MessageStyle::Gtest => "[          ]",
                    MessageStyle::Parser => "[colecho:log:0]",
                    MessageStyle::Bright => "---",
                },
                1 => match style {
                    MessageStyle::Default => "---",
                    MessageStyle::Verbose => "[ LOG 1   ]:",
                    MessageStyle::Gtest => "[----------]",
                    MessageStyle::Parser => "[colecho:log:1]",
                    MessageStyle::Bright => "---",
                },
                2 => match style {
                    MessageStyle::Default => "***",
                    MessageStyle::Verbose => "[ LOG 2   ]:",
                    MessageStyle::Gtest => "[**********]",
                    MessageStyle::Parser => "[colecho:log:2]",
                    MessageStyle::Bright => "---",
                },
                3 => match style {
                    MessageStyle::Default => ">>>",
                    MessageStyle::Verbose => "[ LOG 3   ]:",
                    MessageStyle::Gtest => "[>>>>>>>>>>]",
                    MessageStyle::Parser => "[colecho:log:3]",
                    MessageStyle::Bright => "---",
                },
                _ => "",
            },
            MessageType::Info => match level {
                0 => match style {
                    MessageStyle::Default => "  >",
                    MessageStyle::Verbose => "[ INFO 0  ]:",
                    MessageStyle::Gtest => "[->->->->->]",
                    MessageStyle::Parser => "[colecho:info:0]",
                    MessageStyle::Bright => "~~~",
                },
                1 => match style {
                    MessageStyle::Default => "  >",
                    MessageStyle::Verbose => "[ INFO 1  ]:",
                    MessageStyle::Gtest => "[->->->->->]",
                    MessageStyle::Parser => "[colecho:info:1]",
                    MessageStyle::Bright => "~~~",
                },
                2 => match style {
                    MessageStyle::Default => "-->",
                    MessageStyle::Verbose => "[ INFO 2  ]:",
                    MessageStyle::Gtest => "[->>->>->>-]",
                    MessageStyle::Parser => "[colecho:info:2]",
                    MessageStyle::Bright => "III",
                },
                3 => match style {
                    MessageStyle::Default => "-->",
                    MessageStyle::Verbose => "[ INFO 3  ]:",
                    MessageStyle::Gtest => "[->>->>->>-]",
                    MessageStyle::Parser => "[colecho:info:3]",
                    MessageStyle::Bright => "III",
                },
                _ => "",
            },
            MessageType::Warn => match level {
                0 => match style {
                    MessageStyle::Default => "==>",
                    MessageStyle::Verbose => "[ WARN 0  ]:",
                    MessageStyle::Gtest => "[=>>=>>=>>=]",
                    MessageStyle::Parser => "[colecho:warn:0]",
                    MessageStyle::Bright => ">>>",
                },
                1 => match style {
                    MessageStyle::Default => "==>",
                    MessageStyle::Verbose => "[ WARN 1  ]:",
                    MessageStyle::Gtest => "[=>>=>>=>>=]",
                    MessageStyle::Parser => "[colecho:warn:1]",
                    MessageStyle::Bright => ">>>",
                },
                2 => match style {
                    MessageStyle::Default => "=>>",
                    MessageStyle::Verbose => "[ WARN 2  ]:",
                    MessageStyle::Gtest => "[=>>=>>=>>=]",
                    MessageStyle::Parser => "[colecho:warn:2]",
                    MessageStyle::Bright => "WWW",
                },
                3 => match style {
                    MessageStyle::Default => "=>>",
                    MessageStyle::Verbose => "[ WARN 3  ]:",
                    MessageStyle::Gtest => "[=>>=>>=>>=]",
                    MessageStyle::Parser => "[colecho:warn:3]",
                    MessageStyle::Bright => "WWW",
                },
                _ => "",
            },
            MessageType::Error => match level {
                0 => match style {
                    MessageStyle::Default => "###",
                    MessageStyle::Verbose => "[ ERROR 0 ]:",
                    MessageStyle::Gtest => "[##########]",
                    MessageStyle::Parser => "[colecho:error:0]",
                    MessageStyle::Bright => "!!!",
                },
                1 => match style {
                    MessageStyle::Default => "!!!",
                    MessageStyle::Verbose => "[ ERROR 1 ]:",
                    MessageStyle::Gtest => "[!!!!!!!!!!]",
                    MessageStyle::Parser => "[colecho:error:1]",
                    MessageStyle::Bright => "!!!",
                },
                2 => match style {
                    MessageStyle::Default => "!!!",
                    MessageStyle::Verbose => "[ ERROR 2 ]:",
                    MessageStyle::Gtest => "[!!!!!!!!!!]",
                    MessageStyle::Parser => "[colecho:error:2]",
                    MessageStyle::Bright => "EEE",
                },
                3 => match style {
                    MessageStyle::Default => "!!!",
                    MessageStyle::Verbose => "[ ERROR 3 ]:",
                    MessageStyle::Gtest => "[!!!!!!!!!!]",
                    MessageStyle::Parser => "[colecho:error:3]",
                    MessageStyle::Bright => "EEE",
                },
                _ => "",
            },
        };

        res.clear()
    }

    pub fn color_from_enum(
        prefix: colored::ColoredString,
        message_type: &MessageType,
        message_level: &u8,
    ) -> colored::ColoredString {
        match message_type {
            MessageType::Log => prefix,
            MessageType::Info => match message_level {
                0 => prefix.green(),
                1 => prefix.blue(),
                2 => prefix.yellow(),
                3 => prefix.red(),
                _ => prefix,
            },
            MessageType::Warn => match message_level {
                0 => prefix.bold().green(),
                1 => prefix.bold().yellow(),
                2 => prefix.bold().bright_yellow(),
                3 => prefix.bold().bright_red(),
                _ => prefix,
            },
            MessageType::Error => match message_level {
                0 => prefix.bright_green().bold().on_black().blink(),
                1 => prefix.bright_white().on_red().bold(),
                2 => prefix.bright_blue().on_red().bold(),
                3 => prefix.bright_yellow().on_red().bold(),
                _ => prefix,
            },
        }
    }

    pub fn colecho(msgtype: MessageType, level: u8, message: &str, pre_prefix:&str) {
        let pref: colored::ColoredString = from_enum(&msgtype, &level, &MessageStyle::Default);
        let pref = color_from_enum(pref, &msgtype, &level);
        print!("{}{} {}", pre_prefix,pref, message);
    }

}
