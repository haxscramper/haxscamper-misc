use terminal_size::{terminal_size, Width};

pub fn justify_fit_terminal(
    padding: (usize, usize),
    string: &str,
) -> Vec<String> {
    let (left_padding, right_padding) = padding;

    let term_width = if let Some((Width(term_width), _)) = terminal_size()
    {
        (term_width as usize) - left_padding - right_padding
    } else {
        8192
    };

    let str_split: Vec<String> = string
        .split(' ')
        .map(|s| s.trim())
        .filter(|s| !s.is_empty())
        .map(|s| s.to_string())
        .collect();

    let mut res: Vec<String> = Vec::new();
    let mut buf = String::new();

    for s in str_split {
        if buf.len() + s.len() <= term_width as usize {
            buf = buf + " " + &s;
        } else {
            buf = " ".repeat(left_padding).to_owned() + &buf;
            res.push(buf);
            buf = " ".to_owned() + &s;
        }
    }

    buf = " ".repeat(left_padding).to_owned() + &buf;
    res.push(buf);

    return res;
}
