#[macro_use]
extern crate clap;
extern crate chrono;
extern crate colored;

use colored::*;
use std::env;
use std::path::Path;

fn main() {
    let args: clap::ArgMatches = clap_app!
        (myapp =>
         (@arg last_proc_status: --last_proc_status +takes_value *
          "Last process status")
        )
        .get_matches();

    let last_proc_status = args.value_of("last_proc_status").unwrap();
    let pwd = env::var("PWD").unwrap();
    let pwd = Path::new(&pwd);

    let path_parts: Vec<&str> = pwd
        .components()
        .rev()
        .map(|i| i.as_os_str().to_str().unwrap())
        .collect();

    println!("{}", &path_parts[0..2].join("/").green());

    print!("[{:^3}] > ", last_proc_status.red());
}
