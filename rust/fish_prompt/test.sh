#!/usr/bin/env bash
cd "$(dirname "$0")"
set -o nounset
set -o errexit

cecho -i1 "Running test.sh"
##== Only edit lines after this comment

./build.sh
./target/debug/fish_prompt --last_proc_status $?
