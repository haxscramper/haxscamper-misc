#!/usr/bin/env bash
cd "$(dirname "$0")"
set -o nounset
set -o errexit

cecho -i1 "Running run-dev.sh"\n
cat << EOF | entr -c "./test.sh"
./src/main.rs
./test.sh
./Cargo.toml
test.sh
EOF

