#!/usr/bin/env bash
# -*- coding: utf-8 -*-
# bash
cd "$(dirname "$0")"
set -o nounset
set -o errexit

gcc -oout.bin x_cli_type.c -lX11
ls
./out.bin
