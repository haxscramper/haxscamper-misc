#!/usr/bin/env fish
for dir in *
    cecho -i -p $dir
    fish -c "cd $dir; git status"
end
