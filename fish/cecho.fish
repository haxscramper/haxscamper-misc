#! /usr/bin/env fish

function manpages
printf "
.TH CECHO 1 'February 2, 2019' 'cecho version 1.0'
.SH NAME
cecho \- Colored ECHO
.SH SYNOPSIS
cecho [-w|w1|l|i|i1|i2|i3|e|E] [-su] [-si] [-sb] [-sB]
.SH DESCRIPTION
cecho creates colored version of the argument with optionally
supplied prefixesA
.SS Options
.TP
-si
Add italc
.TP
-su
Add underline
.TP
-sb
Add bold
.TP
-ss
Add strikethrough
.TP
-w
Use WARN style
.TP
-w1
Use WARN_1 style
.TP
-i
Use INFO style
.TP
-fc=[colorname]
Use foreground color [colorname] for printing. Possible values are: black,
red, green, yellow, blue, magenta, cyan, white
" | groff -man -Tascii
end

set message ""
set codes ""
set prefix "  >"
set allow_prefix 1


for arg in $argv
    switch $arg
        case -h
            manpages
            exit 0
        ## Style options
        case -su # Underline
            set codes "$codes\e[4m"
        case -sb # Bold
            set codes "$codes\e[1m"
        case -sB # Blink
            set codes "$codes\e[5m"
        case -si # Italic
            set codes "$codes\e[3m"
        case -ss # Strikethrough
            set codes "$codes\e[9m"
        case -sI # Inverted
            set codes "$codes\e[31m"
        ### Unset style
        case
        ## LOG
        case -l
           set codes "\e[0m"
           set prefix "  >"
        ## WARN styles
        case -w
            set codes "\e[35;1m"
            set prefix "==>"
        case -w1
            set codes "\e[96;1m"
            set prefix "==>"
        ## INFO styles
        case -i
            set codes "\e[32m"
            set prefix " ->"
        case -i1
            set codes "\e[33m"
            set prefix " ->"
        case -i2
            set codes "\e[34m"
            set prefix "-->"
        case -i3
            set codes "\e[31m"
            set prefix "-->"
        ## ERROR styles
        case -e
            set codes "\e[97;41;1m"
            set prefix "!!!"
        case -E
            set codes "\e[40;92;5;1m"
            set prefix "###"
        ## Other
        case -p
            set allow_prefix 0
        case --example
            for prefix in i i1 i2 i3 l w w1 e E
                cecho -$prefix "Example"
            end
        case '-fc=*'
            switch $arg
                case -fc=red
                    set codes "$codes\e[31m"
                case -fc=green
                    set codes "$codes\e[32m"
                case -fc=yellow
                    set codes "$codes\e[33m"
                case -fc=blue
                    set codes "$codes\e[34m"
                case -fc=magenta
                    set codes "$codes\e[35m"
                case -fc=cyan
                    set codes "$codes\e[36m"
                case -fc=white
                    set codes "$codes\e[37m"
                case -fc=black
                    set codes "$codes\e[30m"
            end
        case '*'
            set -a message $arg
    end
end


set message (printf "%s" $message | sed 's/^\s*//')

if test $allow_prefix -eq 1
    echo -e "$codes$prefix\e[0m $codes$message\e[0m"
else
    echo -e "$codes$message\e[0m"
end
